//
//  NSString+LocalizeString.h
//  Gold Chain
//
//  Created by Andrei Boulgakov on 04/11/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LocalizeString)

+ (NSString*)languageSelectedStringForKey:(NSString *)key;
+ (NSString *)getCurrentSavedLanguage;



@end
