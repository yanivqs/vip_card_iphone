//
//  FactoryListData.h
//  VIPCard
//
//  Created by Manoj Kumar on 11/5/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"
#import "Fcatorycd.h"

@interface FactoryListData : WSBaseData
@property (nonatomic, strong) NSString *id, *name;

-(id)populateCoreDataFromObject:(id)coreData;
-(void)populateObjectFromCorData:(id)coreData;

@end
