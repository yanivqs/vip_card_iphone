//
//  PopUpMealsListVC.h
//  VIPCard
//
//  Created by Vishal Kolhe on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "WSExtraForMealsResponse.h"

@protocol PopUpMealsListVCDelegate <NSObject>

-(void)selectedItem:(id)item;
-(void)selectedListofItems:(NSArray *)aList;

@end

@interface PopUpMealsListVC : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (assign, nonatomic) id<PopUpMealsListVCDelegate> delegate;

- (IBAction)onClickSubmitButton:(id)sender;

@property (nonatomic, strong) WSExtraForMealsResponse *extraForMealsObj;

@end
