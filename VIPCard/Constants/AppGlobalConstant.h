//
//  AppGlobalConstant.h
//  HopKidDrive
//
//  Created by SanC on 17/10/14.
//  Copyright (c) 2014 Clerisyinc. All rights reserved.
//

#ifndef HopKidDrive_AppGlobalConstant_h
#define HopKidDrive_AppGlobalConstant_h


#endif
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)




#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBAlpha(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_LARGE_SCREEN ([[UIScreen mainScreen] bounds].size.height > 568)?TRUE:FALSE

#define APP_TITLE_WHITE_COLOR [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1]




#define AppDefaultLightFont(r) [UIFont fontWithName:@"Raleway-Light" size:r]
#define AppDefaultFont(r) [UIFont fontWithName:@"Raleway" size:r]
#define AppDefaultMediumFont(r) [UIFont fontWithName: @"Raleway-Menium" size:r]
#define AppDefaultSemiBolFont(r) [UIFont fontWithName:@"Raleway-SemiBold" size:r]
#define AppDefaultBoldFont(r) [UIFont fontWithName:@"Raleway-Bold" size:r]
#define AppDefaultExtraBoldFont(r) [UIFont fontWithName:@"Raleway-ExtraBold" size:r]


#define AppProximaBoldFont(r) [UIFont fontWithName:@"ProximaNova-Bold" size:r]
#define AppProximaSemiBoldFont(r) [UIFont fontWithName:@"ProximaNova-Semibold" size:r]

#define SERVER_FULL_DATE_FORMAT @"yyyy-MM-dd HH:mm:ss"
#define SERVER_DATE_FORMAT @"yyyy-MM-dd"
#define DISPLAY_DATE_FORMAT @"MM/dd/yyyy"
#define DISPLAY_TIME_FORMAT @"h:mm a"

#define SCREEN_WIDTH            [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT           [UIScreen mainScreen].bounds.size.height

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define IPHONE  UIUserInterfaceIdiomPhone

#define IS_IPHONE_4 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON)
#define IS_IPHONE_5 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)


#define ENGLSIH_LANGUAGE     @"en"
#define HEBREW_LANGUAGE     @"he"
#define RUSSIAN_LANGUAGE    @"ru"
#define APPLICATION_DEFAULT_LANGUAGE    @"application_default_laungage"


#define PLACE_HOLDER_IMAGE @"placeholder"
#define Place_HOLDER_IMAGEBLUE @"placeHolderBlue"
#define SHEKEL @"₪"

//Key constant
#define kAppDeviceNotificationToken  @"kAppDeviceNotificationToken"
#define kAppLastLoggedUserName  @"kAppLastLoggedUserName"

#define LocalMSG(s) NSLocalizedString(s, @"")

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]
#define DATE_FMT_YYYY_MM_DD         @"yyyy-MM-dd"
#define DATE_FMT_DD_MM_YYYY         @"dd-MM-yyyy"
#define GRAPH_DATE_FMT_DD_MM_YYYY   @"dd/MM/yyyy"
#define DATE_FMT_DD_MM_YYYY_1       @"yyyy-MM-DD"

#define DATE_FMT_DD_MM_YYYY_2      @"yyyy-MM-dd"


#define DATE_FMT_DD_MM_YYYY__HH_MM_SS_12 @"dd/MM/yyyy hh:mm:ss"
#define DATE_FMT_DD_MM_YYYY__HH_MM_SS_24 @"dd/MM/yyyy HH:mm:ss"
#define DATE_yyyy_MM_dd_HH_MM_SS    @"yyyy-MM-dd HH:mm:ss"
#define TIME_FMT_HH_MM_SS           @"HH:mm:ss"
#define DATE_SUMMARY_FORMAT         @"HH:mm, dd MMM yyyy"

#define DATE_FMT_100                @"dd MMM yyyy hh:mm aaa"
#define RECEIPT_NAME_DATE_FORMAT    @"dd.MM.YYYY HH:mm"
#define DATE_FMT_TIME_ONLY_24       @"HH:mm"
#define DATE_MM_DD_HH_MM            @"MMM dd"