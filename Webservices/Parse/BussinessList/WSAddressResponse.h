//
//  WSAddressResponse.h
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSAddressResponse : WSBaseData

/*
 NumberStreet : "10"
 Floor : "2"
 NumberApartment : "23"
 Entrance : "2"
 DrivingDirections : null
 street : "העצמאות"
 town : "יבנה"
*/

@property (nonatomic, strong) NSString *NumberStreet, *Floor, *NumberApartment, *Entrance, *DrivingDirections, *street, *town;
//-(id)initWithDictionary:(NSDictionary *)dict;
@end
