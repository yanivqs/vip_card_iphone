//
//  WSBaseData.m
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//
#define SAFE(src) \
src!=nil?src:NSNULL

#define SAFE_DEF(value, default) ((value==nil)?(default):(value))
#define NSNULL [NSNull null]
#define IsNULL(val) (val==nil || [val isKindOfClass:[NSNull class]])


#import "WSBaseData.h"
#import <objc/runtime.h>

@implementation WSBaseData
@synthesize ignoreProperties;

- (id) initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
        [self parseWithDictionary:dict];
    
    return self;
}

-(void) parseWithDictionary:(NSDictionary *)dict
{
    @autoreleasepool {
       [self populateFromDictionary:dict];
    }
    
}

- (void)populateFromDictionary:(NSDictionary *)dict
{
    unsigned int outCount, i;
    objc_property_t *clsProperties = class_copyPropertyList([self class], &outCount);
    
    for (i = 0; i < outCount; i++)
    {
        objc_property_t property = clsProperties[i];
        NSString *propertyName = [[NSString alloc] initWithUTF8String:property_getName(property)];
        
        id propertyValue = [dict valueForKey:(NSString *)propertyName];
        //now check if value is empty then ignore field
        
        if(propertyValue)
        {
            if(IsNULL(propertyValue))
            {
                propertyValue = @"";
            }
            [self  setValue:SAFE_DEF(propertyValue, @"") forKey:propertyName];
        }
    }
    free(clsProperties);
    
}

- (NSMutableDictionary *)getDictionaryFromObject
{
   
    
    unsigned int outCount, i;
    objc_property_t *clsProperties = class_copyPropertyList([self class], &outCount);
    NSMutableDictionary *props = [NSMutableDictionary dictionaryWithCapacity:outCount];
    
        for (i = 0; i < outCount; i++)
        {
            objc_property_t property = clsProperties[i];
            NSString *propertyName = [[NSString alloc] initWithUTF8String:property_getName(property)];
            if(!ignoreProperties || ![ignoreProperties containsObject:propertyName])
            {
                id propertyValue = [self valueForKey:(NSString *)propertyName];
                //now check if value is empty then ignore field
                
                if(propertyValue)
                {
                    if([propertyValue isKindOfClass:[NSString class]])
                    {
                        if(![propertyValue isEmpty])
                            [props setObject:propertyValue forKey:propertyName];
                    }
                    else if([propertyValue isKindOfClass:[NSNumber class]])
                    {
                        if(![[NSDecimalNumber notANumber] isEqualToNumber:propertyValue])
                            [props setObject:propertyValue forKey:propertyName];
                    }
                    else
                        [props setObject:propertyValue forKey:propertyName];
                }
            }
        }
        free(clsProperties);
    
   
    return props;
}


-(id)getCoreDataFromObject:(id) coreData
{
    return coreData;
}

-(void) populateObjectFromCorData:(id)coreData
{
    
}
@end
