//
//  MarketProductVC.h
//  VIPCard
//
//  Created by Vishal Kolhe on 20/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "CatTopProduct.h"
#import "CatTopProductDetail.h"


@interface MarketProductVC : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *listOfmarketItems;
@property (nonatomic, strong) CatTopProduct *marketResponse;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong)  NSString *marketPlaceId;
@property (weak, nonatomic) IBOutlet UITableView *ProductListView;
@property (assign) BOOL isCoupns;
@property (assign) BOOL isSearch;
@property (nonatomic,strong)NSArray *filteredList;


-(void) reloadScreenData;

//-(float)height :(NSMutableAttributedString*)string;

@end
