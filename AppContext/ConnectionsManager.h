//
//  ConnectionsManager.h
//  Chain
//
//  Created by Nava Carmon on 4/25/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "WSBaseResponse.h"
#import "WSConstants.h"

@protocol ServerResponseDelegate <NSObject>
- (void) success:(WSBaseResponse *)response;
- (void) failure:(WSBaseResponse *)response;
@end

typedef void (^CompletionBlock)(NSDictionary *result, NSError *error);

@interface ConnectionsManager : AFHTTPRequestOperationManager <ServerResponseDelegate>

+ (instancetype)sharedManager;

-(void)getdeals_withdelegate:(id<ServerResponseDelegate>) delegate;
-(void)fetchCoupons_withdelegate:(id<ServerResponseDelegate>) delegate;

-(void)fetchDealOfCoupons_withdelegate:(id<ServerResponseDelegate>) delegate;

-(void)getBussinessList_withdelegate:(id<ServerResponseDelegate>) delegate;

-(void)getCatMealforBussinesswithID:(NSString *)bussinessID delegate:(id<ServerResponseDelegate>) delegate;
-(void)getFindBussinessList_withdelegate:(id<ServerResponseDelegate>) delegate;


-(void)gettowns_withdelegate:(id<ServerResponseDelegate>) delegate;
-(void)getstreets_withdelegate:(id<ServerResponseDelegate>) delegate;

-(void)submitOrder:(NSDictionary *)params withdelegate:(id<ServerResponseDelegate>) delegate;
-(void)getFildCategoryFilterList_withdelegate:(id<ServerResponseDelegate>) delegate;
-(void)getFieldCategoryDetail_withdelegate:(NSString *)filterID delegate:(id<ServerResponseDelegate>) delegate;
-(void)getloginwithmember_withdelegate:(NSString *)cardId delegate:(id<ServerResponseDelegate>) delegate;

-(void)login_withdelegate:(NSDictionary *)dicdata delegate:(id<ServerResponseDelegate>) delegate;

-(void)getregistration_withdelegate:(NSDictionary *)dictdata delegate:(id<ServerResponseDelegate>) delegate;
-(void)getMarketList_withdelegate:(id<ServerResponseDelegate>) delegate;
-(void)getCatTopProduct_withdelegate:(NSString *)marketID delegate:(id<ServerResponseDelegate>) delegate;
-(void)fetchOnlyBussiness_withdelegate:(NSString *)businessId delegate:(id<ServerResponseDelegate>) delegate;
-(void)fetchMealsOfBusiness_withdelegate:(NSString *)businessId subCategory:(NSString*)subCategory delegate:(id<ServerResponseDelegate>)delegate;
-(void)getShabatData_withdelegate:(id<ServerResponseDelegate>)delegate;

-(void)markBusinessCallOrder_withdelegate:(NSDictionary*)dict delegate:(id<ServerResponseDelegate>)delegate;
-(void)getBusinessDeliveryTime_withdelegate:(NSString *)businessId delegate:(id<ServerResponseDelegate>)delegate;
-(void)submitOrderPayment:(NSDictionary *)params withdelegate:(id<ServerResponseDelegate>) delegate;


//Manoj kumar
-(void)getFactoryList:(NSDictionary *)params withdelegate:(id<ServerResponseDelegate>) delegate;

//- (void) createNewUserWithParameters:(NSDictionary *) parameters delegate:(id<ServerResponseDelegate>) delegate;
//- (void) loginWithParameters:(NSDictionary *) parameters delegate:(id<ServerResponseDelegate>) delegate;

@end
