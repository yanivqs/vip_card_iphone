//
//  UIImageView+AdjustToCenter.h
//  Chain
//
//  Created by Nava Carmon on 6/6/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AdjustToCenter)
- (CGFloat) adjustImageForBounds:(UIImage *) img;

@end
