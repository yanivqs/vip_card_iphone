//
//  HomeGalleryResponse.h
//  VIPCard
//
//  Created by SanC on 14/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WSBaseData.h"

@interface GridGalleryResponse : WSBaseData

//id: "65",
//name: "white41.jpg",
//NewName: null,
//IdBusiness: "2",
//PathDirectory: "white&files=",
//TypeGallery: "2"


@property (strong ,nonatomic) NSString *id,*name,*NewName,*IdBusiness,*PathDirectory,*TypeGallery;
@end
