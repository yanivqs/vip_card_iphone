//
//  AppAlertView.m
//  CheckNet
//
//  Created by Enovate Macbook Pro on 21/06/16.
//  Copyright © 2016 Enovate IT Outsourcing Pvt Ltd. All rights reserved.
//

#import "AppAlertView.h"

#import <QuartzCore/QuartzCore.h>


#define AppColorFromRGBAlpha(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define kTitleColor AppColorFromRGBAlpha(0x000000)

#define kMessageColor AppColorFromRGBAlpha(0x000000)
#define kOtherButtonColor AppColorFromRGBAlpha(0x000000)
#define kButtonSeparatorColor AppColorFromRGBAlpha(0xCCCCCC)

#define kPopupViewBGcolor [UIColor colorWithRed:(127.0/255) green:(80.0/255) blue:(161.0/255) alpha:1]


#define kTitleFont  [UIFont boldSystemFontOfSize:16.0]
#define kMessageFont  [UIFont systemFontOfSize:16.0]
#define kButtonFont  [UIFont boldSystemFontOfSize:14.0]

#define kBottomSpaceFromButon 60.0f
#define kTopPadding  30.0f
#define kSidePadding 10.0f
#define kViewSideSpace 10.0f
#define kViewMinHeight 230.f

#define kButtonHeight 40.0f

#define kPaddingFromTitle 40.f




@interface AppAlertView()
{
    UIImageView *topImg;
    UILabel *lblMessage;
    UIImageView *bottomImg;
    UIButton *cancel;
    UIImageView *popupImg;
    UIButton *cancelButton;
    UILabel *lblTitle
    ;



}

@property(nonatomic,strong) NSMutableArray *arrButtons;

@end



@implementation AppAlertView
@synthesize childView = _childView,arrButtons;


+(AppAlertView *) createWithTitle:(NSString *) title message:(NSString *) msg cancelButton:(NSString *) cancelButtonTitle otherButtons:(NSString *) otherButtonTitle, ...
{
    AppAlertView *alertView =[[AppAlertView alloc] initWithTitle:title message:msg cancelButton:cancelButtonTitle];
    if(alertView)
    {
        va_list args;
        va_start(args, otherButtonTitle);
        for (NSString *arg = otherButtonTitle; arg != nil; arg = va_arg(args, NSString*))
        {
           // [alertView addOtherButton:arg];
        }
        va_end(args);
    }
    
    return alertView;
    
}


+(AppAlertView *) createWithView:(UIView *) childView title:(NSString *) title message:(NSString *) msg cancelButton:(NSString *) cancelButtonTitle otherButtons:(NSString *) otherButtonTitle, ...
{
    AppAlertView *alertView =[[AppAlertView alloc] initWithTitle:title message:msg cancelButton:cancelButtonTitle];
    if(alertView)
    {
        alertView.childView = childView;
        va_list args;
        va_start(args, otherButtonTitle);
        for (NSString *arg = otherButtonTitle; arg != nil; arg = va_arg(args, NSString*))
        {
            //[alertView addOtherButton:arg];
        }
        va_end(args);
    }
    
    return alertView;
}

-(id) initWithTitle:(NSString *) title message:(NSString *) msg cancelButton:(NSString *) cancelButtonTitle
{
    self = [super init];
    if(self)
    {
        
        
        //backgroundView
        backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        
        popupView=[[UIView alloc]initWithFrame:CGRectZero];
        popupImg=[[UIImageView alloc]initWithFrame:CGRectZero];
        popupImg.image=[UIImage imageNamed:@"tos_bg"];
        //top
        topImg = [[UIImageView alloc] initWithFrame:CGRectZero];
        topImg.image=[UIImage imageNamed:@"text_header"];
                // cancel button
        cancel=[[UIButton alloc]initWithFrame:CGRectZero];
        
        [cancel setBackgroundImage:[UIImage imageNamed:@"text_cross"]
                            forState:UIControlStateNormal];
        
        [cancel addTarget:self action:@selector(onButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        //Title
        lblTitle = [[UILabel alloc] initWithFrame:CGRectZero];
        lblTitle.text = title;
        lblTitle.numberOfLines = 0;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
        //Message
        lblMessage = [[UILabel alloc] initWithFrame:CGRectZero];
        lblMessage.text = msg;
        lblMessage.textColor=[UIColor whiteColor];
        lblMessage.numberOfLines = 0;
        lblMessage.textAlignment = NSTextAlignmentCenter;
        lblMessage.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        //bottom
        bottomImg =[[UIImageView alloc]initWithFrame:CGRectZero];
        bottomImg.image=[UIImage imageNamed:@"text_bottom"];

        
        //Now assign color and font
        _messageFont = kMessageFont;
        
        
        if(cancelButtonTitle)
        {
            cancelButton =[UIButton buttonWithType:UIButtonTypeCustom];
            cancelButton.tag = 0;
            _cancelButtonIndex = 0;
            [cancelButton addTarget:self action:@selector(onButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self titleForButton:cancelButton title:cancelButtonTitle];
        }

        
        [self addSubview:backgroundView];
        
    }
    
    return self;
    
}
-(void) titleForButton:(UIButton *)btn title:(NSString *) title{
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateSelected];
}
-(void) fontForButton:(UIButton *)btn font:(UIFont *) font{
    [btn.titleLabel setFont:font];
}

-(void) addOtherButton:(NSString *) btnTitle
{
    UIButton *btn =[UIButton buttonWithType:UIButtonTypeCustom];
    btn.tag = [arrButtons count] + 1;
    [self titleForButton:btn title:btnTitle];
    [btn addTarget:self action:@selector(onButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [arrButtons addObject:btn];
    
}
-(void) setAlertTitle:(NSString *)alertTitle
{
    _alertTitle = alertTitle;
    if(lblTitle)
        lblTitle.text = _alertTitle;
}



-(void) show:(UIView *) parentView rtlFlag:(BOOL)flag
{
    rtlFlag = flag;
    _parentView = parentView;
    
     [self.layer setOpacity:0.0f];
    [self createComponents];
    [self doComponentStyling];
    [self layoutViewItems];
    
    [self addTouchHandlers];
    
    //Now add this to main window
    UIWindow *win = [self findMainTopWindow];
    [win addSubview:self];
    
    [self.layer setOpacity:0.0f];
    [popupView.layer setOpacity:0.0f];
    
    
    [UIView animateWithDuration:0.2f animations:^{
        [self.layer setOpacity:1.0f];
        [popupView.layer setOpacity:1.0f];
    }                completion:^(BOOL finished) {
        
        
    }];

    
}

-(UIWindow *) findMainTopWindow{
    
    NSEnumerator *frontToBackWindows = [[[UIApplication sharedApplication]windows] reverseObjectEnumerator];
    for (UIWindow *window in frontToBackWindows){
        BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;
        BOOL windowIsVisible = !window.hidden && window.alpha > 0;
        BOOL windowLevelNormal = window.windowLevel == UIWindowLevelNormal;
        
        if (windowOnMainScreen && windowIsVisible && windowLevelNormal) {
            return window;
        }
    }
    UIWindow *window =[[[UIApplication sharedApplication]windows] lastObject];
    if(window.alpha == 0)
        window = [[UIApplication sharedApplication]keyWindow];
    
    return window;
    
}

-(void) addTouchHandlers{
    if(!cancel  == 0)
    {
      UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnBackground:)];
      [self addGestureRecognizer:tapGesture];
    }
    
}
-(void) close
{
   
    [UIView animateWithDuration:0.1f animations:^{
        [self.layer setOpacity:0.0f];
    }                completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}


-(void) setAlertMessage:(NSString *)alertMessage
{
    _alertMessage = alertMessage;
    if(lblMessage)
    {
        lblMessage.text = _alertMessage;
    }
}

-(void) addShadowToView:(UIView *) v
{
    v.layer.masksToBounds = NO;
    v.layer.shadowOffset = CGSizeMake(-1, 1);
    v.layer.shadowRadius = 5;
    v.layer.shadowOpacity = 0.5;

}


//Create and to VIEW
-(void) createComponents{

    if(!popupView)
        [popupView removeFromSuperview];
        popupView = [[UIView alloc] initWithFrame:CGRectZero];
    //now add item to popupView
    [popupView addSubview:popupImg];
    [popupImg addSubview:topImg];
    [popupImg addSubview:bottomImg];
    [popupImg addSubview:lblMessage];
    [topImg addSubview:cancel];
       
    [self addSubview:popupView];
    
}
//Now before SHOW give color and style

-(void) doComponentStyling
{
    
    //message
    lblMessage.font = self.messageFont;
    //lblMessage.textColor = self.messageTextColor;
    
    
    //Background
    backgroundView.backgroundColor = [UIColor whiteColor];
    
    //Popupview
//    popupView.backgroundColor = kPopupViewBGcolor;
//    popupView.layer.borderWidth = 1.0;
//    popupView.layer.borderColor = _buttonSeperatorColor.CGColor;
    popupView.layer.cornerRadius = 2.0;
//    popupView.clipsToBounds = YES;
//    [self addShadowToView:popupView];
}



-(void) layoutSubviews
{
    [self layoutViewItems];
    [super layoutSubviews];
}



-(void) layoutViewItems {
    
    CGRect windowFrame = (_parentView) ? _parentView.frame:[[[[UIApplication sharedApplication]delegate] window]frame];
    self.frame = windowFrame;
    backgroundView.frame = windowFrame;
    
    CGFloat screenWidth = CGRectGetWidth(windowFrame);
    CGFloat screenHeight = CGRectGetHeight(windowFrame);
    
    //BackgroundView
    backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    CGRect popupFrame = CGRectMake(0, 0, screenWidth - (kViewSideSpace * 2), 0);
    
    //CGRect itemFrame = CGRectMake(kSidePadding, 0, 0, 30.f);
    popupFrame.size.width = CGRectGetWidth(popupFrame) - kSidePadding * 2;
    CGFloat popViewContenWidth = popupFrame.size.width;
    CGFloat yPos = 0;
    //Title Adjustment
    topImg.frame = popupFrame;
    CGFloat lblHeight = CGRectGetHeight(topImg.frame);
    if(lblHeight > 5)
    {
        lblHeight += 5;
        topImg.frame = CGRectMake(kSidePadding, yPos, topImg.frame.size.width, lblHeight);
    }
        yPos = CGRectGetMaxY(topImg.frame) + kPaddingFromTitle;
    
    CGFloat xPos=kTopPadding;
    cancel.frame = CGRectMake(250, topImg.frame.origin.y + 2, 25, 25);
    xPos = CGRectGetMaxX(cancel.frame) + 1;
    
    //Adjust Child View
   
    
        //Message
        lblMessage.frame = popupFrame;
        [lblMessage sizeToFit];
        lblHeight = CGRectGetHeight(lblMessage.frame);
        if(lblHeight > 5)
        {
            lblHeight += 5;
            lblMessage.frame = CGRectMake(kSidePadding, yPos, lblMessage.frame.size.width-20, lblHeight);
        }
            yPos = CGRectGetMaxY(lblMessage.frame) + kBottomSpaceFromButon;
    
    
        //Now buttonHolder
    
        lblHeight = kButtonHeight + 1;
        CGFloat popupWidth = CGRectGetWidth(popupFrame);
    
        buttomView.frame = CGRectMake(0, yPos, popupWidth, lblHeight);
    
       CGFloat bottomHeight = CGRectGetHeight(buttomView.frame);
       if(bottomHeight > 5)
       {
        bottomHeight += 5;
        buttomView.frame = CGRectMake(kSidePadding, yPos, popViewContenWidth, bottomHeight);
        
        yPos = CGRectGetMaxY(topImg.frame) + kPaddingFromTitle;
       }
        bottomImg.frame=CGRectMake(0, yPos, popViewContenWidth, 15);
    
    
        popupFrame.size.height  =  CGRectGetMaxY(buttomView.frame);
    
        popupFrame.size.height  = yPos;
    
    popupView.frame = popupFrame;
    popupImg.frame=popupFrame;
    //now put into center
    popupView.center = self.center;
    
       }



//Closing when tapped on the background view
- (void)tappedOnBackground:(UITapGestureRecognizer *)tapGesture {
    
    CGPoint location = [tapGesture locationInView:self];
    if (CGRectContainsPoint(popupView.frame, location))
    {
        return;
    }
    [self close];
}

-(void) onButtonPressed:(UIButton *) btn
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(onButtonPressed:)])
    {
      if(btn == cancel)

        [self close];
    }
   
}

@end
