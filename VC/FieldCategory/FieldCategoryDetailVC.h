//
//  FieldCategoryDetailVC.h
//  VIPCard
//
//  Created by SanC on 03/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "WSCategoryFilterDetailResponse.h"
#import "FieldCategoryDetailCell.h"
#import "WSAddressResponse.h"
#import "SWRevealViewController.h"
#import "WSCategoryFilterDetailResponse.h"

@interface FieldCategoryDetailVC : BaseViewController
{

	IBOutlet UIView *view_BackGround;
	__weak IBOutlet UITableView *tbl_View;
	__weak IBOutlet UILabel *lbl_TopTitle;
	NSArray *array_ViewControllers;
	NSMutableArray *arrayCopyCategory;
	__weak IBOutlet UITextField *txtField_Search;
}
@property(strong,nonatomic)WSCategoryFilterDetailResponse *wsCategoryDetailResponse;
@property (weak, nonatomic) IBOutlet UIButton *btn_SideMenu;
@property(strong,nonatomic)NSMutableArray *array_categoryDetailList;
@property(strong,nonatomic)NSString *str_Title;
- (IBAction)onClick_SideMenu:(id)sender;


@end
