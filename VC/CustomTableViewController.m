//
//  CustomTableViewController.m
//  VIPCard
//
//  Created by fis on 19/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CustomTableViewController.h"

@interface CustomTableViewController ()
{
    NSArray *arr;
    NSArray *imagesArray;
}

@end

@implementation CustomTableViewController

- (void)viewDidLoad {
    [self showBackButton:YES];
    [super viewDidLoad];
    arr=@[@"First",@"Second",@"Third",@"Fourth",@"Fifth"];
    imagesArray=@[@"deals_b",@"icon_day",@"marketing_chains",@"meal",@"menu_business",@"menu_coupon"];
    // Do any additional setup after loading the view.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arr count];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    // To "clear" the footer view
    return [UIView new] ;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VIPCardTableViewCell *cell = (VIPCardTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"VIPCardTableViewCell"];
    if (cell==nil)
    {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"VIPCardTableViewCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.nameLabel.text = [arr objectAtIndex:indexPath.row];
    cell.descriptionLabel.text = [arr objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]];
    //[[cell imageView]setImage:[UIImage imageNamed:[imagesArray objectAtIndex:indexPath.item]]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier:@"mainSegue" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
