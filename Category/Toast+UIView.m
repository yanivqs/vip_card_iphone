//
//  Toast+UIView.m
//  Toast
//  Version 2.0
//
//  Copyright 2013 Charles Scalesse.
//

#import "Toast+UIView.h"
#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>
#import "NSString+CommonForApp.h"
#import "AppStyleConstant.h"

/*
 *  CONFIGURE THESE VALUES TO ADJUST LOOK & FEEL,
 *  DISPLAY DURATION, ETC.
 */

// general appearance

static const CGFloat CSToastYPos                =  20.0f; // Status bar height

static const CGFloat CSToastMaxWidth            = 0.8;      // 80% of parent view width
static const CGFloat CSToastMaxHeight           = 0.8;      // 80% of parent view height
static const CGFloat CSToastHorizontalPadding   = 10.0;
static const CGFloat CSToastVerticalPadding     = 10.0;
static const CGFloat CSToastCornerRadius        = 5.0;
static const CGFloat CSToastOpacity             = 0.4;
static const CGFloat CSToastFontSize            = 14.0;
static const CGFloat CSToastMaxTitleLines       = 0;
static const CGFloat CSToastMaxMessageLines     = 0;
static const CGFloat CSToastFadeDuration        = 0.2;

// shadow appearance
static const CGFloat CSToastShadowOpacity       = 0.8;
static const CGFloat CSToastShadowRadius        = 6.0;
static const CGSize  CSToastShadowOffset        = { 4.0, 4.0 };
static const BOOL    CSToastDisplayShadow       = YES;

// display duration and position
static const CGFloat CSToastDefaultDuration     = 2.0;
static const NSString * CSToastDefaultPosition  = @"bottom";

// image view size
static const CGFloat CSToastImageViewWidth      = 80.0;
static const CGFloat CSToastImageViewHeight     = 80.0;

// activity
static const CGFloat CSToastActivityWidth       = 100.0;
static const CGFloat CSToastActivityHeight      = 100.0;
static const NSString * CSToastActivityDefaultPosition = @"center";
static const NSString * CSToastActivityViewKey  = @"CSToastActivityViewKey";


@interface UIView (ToastPrivate)<UIGestureRecognizerDelegate>

- (CGPoint)centerPointForPosition:(id)position withToast:(UIView *)toast;
//- (UIView *)viewForMessage:(NSString *)message title:(NSString *)title image:(UIImage *)image;
- (void)makeToast:(NSString *)message duration:(CGFloat)interval position:(id)position title:(NSString *)title hint:(NSString *)hintStr image:(UIImage *)image;
@end


@implementation UIView (Toast)

#pragma mark - Toast Methods

- (void)makeToast:(NSString *)message {
    [self makeToast:message duration:CSToastDefaultDuration position:CSToastDefaultPosition];
}

- (void)makeToast:(NSString *)message duration:(CGFloat)interval position:(id)position {
    UIView *toast = [self viewForMessage:message title:nil hint:nil image:nil];
    [self showToast:toast duration:interval position:position];  
}

- (void)makeToast:(NSString *)message duration:(CGFloat)interval position:(id)position title:(NSString *)title {
    UIView *toast = [self viewForMessage:message title:title hint:nil image:nil];
    [self showToast:toast duration:interval position:position];  
}



-(void)setToastFrame:(NSString *)message duration:(CGFloat)interval frame:(CGRect)frame
{

}


- (void)makeToast:(NSString *)message duration:(CGFloat)interval position:(id)position image:(UIImage *)image {
    UIView *toast = [self viewForMessage:message title:nil hint:nil image:image];
    [self showToast:toast duration:interval position:position];  
}

- (void)makeToast:(NSString *)message duration:(CGFloat)interval  position:(id)position title:(NSString *)title hint:(NSString *)hintStr image:(UIImage *)image
{
    UIView *toast = [self viewForMessage:message title:title hint:hintStr image:image];
    [self showToast:toast duration:interval position:position];
}

- (void)showToast:(UIView *)toast {
    [self showToast:toast duration:CSToastDefaultDuration position:CSToastDefaultPosition];
}

- (void)showToast:(UIView *)toast duration:(CGFloat)interval position:(id)point {
    toast.center = [self centerPointForPosition:point withToast:toast];
    toast.alpha = 0.0;
    [self addSubview:toast];
	[self bringSubviewToFront:toast];
	if(interval >0.5)
		interval = interval - 0.3;

    [UIView animateWithDuration:CSToastFadeDuration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         toast.alpha = 1.0;
                     } completion:^(BOOL finished) {


						 [self performSelector:@selector(hideToastAfterTimer:) withObject:toast afterDelay:interval];
                       /*  [UIView animateWithDuration:CSToastFadeDuration
                                               delay:interval
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              toast.alpha = 0.0;
                                          } completion:^(BOOL finished) {
                                              [toast removeFromSuperview];
                                          }];*/
                     }];
}

#pragma mark - Toast Activity Methods

-(void) hideToastAfterTimer:(UIView *) toast
{
	if(toast && toast.superview != nil)
	{
		[UIView animateWithDuration:CSToastFadeDuration
							   delay:0.2
							 options:UIViewAnimationOptionCurveEaseIn
						  animations:^{
							  toast.alpha = 0.0;
						  } completion:^(BOOL finished) {
							  [toast removeFromSuperview];
						  }];
	}

}

- (void)makeToastActivity {
    [self makeToastActivity:CSToastActivityDefaultPosition];
}

- (void)makeToastActivity:(id)position {
    // sanity
    UIView *existingActivityView = (UIView *)objc_getAssociatedObject(self, &CSToastActivityViewKey);
    if (existingActivityView != nil) return;
    
    UIView *activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CSToastActivityWidth, CSToastActivityHeight)];
    activityView.center = [self centerPointForPosition:position withToast:activityView];
    activityView.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:CSToastOpacity];
    activityView.alpha = 0.0;
    activityView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    activityView.layer.cornerRadius = CSToastCornerRadius;
    
    if (CSToastDisplayShadow) {
        activityView.layer.shadowColor = [UIColor blackColor].CGColor;
		 activityView.layer.shadowOpacity = CSToastShadowOpacity;
        activityView.layer.shadowRadius = CSToastShadowRadius;
        activityView.layer.shadowOffset = CSToastShadowOffset;
    }
    
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicatorView.center = CGPointMake(activityView.bounds.size.width / 2, activityView.bounds.size.height / 2);
    [activityView addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    // associate ourselves with the activity view
    objc_setAssociatedObject (self, &CSToastActivityViewKey, activityView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self addSubview:activityView];
    
    [UIView animateWithDuration:CSToastFadeDuration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         activityView.alpha = 1.0;
                     } completion:nil];
}

- (void)hideToastActivity {
    UIView *existingActivityView = (UIView *)objc_getAssociatedObject(self, &CSToastActivityViewKey);
    if (existingActivityView != nil) {
        [UIView animateWithDuration:CSToastFadeDuration
                              delay:0.0
                            options:(UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState)
                         animations:^{
                             existingActivityView.alpha = 0.0;
                         } completion:^(BOOL finished) {
                             [existingActivityView removeFromSuperview];
                             objc_setAssociatedObject (self, &CSToastActivityViewKey, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                         }];
    }
}

#pragma mark - Private Methods

- (CGPoint)centerPointForPosition:(id)point withToast:(UIView *)toast {
    if([point isKindOfClass:[NSString class]]) {
        // convert string literals @"top", @"bottom", @"center", or any point wrapped in an NSValue object into a CGPoint
        if([point caseInsensitiveCompare:@"top"] == NSOrderedSame) {
            return CGPointMake(self.bounds.size.width/2, (toast.frame.size.height / 2) + CSToastYPos);
        } else if([point caseInsensitiveCompare:@"bottom"] == NSOrderedSame) {
            return CGPointMake(self.bounds.size.width/2, (self.bounds.size.height - (toast.frame.size.height / 2)) - CSToastYPos);
        } else if([point caseInsensitiveCompare:@"center"] == NSOrderedSame) {
            return CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
        }
    } else if ([point isKindOfClass:[NSValue class]]) {
        return [point CGPointValue];
    }
    
    NSLog(@"Warning: Invalid position for toast.");
    return [self centerPointForPosition:CSToastDefaultPosition withToast:toast];
}

- (UIView *)viewForMessage:(NSString *)message title:(NSString *)title hint:(NSString *)hintstr image:(UIImage *)image
{
    // sanity
    if((message == nil) && (title == nil) && (image == nil) && (hintstr == nil)) return nil;
    
    // dynamically build a toast view with any combination of message, title, & image.
    UILabel *messageLabel = nil;
    UILabel *titleLabel = nil;
    UILabel *hintLabel = nil;
    UIImageView *imageView = nil;
    
    
    // create the parent view
    UIView *wrapperView = [[UIView alloc] init];
    wrapperView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    wrapperView.layer.cornerRadius = CSToastCornerRadius;
    
    if (CSToastDisplayShadow) {
        //wrapperView.layer.shadowColor = APP_TOAST_BORDERCOLOR.CGColor;//[UIColor blackColor].CGColor;
        wrapperView.layer.shadowOpacity = CSToastShadowOpacity;
        wrapperView.layer.borderColor = APP_TOAST_BORDERCOLOR.CGColor;
        wrapperView.layer.borderWidth = 2.0f;
        wrapperView.layer.shadowRadius = CSToastShadowRadius;
        wrapperView.layer.shadowOffset = CSToastShadowOffset;
    }
    
    wrapperView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:CSToastOpacity];
    
    if(image != nil) {
        imageView = [[UIImageView alloc] initWithImage:image];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.frame = CGRectMake(CSToastHorizontalPadding, CSToastVerticalPadding, CSToastImageViewWidth, CSToastImageViewHeight);
    }
    
    CGFloat imageWidth, imageHeight, imageLeft;
    
    // the imageView frame values will be used to size & position the other views
    if(imageView != nil) {
        imageWidth = imageView.bounds.size.width;
        imageHeight = imageView.bounds.size.height;
        imageLeft = CSToastHorizontalPadding;
    } else {
        imageWidth = imageHeight = imageLeft = 0.0;
    }
    
    if (title != nil)
    {
        titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = CSToastMaxTitleLines;
        titleLabel.font = AppDefaultFont(CSToastFontSize);
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.textColor = APP_BLACK_COLOR;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.alpha = 1.0;
        titleLabel.text = title;
        
        // size the title label according to the length of the text
		// CGSize maxSizeTitle = CGSizeMake((self.bounds.size.width * CSToastMaxWidth), self.bounds.size.height);
		//CGSize expectedSizeTitle = [title sizeWithFont:titleLabel.font constrainedToSize:maxSizeTitle lineBreakMode:titleLabel.lineBreakMode];

		CGSize expectedSizeTitle = [title sizeWithAttributes:@{NSFontAttributeName: titleLabel.font}];




        titleLabel.frame = CGRectMake(0.0, 0.0, expectedSizeTitle.width, expectedSizeTitle.height);
    }
    
    if (message != nil)
    {
        messageLabel = [[UILabel alloc] init];
        messageLabel.numberOfLines = CSToastMaxMessageLines;
        messageLabel.font = AppDefaultFont(CSToastFontSize);
        messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        messageLabel.textColor = APP_BLACK_COLOR;
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.alpha = 1.0;
        messageLabel.text = message;
        
        // size the message label according to the length of the text
		//  CGSize maxSizeMessage = CGSizeMake((self.bounds.size.width * CSToastMaxWidth), self.bounds.size.height * CSToastMaxHeight);
		// CGSize expectedSizeMessage = [message sizeWithFont:messageLabel.font constrainedToSize:maxSizeMessage lineBreakMode:messageLabel.lineBreakMode];
		CGSize expectedSizeMessage = [message sizeWithAttributes:@{NSFontAttributeName: messageLabel.font }];

        messageLabel.frame = CGRectMake(0.0, 0.0, expectedSizeMessage.width, expectedSizeMessage.height);
    }
    if (hintstr != nil)
    {
        hintLabel = [[UILabel alloc] init];
        hintLabel.numberOfLines = CSToastMaxMessageLines;
        hintLabel.font = AppDefaultFont(CSToastFontSize);
        hintLabel.lineBreakMode = NSLineBreakByWordWrapping;
        hintLabel.textColor = APP_BLACK_COLOR;
        hintLabel.backgroundColor = [UIColor clearColor];
        hintLabel.alpha = 1.0;

        hintLabel.text = hintstr;
        
        // size the message label according to the length of the text
		//CGSize maxSizeMessage = CGSizeMake((self.bounds.size.width * CSToastMaxWidth), self.bounds.size.height * CSToastMaxHeight);
		// CGSize expectedSizeMessage = [hintstr sizeWithFont:hintLabel.font constrainedToSize:maxSizeMessage lineBreakMode:messageLabel.lineBreakMode];


		CGSize expectedSizeMessage = [hintstr sizeWithAttributes:@{NSFontAttributeName:hintLabel.font }];

        hintLabel.frame = CGRectMake(0.0, 0.0, expectedSizeMessage.width, expectedSizeMessage.height);
    }
    
    // titleLabel frame values
    CGFloat titleWidth, titleHeight, titleTop, titleLeft;
    
    if(titleLabel != nil) {
        titleWidth = titleLabel.bounds.size.width+35;
        titleHeight = titleLabel.bounds.size.height;
        titleTop = CSToastVerticalPadding;
        titleLeft = imageLeft + imageWidth + CSToastHorizontalPadding;
    } else {
        titleWidth = titleHeight = titleTop = titleLeft = 0.0;
        titleHeight = 10.0f;  // setting default height
    }
    
    // messageLabel frame values
    CGFloat messageWidth, messageHeight, messageLeft, messageTop;

    if(messageLabel != nil) {

        messageHeight = messageLabel.bounds.size.height;
        messageLeft = imageLeft + imageWidth + CSToastHorizontalPadding;
        messageTop = titleTop + titleHeight;
		 messageWidth =messageLabel.bounds.size.width+35;
    } else {
        messageWidth = messageHeight = messageLeft = messageTop = 0.0;
        messageTop = 10.0f;   // setting default mesage height
    }
    
    // messageLabel frame values
    CGFloat hintLabelWidth, hintLabelHeight, hintLabelLeft, hintLabelTop;
    
    if(hintLabel != nil) {
        hintLabelWidth = hintLabel.bounds.size.width+35;
        hintLabelHeight = hintLabel.bounds.size.height;
        hintLabelLeft = imageLeft + imageWidth + CSToastHorizontalPadding;
        hintLabelTop = messageTop + messageHeight;
    } else {
        hintLabelWidth = hintLabelHeight = hintLabelLeft = hintLabelTop = 0.0;
        hintLabelHeight = 10.0f;
    }

    //CGFloat longerWidth = MAX(titleWidth, messageWidth);
    //CGFloat longerLeft = MAX(titleLeft, messageLeft);
    
    // wrapper width uses the longerWidth or the image width, whatever is larger. same logic applies to the wrapper height
    CGFloat wrapperWidth = (self.bounds.size.width * 0.9);//MAX((imageWidth + (CSToastHorizontalPadding * 2)), (longerLeft + longerWidth + CSToastHorizontalPadding));
    CGFloat wrapperHeight;
    if(hintLabel != nil)
    {
        wrapperHeight = MAX((hintLabelTop + hintLabelHeight + CSToastVerticalPadding), (imageHeight + (CSToastVerticalPadding * 2)));
    }
    else if((hintLabel = nil) && (titleLabel == nil))
    {
        wrapperHeight = MAX((10 + messageHeight + CSToastVerticalPadding), (imageHeight + (CSToastVerticalPadding * 2)));
    }
    else
    {
        wrapperHeight = MAX((messageTop + messageHeight + CSToastVerticalPadding), (imageHeight + (CSToastVerticalPadding * 2)));
    }
    wrapperView.frame = CGRectMake(0.0, 0.0f, wrapperWidth, wrapperHeight);
    
    //cross button
    
    if(titleLabel != nil) {
        titleLabel.frame = CGRectMake(titleLeft, titleTop, titleWidth, titleHeight);
        [wrapperView addSubview:titleLabel];
    }
    
    if(messageLabel != nil) {
        messageLabel.frame = CGRectMake(messageLeft, messageTop, messageWidth, messageHeight);
        [wrapperView addSubview:messageLabel];
    }
    
    if(hintLabel != nil) {
        hintLabel.frame = CGRectMake(hintLabelLeft, hintLabelTop, hintLabelWidth, hintLabelHeight);
        [wrapperView addSubview:hintLabel];
    }
    
    if(imageView != nil)
    {
        [wrapperView addSubview:imageView];
    }
    
    wrapperView.center = [self centerPointForPosition:@"center" withToast:wrapperView];
    //[wrapperView setBackgroundColor:APP_ORANGE_COLOR];
    
    //[imageView addSubview:wrapperView];
    //-----
    UIImageView * closeBtn = [[UIImageView alloc] initWithFrame:CGRectMake(wrapperView.frame.size.width - 35.0f, wrapperView.frame.size.height/2-11, 22.0f, 22)];
    [closeBtn setImage:[UIImage imageNamed:@"cancel@1x.png"]];
    [closeBtn setUserInteractionEnabled:YES];

    [wrapperView addSubview:closeBtn];
    
    //[wrapperView bringSubviewToFront:closeBtn];
    wrapperView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideToastView:)];
    tapGesture.cancelsTouchesInView = NO;
   
   // [tapGesture setNumberOfTouchesRequired:1.0f];
    [wrapperView addGestureRecognizer:tapGesture];
    
    return wrapperView;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	return YES;
}



-(void) hideToastView:(UIGestureRecognizer *) tap
{
	if(tap.view)
	{
		[tap.view removeFromSuperview];
	}
}

- (IBAction)dismissView:(id)sender
{
    [self hideToastActivity];
}

@end
