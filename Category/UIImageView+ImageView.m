//
//  UIImageView+ImageView.m
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "UIImageView+ImageView.h"

@implementation UIImageView (ImageView)

-(UIImageView*)makeBorderImageView:(UIImageView*)imageView toColor:(UIColor*)color toWidth:(NSInteger)width
{
    UIImageView *tempimageView = imageView;
    
    tempimageView.layer.borderWidth = width;
    tempimageView.layer.borderColor = color.CGColor;
    
    return tempimageView;
}

-(UIImageView*)imageViewCornerRadius:(UIImageView*)imageView toRadius:(NSInteger)radius
{
    UIImageView *tempimageView = imageView;
    
    tempimageView.layer.cornerRadius = radius;
    tempimageView.layer.masksToBounds = YES;
    
    return tempimageView;
}

@end
