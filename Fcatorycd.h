//
//  Fcatorycd.h
//  VIPCard
//
//  Created by Syntel-Amargoal1 on 11/9/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Fcatorycd : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Fcatorycd+CoreDataProperties.h"
