//
//  BussinessCustomCell.h
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface BussinessCustomCell : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bussinessImg;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *baseView;

@end
