//
//  UserLoginVC.h
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"

@interface UserLoginVC : BaseViewController<ServerResponseDelegate>


//"TITLE_DONT_HAVE_CARD"    = "אין לי כרטיס‎";
//"TITLE_HAVE_CARD"    = "יש לי כרטיס‎";
//"BUTTON_TITLE_LOGIN"     =  "התחבר";
//"TITLE_PLEASE_SIGN_IN"    =   "למשתמש רשום\n אנא לחץ התחבר‎";
//"TITLE_TICKET"   = "מספר כרטיס‎";
//"TITLE_CHECKOUT"   =  "בדוק‎";

@property (strong, nonatomic) IBOutlet UIImageView *img_Dont_have_card;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Dont_have_card;
@property (strong, nonatomic) IBOutlet UIButton *btnDontHaveCard;
@property (strong, nonatomic) IBOutlet UIButton *btnHaveCard;
@property (strong, nonatomic) IBOutlet UIImageView *img_Have_Card;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Have_Card;
@property (strong, nonatomic) IBOutlet UIButton *btn_Login;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Please_SignIn;
@property (strong, nonatomic) IBOutlet UIView *viewHaveYouCard;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Titcker_Card;
@property (strong, nonatomic) IBOutlet UITextField *txtTicketNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckOut;

- (IBAction)btn_login:(id)sender;

-(IBAction)action_haveCard:(id)sender;
-(IBAction)action_dontHaveCard:(id)sender;
-(void)memberloginAPI;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraints_HaveCardHeight;


@end
