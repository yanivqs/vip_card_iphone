//
//  WSBussinessObj.h
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

/*
 id : "10"
 name : "אסיאתי"
*/

@interface WSBussinessObjResponse : WSBaseData
@property (nonatomic, strong) NSString *id, *name;

@property (nonatomic, strong) NSArray *bussinessList;

//-(id)initWithDictionary:(NSDictionary *)dict;

@end
