//
//  OrderingDetailsObj.h
//  VIPCard
//
//  Created by Vishal Kolhe on 01/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderingDetailsObj : NSObject

/*
 "IdUser": "0",
 "Phone": "054-66767676",
 "IdBusiness": "0",
 "totalPrice": "9000",
 "DeliveryPrice": "0",
 "TakeAwayOrDelivery": "\u0000",
 "CommentOnOrder": null,
 "IdAddress": "1",
 "CommentsShipping": null,
 "IdPayment": "0",
 "DateOrder": null,
*/


@property (nonatomic, strong) NSString *bussinessID, *totalAmount, *deliveryPrice, *TakeAwayOrDelivery, *CommentOnOrder, *idAddress,*priceDiscount,*priceBeforeDiscount;

//shilpa added
@property (nonatomic, strong) NSString *totalAfterDiscount;
@end
