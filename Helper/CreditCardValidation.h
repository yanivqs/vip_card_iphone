//
//  CreditCardValidation.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 08/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CreditCardValidation : NSObject

- (BOOL) validateCard:(NSString*) cardno withType:(int ) type;

@end