//
//  CategoryForMealCell.m
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#define SHEKEL @"₪"
#import "CategoryForMealCell.h"
#import "UIImageView+WebCache.h"
#import "AppGlobalConstant.h"
#import "WSConstants.h"

@implementation CategoryForMealCell

-(void)populateData:(WSMealsResponse *)aData
{
    [self.lblDesc setText:aData.description];
    [self.lblName setText:aData.name];
    [self.lblPrice setText:[NSString stringWithFormat:@"%@%@",SHEKEL,aData.price]];
    
    NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImagesMealsDeals,aData.image]];
    
    if(aData.image.length > 0)
        [self.mealImg sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",PLACE_HOLDER_IMAGE]]];
    else
        [self.mealImg setImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
    
    
}

-(void) adjustConstraint
{
    [self.lblName sizeToFit];
    CGFloat tmpH = CGRectGetHeight(self.lblName.frame);
    if(IS_OS_8_OR_LATER)
    {
        
        self.con_lblDecConstraints.active = TRUE;
        self.con_lblNameConstraints.active = TRUE;
        
    }
    
    if (tmpH > 21)
        self.con_lblNameConstraints.constant = tmpH;
    else
        self.con_lblNameConstraints.constant = 21.0;
    
    [self.lblDesc sizeToFit];
    tmpH = CGRectGetHeight(self.lblDesc.frame);
    
    // CGSize toName = [self getStringSize:cell.lblName andString:cell.lblName.text];
    // CGSize toDes = [self getStringSize:cell.lblDesc andString:cell.lblDesc.text];
    
    if (tmpH > 21)
        self.con_lblDecConstraints.constant = tmpH;
    else
        self.con_lblDecConstraints.constant = 21.0;
    
    // @Sarika
    /*
    CGSize size = [self.lblDesc.text sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont fontWithName:@"Helvetica Neue" size:17.0f]}];
    
    //adjust the label the the new height.
    CGRect newFrame = self.lblDesc.frame;
    newFrame.size.height = size.height;
    self.lblDesc.frame = newFrame;*/
    
    
     self.lblDesc.lineBreakMode = NSLineBreakByWordWrapping;
     self.lblDesc.numberOfLines = 0;
    
    [self setNeedsUpdateConstraints];
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    [self adjustConstraint];
}

@end
