//
//  AddressObj.m
//  VIPCard
//
//  Created by Vishal Kolhe on 01/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "AddressObj.h"

@implementation AddressObj

/*
 NumberStreet": "100",
 "Floor": "200",
 "NumberApartment": "2300",
 "Entrance": "200",
 "DrivingDirections": null,
 "street": "העצמאות",
 "town": "יבנה"
*/

@synthesize NumberApartment, Floor, NumberStreet, Entrance, description, street, town;

@end
