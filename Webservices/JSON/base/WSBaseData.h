//
//  WSBaseData.h
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NSString+CommonForApp.h"
#import "WSConstants.h"

@interface WSBaseData : NSObject
{
    NSArray *ignoreProperties;
}

@property (nonatomic,retain) NSArray *ignoreProperties;
- (id) initWithDictionary:(NSDictionary *)dict;
-(void) parseWithDictionary:(NSDictionary *)dict;
- (void)populateFromDictionary:(NSDictionary *)dict;
- (NSMutableDictionary *)getDictionaryFromObject;
-(id)getCoreDataFromObject:(id) coreData;
-(void) populateObjectFromCorData:(id)coreData;
@end
