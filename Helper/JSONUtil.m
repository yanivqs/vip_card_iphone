//
//  JSONUtil.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 18/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "JSONUtil.h"

@implementation JSONUtil

+(NSString*)dicToJSON:(NSDictionary*)dic
{
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:&err];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return myString;
}
+(NSDictionary*)JSONStrToDic:(NSString*)jsonStr
{
    NSError * err;
    NSDictionary *response = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&err];
    return response;
}
@end
