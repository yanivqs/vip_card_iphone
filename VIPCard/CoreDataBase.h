//
//  CoreDataBase.h
//  ReceiptLess
//
//  Created by SanC on 10/03/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DateTimeUtil.h"
#import "CustomError.h"
//#import "CoreDataConstant.h"
#import <CoreData/CoreData.h>
//#import "AppSharedDataCD.h"
#import "WSConstants.h"
#import <sqlite3.h>





@interface CoreDataBase : NSObject
{
	//shilpa

	//NSString *databasePath;
	//---------//



}
//shilpa
//@property (nonatomic, strong) NSString *documentsDirectory;
//@property (nonatomic, strong) NSString *databaseFilename;
//
//-(void)copyDatabaseIntoDocumentsDirectory;
//
//+(CoreDataBase*)getSharedInstance;
//-(BOOL)createDB;
//-(BOOL) saveData:(NSString*)token;

//-----------------------//


//Static
+(CoreDataBase *) sharedInstanse;
+(NSManagedObjectContext *) managedObjectContext;
+(NSNumber *) getUniqueRecordID;

//access by instane
-(NSManagedObjectContext *) getManageObjectContext;

-(id) newRecordFromDB:(NSString *) tblName;

-(id) getRecordFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate;

-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate;
-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate
                 withLimit:(NSUInteger) limit;
                 
-(NSNumber *) getMaxValue:(NSString *) tblName forField:(NSExpressionDescription *)expressionDescription  withPredicate:(NSPredicate *) predicate;
-(NSNumber *) getCountValue:(NSString *) tblName withfieldName:(NSString *) fieldName withPredicate:(NSPredicate *) predicate;
-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate sortDescriptor:(NSArray *) sortDescriptors;

-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate withColums:(NSArray *) columns;



+(BOOL) saveManagedObject:(NSManagedObject *) rec;
+(BOOL) deleteManagedObject:(NSManagedObject *) rec;

-(NSArray*) getUniqueValues:(NSString*) tblName forField:(NSString *) field withPredicate:(NSPredicate *) predicate;

-(void) cleanTableData:(NSString *) tableName withPredicate:(NSPredicate *) predicate;

//Store logged user information
//-(AppSharedDataCD *) lastLoggedUser;

-(void) saveUserName:(NSString *) userName pass:(NSString *) passwd token:(NSString *) token userid:(NSString *) userid;

-(void) cleanUserSpecificData;
- (BOOL)resetLastLoggedUser;


@end
