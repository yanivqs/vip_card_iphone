//
//  CardData.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 22/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CardData.h"

@implementation CardData

-(id) init{
    if(self=[super init])
    {
        self.cardno = @"";
        self.cardtype = @"";
        self.ownid = @"";
        self.ownname = @"";
        self.expyr = @"";
        self.expmon = @"";
        self.ordamt = @"";
         self.cvv = @"";
    }
    
    return self;
}

@end
