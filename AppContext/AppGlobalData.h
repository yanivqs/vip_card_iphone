//
//  AppContext.h
//  ReachMe
//
//  Created by SanC on 23/12/15.
//  Copyright (c) 2015 BASquare. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBussiness.h"
#import "WSMealsResponse.h"
@class WSBussiness;


@interface AppGlobalData : NSObject
+ (id)sharedManager;




//Business card related data
@property (nonatomic, strong) NSMutableArray *listOfCartItems;



-(void) addItemToCart:(NSString *) bussinessId cartItem:(id) cartItem;

-(NSMutableArray *) getBussinessCart:(NSString *)businessId;

-(void) clearBusinessCart:(NSString *)businessId;

-(void) removeFromBusinessCart:(NSString *) businessId cartItem:(id) cartItem;


-(void) addBusinessData:(WSBussiness *) business withKey:(NSString *) businessId;
-(WSBussiness *) getBusinessDataByKey:(NSString *) businessId;


@end
