//
//  HomeGalleryResponse.h
//  VIPCard
//
//  Created by SanC on 14/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBaseData.h"

@interface HomeGalleryResponse : WSBaseData
@property (strong ,nonatomic) NSString *id,*name,*NewName,*IdBusiness,*PathDirectory,*TypeGallery;
@end
