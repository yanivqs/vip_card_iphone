//
//  CustomImgView.m
//  VIPCard
//
//  Created by SanC on 29/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CustomImgView.h"
#import "UIImageView+WebCache.h"


@interface CustomImgView()


@property(nonatomic,strong) NSString *imgName;

@property(nonatomic,strong) NSURL *imageUrl;



@end

@implementation CustomImgView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype) initWithImage:(NSString *) img imgUrl:(NSURL *)url
{
	self = [super init];
	if(self)
	{
		self.imgName = img;
		self.imageUrl = url;
		//attach event
		[self setImageOnView];
		UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
		[self  addGestureRecognizer:singleTap];
		self.userInteractionEnabled = YES;
	}

	return self;

}

-(void) showInView:(UIView *) parentView{


	if(!parentView)
	{

		UIWindow *window = [[UIApplication sharedApplication] keyWindow];
		self.frame = window.bounds;
		[window addSubview:self];
		return;

	}

	self.frame = parentView.bounds;
	[parentView addSubview:self];
}


-(void)setImageOnView
{
	self.contentMode = UIViewContentModeScaleAspectFit;
	self.backgroundColor=[UIColor whiteColor];

	[self sd_setImageWithURL:self.imageUrl placeholderImage:[UIImage imageNamed:self.imgName]];

//	if (self.imgName)
//    {
//		self.image= [UIImage imageNamed:self.imgName];
//	}
//	else
//	{
//		[self sd_setImageWithURL:self.imageUrl];
//		
//	}
}

-(void) tapDetected:(UIGestureRecognizer *) gesture
{
	[self removeFromSuperview];
}



@end
