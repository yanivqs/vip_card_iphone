//
//  WSBaseRequest.h
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSResponseHeader.h"

@interface WSBaseRequest : NSObject
{
    NSDictionary *requestParams;
}

+(void) setLoggedUserHeader:(WSResponseHeader *) resHeader;
@property (nonatomic, strong)  NSDictionary *requestParams;

-(NSMutableDictionary *) getRequestData;
-(id)initWithParameters:(NSDictionary *) params;

@end