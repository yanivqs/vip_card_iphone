//
//  AppContext.m
//  ReachMe
//
//  Created by SanC on 23/12/15.
//  Copyright (c) 2015 BASquare. All rights reserved.
//

#import "AppGlobalData.h"
#import "WSMealsResponse.h"
#import "WSBussiness.h"

@interface AppGlobalData()

@property (nonatomic,strong) NSMutableDictionary *dictBussinessCart;

@property (nonatomic,strong) NSMutableDictionary *dictBussinessData;

@end

@implementation AppGlobalData
@synthesize listOfCartItems;

@synthesize dictBussinessCart = _dictBussinessCart;

#pragma mark Singleton Methods

+ (id)sharedManager {
	static AppGlobalData *sharedMyManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedMyManager = [[self alloc] init];

	});
	return sharedMyManager;
}

- (id)init {
	if (self = [super init]) {
		self.dictBussinessCart = [NSMutableDictionary dictionary];
		self.dictBussinessData = [NSMutableDictionary dictionary];
		//self.listOfCartItems = [[NSMutableArray alloc] init];

	}
	return self;
}




-(void) addItemToCart:(NSString *) bussinessId cartItem:(id) cartItem
{
	NSMutableArray * arr = [self.dictBussinessCart objectForKey:bussinessId];
	if(arr == nil)
	{
		arr = [NSMutableArray array];
		[self.dictBussinessCart setObject:arr forKey:bussinessId];
	}

	[arr addObject:cartItem];

}

-(NSMutableArray *) getBussinessCart:(NSString *)businessId
{
	NSMutableArray * arr = [self.dictBussinessCart objectForKey:businessId];
	if(arr == nil)
	{
		arr = [NSMutableArray array];
		[self.dictBussinessCart setObject:arr forKey:businessId];

	}

	return arr;
}

-(void) clearBusinessCart:(NSString *)businessId
{
	[self.dictBussinessCart removeObjectForKey:businessId];
}

-(void) removeFromBusinessCart:(NSString *) businessId cartItem:(id) cartItem
{
	NSMutableArray * arr = [self.dictBussinessCart objectForKey:businessId];
	if(arr)
	{
		[arr removeObject:cartItem];
	}

}


/**
 Bussines data
 */
-(void) addBusinessData:(WSBussiness *) business withKey:(NSString *) businessId
{
	[self.dictBussinessData setObject:business forKey:businessId];
}

-(WSBussiness *) getBusinessDataByKey:(NSString *) businessId
{
 return [self.dictBussinessData objectForKey:businessId];
}





@end
