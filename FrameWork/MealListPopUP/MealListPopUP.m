//
//  MealListPopUP.m
//  VIPCard
//
//  Created by SanC on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#define CheckedImage    @"checkbox_clicked"
#define UnCheckImage    @"checkbox"
#import "MealListPopUP.h"


@implementation MealListPopUP
@synthesize extraForMealsObj;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect {
	// Drawing code
	//self.backgroundColor=[UIColor clearColor];
}

- (instancetype) initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	self = [[[UINib nibWithNibName: @"MealListPopUP"
							bundle: [NSBundle bundleForClass: [MealListPopUP class]]]

			 instantiateWithOwner: nil
			 options: nil] firstObject];

	self.frame = frame;
	[self papulateData];
	return self;
}

-(void)papulateData
{
	gesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
	[internalView addGestureRecognizer:gesture];
	[internalView setUserInteractionEnabled:TRUE];
	

}

#pragma mark-UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return listOfMeals.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";

	CatForMenuDataCell *cell =(CatForMenuDataCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil)
	{
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CatForMenuDataCell" owner:self options:nil];
		cell = [nib objectAtIndex:0];

	}
	//tableView.backgroundColor=[UIColor clearColor];
	tableView.separatorColor=[UIColor clearColor];
	[cell.imgCheckBox setHidden:!maelOrExtra];
	cell.backgroundColor=[UIColor whiteColor];

	WSExtraMealsResponse *extraMeals = [listOfMeals objectAtIndex:indexPath.row];

	if(maelOrExtra)
	{
		if([listOfItems containsObject:extraMeals])
		{
			[cell.imgCheckBox setImage:[UIImage imageNamed:CheckedImage]];
		}
		else
		{
			[cell.imgCheckBox setImage:[UIImage imageNamed:UnCheckImage]];
		}
	}
	[cell populateData:extraMeals Forprice:[extraForMealsObj.price intValue]];
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	WSExtraMealsResponse *mealResponse = [listOfMeals objectAtIndex:indexPath.row];
	[self submitData:mealResponse];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CatForMenuDataCell *cell = (CatForMenuDataCell*) [self tableView:tableView cellForRowAtIndexPath:indexPath];
	WSExtraMealsResponse *extraMeals = [listOfMeals objectAtIndex:indexPath.row];

		CGSize size = [self getStringSize:cell.lblName andString:extraMeals.Name];
	if (size.height>39)
    {
		return size.height;
	}
	else
		return 39;
}

#pragma mark-UserDefineMethods
-(CGSize)getStringSize:(UILabel *)aLabel andString:(NSString *)aStr
{
	CGSize expectedLabelSize;
	if(!aStr ||[aStr isEqualToString:@""])
		return expectedLabelSize;
	NSMutableString *myString = [[NSMutableString alloc] initWithString:aStr];

	CGSize maximumLabelSize = CGSizeMake(aLabel.frame.size.width,9999);

	expectedLabelSize = [myString sizeWithFont:aLabel.font
							 constrainedToSize:maximumLabelSize
								 lineBreakMode:aLabel.lineBreakMode];

	//adjust the label the the new height.
	CGRect newFrame = aLabel.frame;
	newFrame.size.height = expectedLabelSize.height;
	aLabel.frame = newFrame;
	return expectedLabelSize;
}


- (IBAction)onClick_SubmitData:(id)sender
{
	if(listOfItems.allObjects.count)
	{
		[self.delegate selectedListofItems:listOfItems.allObjects];
		[self removeFromSuperview];
	}
}

#pragma mark-UserDefineMethods
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
	[self removeFromSuperview];
}

-(void)submitData:(WSExtraMealsResponse *)aData
{
	if(maelOrExtra)
	{
		if([listOfItems containsObject:aData])
		{
			[listOfItems removeObject:aData];
		}
		else
		{
			[listOfItems addObject:aData];
		}

		[self.tableView reloadData];
	}
	else
	{
		[self.delegate selectedItem:aData];
		[self removeFromSuperview];
	}
}

-(void)refreshMealList
{
	self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	btnSubmit=[UIButton buttonWithType:UIButtonTypeCustom];
	listOfMeals = extraForMealsObj.extraMealsList;
	maelOrExtra = [extraForMealsObj.maelOrExtra boolValue];

	if(!maelOrExtra)
		btn_Submit.hidden = YES;
	else
		btn_Submit.hidden=NO;
	listOfItems = [NSMutableSet set];
	CGFloat height = 42;
	float h = height * [listOfMeals count];
	if(h > 410.0)
	{
		self.tableView.frame = CGRectMake(0, 50, self.tableView.frame.size.width, 410);
	}
	else
	{
		self.tableView.frame = CGRectMake(0, 50, self.tableView.frame.size.width, h);
	}
	NSLog(@"h:%f", h);

	if(maelOrExtra)
	{
		if(extraForMealsObj.selectedExtraMealsResponseList != nil)
		{
			for (id item in extraForMealsObj.selectedExtraMealsResponseList) {

				[listOfItems addObject:item];
			}

		}
	}
	else {
		if (extraForMealsObj.selectedExtraMealsResponse != nil)
		{
			[listOfItems addObject:extraForMealsObj.selectedExtraMealsResponse];
		}
	}

	[self.tableView reloadData];
}
@end
