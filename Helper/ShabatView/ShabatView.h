//
//  ShabatView.h
//  VIPCard
//
//  Created by SanC on 18/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionsManager.h"
#import "DejalActivityView.h"
#import "KeyConstant.h"

@interface ShabatView : UIView<ServerResponseDelegate>
{
	NSTimer *timer;
	int currSeconds;
}

@property (nonatomic, retain) IBOutlet UIImageView *img_View;
@property(nonatomic,strong) NSString *imgName;
@property(nonatomic,strong) NSURL *imageUrl;
-(void) showInView:(UIView *) parentView;
-(void)setImageOnView;





@end
