//
//  CatForMenuDataCell.m
//  VIPCard
//
//  Created by SanC on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CatForMenuDataCell.h"
#import "AppGlobalConstant.h"


@implementation CatForMenuDataCell

- (void)awakeFromNib {
    // Initialization code
	}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
-(void)populateData:(WSExtraMealsResponse *)extraMealObj Forprice:(int)price
{
	[self.lblName setText:extraMealObj.Name];
	int totalPrice=[extraMealObj.price intValue]+price;

	if(totalPrice > 0)
	{
		[self.lblPrice setText:[NSString stringWithFormat:@"%@%@",SHEKEL, [NSString stringWithFormat:@"%d", totalPrice]]];
		;
	}
	else
	{
		[self.lblPrice setText:@""];
	}
    
    
    //[self.lblName sizeToFit];
    //CGRect f = self.lblName.frame;
   // self.constaintLblNameHeight.constant = f.size.height;
    
    
    
}

-(void) toggleCheckBox:(BOOL)flag
{
    [self.imgCheckBox setHidden:flag];
    if(flag)
        self.constaintCheckBoxWidth.constant = 0.f;
    else
        self.constaintCheckBoxWidth.constant = 20.f;
}


@end
