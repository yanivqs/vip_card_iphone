//
//  DODcell.m
//  VIPCard
//
//  Created by Vishal Kolhe on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "DODcell.h"
#import "WSAdditionalExtrasResponse.h"

@implementation DODcell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) updateCheckBox:(BOOL) isSelected
{
    if(isSelected)
    {
        [self.btnCheckBox setImage:[UIImage imageNamed:@"checkbox_clicked"] forState:UIControlStateNormal];
        
    }
    else
    {
        [self.btnCheckBox setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
}


- (IBAction)lbl_checkBox:(id)sender {
    self.cellData.selected = !self.cellData.selected;
    [self updateCheckBox:self.cellData.selected];
    //now trigger Delegate
    if(self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(dodCell:onSelectionChange:)])
    {
        [self.cellDelegate dodCell:self onSelectionChange:self.cellData];
    }
    
}
@end
