//
//  DODcell.h
//  VIPCard
//
//  Created by Vishal Kolhe on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WSAdditionalExtrasResponse;

@class DODcell;

@protocol DODCellDelegate <NSObject>

-(void) dodCell:(DODcell *) dodCell onSelectionChange:(WSAdditionalExtrasResponse *) aData;

@end

@interface DODcell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_priceShekel;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Name;
- (IBAction)lbl_checkBox:(id)sender;
@property (nonatomic,strong) WSAdditionalExtrasResponse *cellData;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;

@property(nonatomic,strong) id<DODCellDelegate> cellDelegate;

@property(nonatomic,assign) NSInteger cellIndex;

-(void) updateCheckBox:(BOOL) isSelected;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintLblNameHeight;

@end
