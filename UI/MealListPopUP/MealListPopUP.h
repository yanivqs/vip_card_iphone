//
//  MealListPopUP.h
//  VIPCard
//
//  Created by SanC on 09/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSExtraForMealsResponse.h"
#import "CatForMenuDataCell.h"

@class MealListPopUP;

@protocol MealListPopUPDelegate <NSObject>

-(void) mealListPopup:(MealListPopUP *)mealListPopup selectedItem:(id)item;
-(void)mealListPopup:(MealListPopUP *)mealListPopup selectedListofItems:(NSArray *)aList;

@end

@interface MealListPopUP : UIView
{
	NSArray *listOfMeals;
	NSMutableSet *listOfItems;
	BOOL maelOrExtra;
	int price;
	IBOutlet UIButton *btn_Close;
	UITapGestureRecognizer *gesture;
	__weak IBOutlet UIButton *btn_Submit;
	UIButton *btnSubmit;
	__weak IBOutlet UIView *internalView;

}
@property (strong, nonatomic) IBOutlet UIView *contentViewHolder;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (assign, nonatomic) id<MealListPopUPDelegate> delegate;
@property (nonatomic, strong) WSExtraForMealsResponse *extraForMealsObj;
@property (weak, nonatomic) IBOutlet UILabel *lblExtraDesc;
@property (nonatomic)BOOL isChild;

- (IBAction)onClick_Close:(id)sender;

- (IBAction)onClick_SubmitData:(id)sender;
-(void)refreshMealList;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintSubmitHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintMainViewHeight;

+(MealListPopUP *) loadFromNibWithFrame:(CGRect) frame;

@end
