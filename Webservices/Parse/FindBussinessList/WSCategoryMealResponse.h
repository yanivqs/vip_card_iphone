//
//  WSCategoryMealResponse.h
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSCategoryMealResponse : WSBaseData
//"id":"1",
//"idBusiness":"1",
//"idCategorySub":"1",
//"description":"1",
//"meals":[]
//name = "\U05de\U05d2\U05e9 \U05d0\U05d9\U05e9\U05d9";

@property (strong , nonatomic) NSString *id, *idBusiness,*idCategorySub,*description,*name;
@property (strong , nonatomic) NSArray *mealsList;
@end
