//
//  AddressObj.h
//  VIPCard
//
//  Created by Vishal Kolhe on 01/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressObj : NSObject
@property (nonatomic, strong) NSString *NumberStreet, *Floor, *NumberApartment, *Entrance, *DrivingDirections, *street, *town;

@end
