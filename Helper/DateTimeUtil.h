//
//  DateTimeUtil.h
//  ForumApp
//
//  Created by SanC on 05/03/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+CommonForApp.h"

@interface DateTimeUtil : NSObject

+(NSDate *) localDateFromServerDateString:(NSString *) dateString;
+(NSDate *) serverDateFromLocalDate:(NSDate *) date;

+(NSString *) stringFromDateTime:(NSDate *) date withFormat:(NSString *) aFormat;
+(NSDate *)  dateFromStringDate:(NSString *) date withFormat:(NSString *) aFormat;

+(NSString *) getTimeLabel:(int) timeDiff;
+(NSDate *) getZeroTZDate;
+(NSString *) getZeroTZDateStr;
+(NSString *) getGMTStringDateFrom:(NSDate *) date;
+(NSDate *) getGMTDateFromStrDate:(NSString *) date;

+(NSString *) serverDateFromDisplayDate:(NSDate *) date;
+(NSString *) serverDateFromDisplayDateStr:(NSString *) date;
+(NSString *) displayDateFromServerDate:(NSDate *) date;
+(NSString *) displayDateFromServerDateStr:(NSString *) date;

//GMT to local date
+(NSDate *) localDateFromGMTDate:(NSDate *) date;
+(NSString *) displayDateTimeFromServerDate:(NSDate *) date;
+(NSString *) displayDateTimeFromServerDateStr:(NSString *) date;
+(NSDate *) getTodaysDate;
+(NSDate *) getDateAfterAddDays:(NSInteger) day;
+(NSString *) displayDateFromServerGraphDateStr:(NSString *) date;
//+(NSString *)reverseString:(NSString *)string;
+(NSDate *) weekStartDate;
+(NSDate *) weekEndDate;
+(NSDate *) monthStartDate;
+(NSDate *) monthEndDate;

+(NSDate *) dayStartDate:(NSDate *) date;
+(NSDate *) dayEndDate:(NSDate *) date;

+(NSString *)dayNameFromDate:(NSDate *) date;
+(NSString *)dayFromDate:(NSDate *) date;
+(NSString *)monthsFromDate:(NSDate *)date;
+(NSString *)yearFromDate:(NSDate *)date;
+(NSDate *) getDateForHours:(NSInteger) hour;
+(NSDate *) getDateAfterAddHours:(NSInteger) hour minute:(NSInteger)min date:(NSDate*)finaldate;
+(NSDate *) getDateAfterAddOneDay:(NSInteger) day hour:(NSInteger)hour;
+(NSDate *) gecrrDate;

+(NSDate *) getDateBeforeYears:(NSInteger) year;
+(BOOL)validateTime:(NSString*)time1 time:(NSString*)time2;
+(NSMutableArray*)getYears:(NSDate*)date;
+(NSDictionary*)getMonths;

+(NSInteger) convertIntoSeconds:(NSInteger)h m:(NSInteger)m s:(NSInteger)s ;
+(NSInteger)currentTimeOfDayFromDate:(NSDate *)date;

@end
