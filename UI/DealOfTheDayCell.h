//
//  DealOfTheDayCellCollectionViewCell.h
//  VIPCard
//
//  Created by Andrei on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHCStrikethroughLabel.h"

@interface DealOfTheDayCell : UICollectionViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *lblLimitationTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblShortTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblLongDesTitle;

@property (strong, nonatomic) IBOutlet UIView *viewBottom;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;
@property (strong, nonatomic) IBOutlet UIImageView *imgProduct;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;

@property (strong, nonatomic) IBOutlet UILabel *lblDiscription1;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscription2;
@property (strong, nonatomic) IBOutlet UILabel *lblDiscription3;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet SHCStrikethroughLabel *lblPrice2;
@property (strong, nonatomic) IBOutlet UIButton *btnCopouButton;
@property (strong, nonatomic) IBOutlet UIButton *btnWaze;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIScrollView *subScrollview;
@property (strong, nonatomic) IBOutlet UIView *subView;

@property (strong, nonatomic) IBOutlet UIImageView *imgBarcode;
@property (strong, nonatomic) IBOutlet UILabel *lblBarcodeText;
@property (strong, nonatomic) IBOutlet UIView *barcodeView;

@property (assign, nonatomic)  CGSize sContentSize;

//Constaint Height
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *titleConstaintHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *barcodeViewHeightConstraints;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *discription1ConstraintHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *discription2ConstraintHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *discription3ConstraintHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_SubViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_SubviewWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consBottomView_BottomCons;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_bottomViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_lblBarcodeHeight;
@property (strong, nonatomic) IBOutlet UIImageView *img_Expired;


@end
