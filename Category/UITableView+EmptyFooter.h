//
//  UITableView+EmptyFooter.h
//  VIPCard
//
//  Created by Andrei on 31/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppGlobalConstant.h"

@interface UITableView (EmptyFooter)
-(void) customizeTableView;
@end
