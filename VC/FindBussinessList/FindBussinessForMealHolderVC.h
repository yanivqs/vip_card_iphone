//
//  FindBussinessForMealHolderVC.h
//  VIPCard
//
//  Created by SanC on 13/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSFindBussinessListResponse.h"
#import "WSCategoryFilterDetailResponse.h"
#import "BaseViewController.h"

@interface FindBussinessForMealHolderVC : BaseViewController

@property(strong,nonatomic) WSFindBussinessListResponse *findBussinessResponse;
@property(strong,nonatomic) WSCategoryFilterDetailResponse *findcategoryResponse;

@end
