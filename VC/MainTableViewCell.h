//
//  MainTableViewCell.h
//  VIPCard
//
//  Created by fis on 20/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSCouponsResponse.h"
#import "SHCStrikethroughLabel.h"


@interface MainTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UILabel *nameBussiness;
@property (weak, nonatomic) IBOutlet UILabel *discPrice;
@property (weak, nonatomic) IBOutlet SHCStrikethroughLabel *mainPrice;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_NameHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_DescriptionHeight;

-(void)populateData:(WSCouponsResponse *)aData andState:(BOOL)aStatus;
-(void)populateDataBussinessList:(WSCouponsResponse *)dealResponse andState:(BOOL)aStatus;
@property (strong, nonatomic) IBOutlet UILabel *lbl_SalesType;

//-------
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintSaleTypeHEight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintLblNameHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintLblDescHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintDiscPriceHeight;

@end
