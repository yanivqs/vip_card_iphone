//
//  WSConstants.h
//  VIPCard
//
//  Created by Vishal Kolhe on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#ifndef WSConstants_h
#define WSConstants_h


#endif /* WSConstants_h */

//Old URL

//#define ApplicationBaseUrl      @"http://test.enovate-it.com/vipCards/api/"
//#define ApplicationBaseUrl      @"http://91.228.127.188/vipcards/api/"//New URL-30/05/2016 comment 20 july
//#define ApplicationBaseUrl      @"91.228.127.188/vipCards/api/"
#define ApplicationBaseUrl  @"http://91.228.127.19/vipcards/api/"  //chages made in 20 july


#define StaggingBaseUrl         @"http://staging.goldchain.biz/"

#define kCouponsList @"coupons"


//Methods
#define mFetchCoupons   @"fetchCoupons"
#define mFetchDeals     @"fetchDeals"
#define mGetDealOfDay   @"fetchDayDeals"
#define mFetchBussinessList @"fetchBusinesslist"
#define mFetchCatMealsForBussiness      @"fetchCatMealForBusiness"
#define mFetchFindBusinessList @"fetchFindBusinesslist"
#define mFetchFilterList @"fetchFindFilterlist"
#define mFetchTowns  @"fetchTowns"
#define mFetchStreets @"fetchStreets"
#define mFetcategoryFilters @"fetchCategoryFilters"
#define mFetchcategoryFilterDetail @"fetchCategoryFiltersBusiness"
#define mFetchCatTopProduct   @"fetchCatTopProduct"
#define mfetchMarketDeal @"fetchMarketDeal"
#define mSubmitOrder    @"saveOrder"
#define mFetchMealForBusinessByCatSub @"fetchMealForBusinessByCatSub"
#define mIsShabat @"isShabat"
#define mMarkBusinessCallOrder @"markBusinessCallOrder"


//Manoj
#define mGetFactorylist @"fetchFactory"

//Image URL
//#define mFetchImages @"https://fs.kamatera.com/index.php/s/TdXaaG8iTqCgeux/download?path=%2F&files="

//#define mFetchImagesBYWeb @"https://fs.kamatera.com/index.php/s/TdXaaG8iTqCgeux/download?path=%2F"
//#define mFetchImagesBYWeb  @"http://91.228.127.188/photos/"//july 22 comment
#define mFetchImagesBYWeb  @"http://91.228.127.19/Photos/"//july 22 replace



//#define mFetchImages @"http://91.228.127.188/photos/"//july 22 comment
#define mFetchImages @"http://91.228.127.19/Photos/"//july 22 replace



//#define mFetchMarketImages @"http://91.228.127.188/photos/marketDeal/"//july 22 comment
#define mFetchMarketImages @"http://91.228.127.19/Photos/marketDeal/"//july 22 replace




//Image URL for Meals & Delas
//#define mFetchImagesMealsDeals @"http://91.228.127.188/photos/"//july 22 comment
#define mFetchImagesMealsDeals @"http://91.228.127.19/Photos/"//july 22 replace


#define kText @"https://twitter.com/shu223"

#define entryImgURL @"http://91.228.127.19/Photos/entery/entery.png"
#define shabatImgURL @"http://91.228.127.19/Photos/shabat/shabatshalom.jpg"

#define kTableMyOrders @"MyOrders"

//Manoj
#define kTableFactoryCD @"FactoryCD"
