//
//  WSTownsResponse.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBaseData.h"

@interface WSTownsResponse : WSBaseData
//id : "1"
//name : "יבנה"

@property (strong ,nonatomic) NSString *id, *name;
@end
