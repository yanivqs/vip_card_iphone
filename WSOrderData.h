//
//  WSOrderData.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 17/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"
#import "MyOrders+CoreDataProperties.h"

@interface WSOrderData : WSBaseData
{
    MyOrders *myOrdersObj;
}
@property (nullable, nonatomic, retain) NSNumber *orderId;
@property (nullable, nonatomic, retain) NSDate *orderDate;
@property (nullable, nonatomic, retain) NSNumber *orderSum;
@property (nullable, nonatomic, retain) NSString *logoURL,*orderObj,*cartData;

-(id)populateCoreDataFromObject:(id)coreData;
-(void)populateObjectFromCorData:(id)coreData;

@end
