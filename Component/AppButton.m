//
//  AppButton.m
//  VIPCard
//
//  Created by SanC on 29/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "AppButton.h"
#import "AppColorConstants.h"

@implementation AppButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self initialize];
	}
	return self;
}


-(id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if(self){
		[self initialize];
	}
	return self;
}


- (void)initialize
{
	self.titleLabel.font = helveticaBoldFont(17);
	self.titleLabel.textColor = [UIColor whiteColor];
	self.titleLabel.textAlignment = NSTextAlignmentCenter;
	self.backgroundColor=[UIColor redColor];

}

@end
