//
//  DEMOCustomAutoCompleteObject.h
//  MLPAutoCompleteDemo
//
//  Created by Eddy Borja on 4/19/13.
//  Copyright (c) 2013 Mainloop. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPAutoCompletionObject.h"

@interface CityAutoCompleteObject : NSObject <MLPAutoCompletionObject>
@property (strong) NSString *cityName;
@property (strong) NSString *cityId;
- (id)initWithCityName:(NSString *)name andId:(NSString *) cityId;

@end
