//
//  CatForMealsCell.h
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "WSExtraForMealsResponse.h"

@interface CatForMealsCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedOptions;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropDown;

-(void)populateData:(WSExtraForMealsResponse *)aData;
-(void)populateDataByName:(NSString *)name str:(NSString *)str_middelName isNecessary:(NSString *)strIsNecessary response:(WSExtraForMealsResponse *)aData;

@end
