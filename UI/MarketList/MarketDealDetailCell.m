//
//  MarketDealDetailCell.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 27/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MarketDealDetailCell.h"

@implementation MarketDealDetailCell
@synthesize btnterms,btnadd;

- (void)prepareForReuse
{
    [btnterms setTitle:NSLocalizedString(@"Terms of network", nil) forState:UIControlStateNormal];
    [btnadd setTitle:NSLocalizedString(@"Add to List", nil) forState:UIControlStateNormal];
    
}
- (IBAction)showinfo:(id)sender {
}
@end
