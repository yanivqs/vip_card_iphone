//
//  ScrollViewImageViewController.h
//  VIPCard
//
//  Created by SanC on 23/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"

@interface ScrollViewImageViewController : BaseViewController<UIScrollViewDelegate>

@end
