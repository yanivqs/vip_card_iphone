//
//  CoreDataBase.m
//  ReceiptLess
//
//  Created by SanC on 10/03/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import "CoreDataBase.h"
#import "NSString+CommonForApp.h"


@implementation CoreDataBase

static NSManagedObjectContext *__managedObjectContext;
static NSManagedObjectModel *__managedObjectModel;
static NSPersistentStoreCoordinator *__persistentStoreCoordinator;


static CoreDataBase *_instance;


//shilpa
//static CoreDataBase *sharedInstance = nil;
//static sqlite3 *database = nil;
//static sqlite3_stmt *statement = nil;
//+(CoreDataBase*)getSharedInstance{
//	if (!sharedInstance) {
//		sharedInstance = [[super allocWithZone:NULL]init];
//		[sharedInstance createDB];
//	}
//	return sharedInstance;
//}
//
//-(BOOL)createDB{
//	NSString *docsDir;
//	NSArray *dirPaths;
//	// Get the documents directory
//	dirPaths = NSSearchPathForDirectoriesInDomains
//	(NSDocumentDirectory, NSUserDomainMask, YES);
//	docsDir = dirPaths[0];
//	// Build the path to the database file
//	databasePath = [[NSString alloc] initWithString:
//					[docsDir stringByAppendingPathComponent: @"GDRE_BLUE.db"]];
//	BOOL isSuccess = YES;
//	NSFileManager *filemgr = [NSFileManager defaultManager];
//	if ([filemgr fileExistsAtPath: databasePath ] == NO)
//	{
//		const char *dbpath = [databasePath UTF8String];
//		if (sqlite3_open(dbpath, &database) == SQLITE_OK)
//		{
//			char *errMsg;
//			const char *sql_stmt =
//			"create table if not exists DeviceToken (deviceToken text)";
//			if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
//				!= SQLITE_OK)
//			{
//				isSuccess = NO;
//				NSLog(@"Failed to create table");
//			}
//			sqlite3_close(database);
//			return  isSuccess;
//		}
//		else {
//			isSuccess = NO;
//			NSLog(@"Failed to open/create database");
//		}
//	}
//	return isSuccess;
//}
//
//
//-(BOOL) saveData:(NSString*)token
//
//{
//	const char *dbpath = [databasePath UTF8String];
//	if (sqlite3_open(dbpath, &database) == SQLITE_OK)
//	{
//
//
//	NSString *insertSQL = [NSString stringWithFormat:@"insert into DeviceToken (deviceToken) values (\"%@\")", token];
//
//
//        const char *insert_stmt = [insertSQL UTF8String];
//        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
//        if (sqlite3_step(statement) == SQLITE_DONE)
//        {
//			return YES;
//		}
//        else {
//			return NO;
//		}
//		 sqlite3_reset(statement);
//	}
//		return NO;
//}


//-----
#pragma mark - Core Data stack
+(CoreDataBase *) sharedInstanse
{
	if(!_instance)
		{
		_instance = [[CoreDataBase alloc] init];
		}
	return _instance;
}
+(NSManagedObjectContext *) managedObjectContext
{

	return  [[CoreDataBase sharedInstanse] getManageObjectContext];
}

-(id) init
{
	self = [super self];
	if(self)
	{
		[self getManageObjectContext];

	}
	return self;
}


-(NSManagedObjectContext *) getManageObjectContext
{
    if (__managedObjectContext)
	{
	return __managedObjectContext;
	}
		NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
		if (coordinator != nil) {
			__managedObjectContext = [[NSManagedObjectContext alloc] init];
			[__managedObjectContext setPersistentStoreCoordinator:coordinator];
		}

  return __managedObjectContext;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"VIPCards" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"VIPCards.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
     [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
     [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
    }
    return __persistentStoreCoordinator;
}

#pragma  get NEW record

-(id) newRecordFromDB:(NSString *) tblName
{
     return [NSEntityDescription insertNewObjectForEntityForName:tblName inManagedObjectContext:[self getManageObjectContext]];
}


#pragma Get Record From DB
-(id) getRecordFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate
{
	CustomError *error;
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:tblName];
	if(predicate)
    [fetch setPredicate:predicate];

	fetch.fetchLimit = 1;
    
    NSArray *results = [[self getManageObjectContext] executeFetchRequest:fetch error:&error];
    if(results && [results count] > 0)
		{
		return [results objectAtIndex:0];
		}
	else if(error)
		{
        NSLog(@"Error: %@", error);
        
		}
    return nil;
}

#pragma Get List fromDB

-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate sortDescriptor:(NSArray *) sortDescriptors
{
	CustomError *error;
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:tblName];
	if(predicate)
		[fetch setPredicate:predicate];
	if(sortDescriptors)
		[fetch setSortDescriptors:sortDescriptors];

	NSArray *results = [[self getManageObjectContext] executeFetchRequest:fetch error:&error];
	if(error){
		NSLog(@"Error in  %@ list retrival: %@",tblName, error);
	}
	return results;
}

-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate
{
	CustomError *error;
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:tblName];
	if(predicate)
      [fetch setPredicate:predicate];

	NSArray *results = [[self getManageObjectContext] executeFetchRequest:fetch error:&error];
	if(error)
		{
		NSLog(@"Error in  %@ list retrival: %@",tblName, error);
		
		}
	return results;
}


-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate withColums:(NSArray *) columns
{
    CustomError *error;
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:tblName];
    if(predicate)
        [fetch setPredicate:predicate];
    
    if(columns && columns.count > 0)
        [fetch setPropertiesToFetch:columns];
    
    NSArray *results = [[self getManageObjectContext] executeFetchRequest:fetch error:&error];
    if(error)
    {
        NSLog(@"Error in  %@ list retrival: %@",tblName, error);
        
    }
    return results;
}



-(NSArray *) getListFromDB:(NSString *) tblName withPredicate:(NSPredicate *) predicate
                 withLimit:(NSUInteger) limit{
	CustomError *error;
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:tblName];
    NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"payment_date"
                                                                    ascending:NO];
    [fetch setSortDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [fetch setFetchLimit:limit];
	if(predicate)
        [fetch setPredicate:predicate];
    
	NSArray *results = [[self getManageObjectContext] executeFetchRequest:fetch error:&error];
	if(error)
    {
		NSLog(@"Error in  %@ list retrival: %@",tblName, error);
		
    }
	return results;
}

//Get count value
-(NSNumber *) getCountValue:(NSString *)tblName withfieldName:(NSString *)fieldName withPredicate:(NSPredicate *)predicate
{
	NSExpression *keyExpression = [NSExpression expressionForKeyPath:fieldName];

	//create the NSExpression to tell our NSExpressionDescription which calculation we are performing.
	NSExpression *maxExpression = [NSExpression expressionForFunction:@"count:" arguments:[NSArray arrayWithObject:keyExpression]];
	NSExpressionDescription *description = [[NSExpressionDescription alloc] init];
	[description setName:@"countval"];
	[description setExpression:maxExpression];
	[description setExpressionResultType:NSInteger32AttributeType];
	CustomError *error;
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:tblName];
	if(predicate)
		[fetch setPredicate:predicate];

	[fetch setResultType:NSDictionaryResultType];
	[fetch setPropertiesToFetch:[NSArray arrayWithObject:description]];
	NSArray *results = [[self getManageObjectContext] executeFetchRequest:fetch error:&error];
	if(error)
	{
		NSLog(@"Error in  %@ list retrival: %@",tblName, error);

	}

	if(results && [results count] > 0)
	{
		return [[results objectAtIndex:0] valueForKey:@"countval"];
	}
	else
	{
		NSLog(@"Error: %@", error);

	}

	return [NSNumber numberWithInt:0];
}


-(NSNumber *) getMaxValue:(NSString *) tblName forField:(NSExpressionDescription *)expressionDescription  withPredicate:(NSPredicate *) predicate;
{
	CustomError *error;
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:tblName];
	if(predicate)
      [fetch setPredicate:predicate];

	[expressionDescription setName:@"maxval"];

	[fetch setResultType:NSDictionaryResultType];
	[fetch setPropertiesToFetch:[NSArray arrayWithObject:expressionDescription]];
	NSArray *results = [[self getManageObjectContext] executeFetchRequest:fetch error:&error];
	if(error)
		{
		NSLog(@"Error in  %@ list retrival: %@",tblName, error);
		
		}
	
	 if(results && [results count] > 0)
		{
		 return [[results objectAtIndex:0] valueForKey:@"maxval"];
		}
	  else
		{
          NSLog(@"Error: %@", error);
        
		}
	
	return [NSNumber numberWithInt:0];
}



// Unique Negative record ID

+(NSNumber *) getUniqueRecordID
{

	//  NSError *error;
	//  NSDate *date = [NSDate date];
	//  NSTimeInterval ti =  [date timeIntervalSince1970];


   return nil;
	
}

+(BOOL) saveManagedObject:(NSManagedObject *) rec
{
	NSError *error;
	if([[CoreDataBase managedObjectContext] save:&error])
		return YES;

	NSLog(@"Error in saving data:%@",[error localizedDescription]);

	return NO;
}

+(BOOL) deleteManagedObject:(NSManagedObject *) rec
{
	[[CoreDataBase managedObjectContext] deleteObject:rec];
    NSError *error;
    [[CoreDataBase managedObjectContext] save:&error];
    if(error)
        NSLog(@"Error in deleting object");
	return YES;
}

-(NSArray*) getUniqueValues:(NSString*) tblName forField:(NSString *) field withPredicate:(NSPredicate *) predicate
{
	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:tblName];
	NSEntityDescription *entity = [NSEntityDescription entityForName:tblName inManagedObjectContext:[self getManageObjectContext]];

	fetchRequest.resultType = NSDictionaryResultType;
	fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:field]];
	fetchRequest.returnsDistinctResults = YES;
	if(predicate)
		[fetchRequest setPredicate:predicate];

	// Now it should yield an NSArray of distinct values in dictionaries.
	NSArray *dictionaries = [[self getManageObjectContext] executeFetchRequest:fetchRequest error:nil];
	if(!dictionaries || dictionaries.count == 0)
		return nil;

	return dictionaries;

}


//User related operation


-(void) cleanTableData:(NSString *) tableName withPredicate:(NSPredicate *) predicate
{
	NSArray *list = [self getListFromDB:tableName withPredicate:predicate];
	for (NSManagedObject * obj in list)
    {
		[obj.managedObjectContext deleteObject:obj];
	}
}



-(void) cleanUserSpecificData
{
	//delete all suppliers belong to user
	[self cleanTableData:kTableMyOrders withPredicate:nil];
	
  //  [self cleanTableData:kTableInvoiceImagesCD withPredicate:nil];

}


@end
