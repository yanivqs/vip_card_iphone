//
//  WSBussinessObj.m
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBussinessObjResponse.h"
#import "WSOpeningHoursResponse.h"
#import "WSAdditionalExtrasResponse.h"
#import "WSAddressResponse.h"
#import "WSBussiness.h"

@implementation WSBussinessObjResponse
@synthesize id, name;


-(void)populateFromDictionary:(NSDictionary *)dict
{
    [super populateFromDictionary:dict];
    
    NSArray *tempBusinessList = [dict objectForKey:@"business"];
    if(tempBusinessList && [tempBusinessList isKindOfClass:[NSArray class]]&& [tempBusinessList count])
    {
        NSMutableArray *arrayTemp = [NSMutableArray array];
        for(NSDictionary *dict in tempBusinessList)
        {
            //[super parseWithDictionary:dict];
            
            WSBussiness *bussiness = [[WSBussiness alloc] initWithDictionary:dict];
            [arrayTemp addObject:bussiness];
        }
        self.bussinessList = arrayTemp;
    }
}

@end