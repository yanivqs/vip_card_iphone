//
//  FieldCategoryDetailVC.m
//  VIPCard
//
//  Created by SanC on 03/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "FieldCategoryDetailVC.h"
#import "FindBussinessForMealHolderVC.h"
#import "CustomImgView.h"

@interface FieldCategoryDetailVC ()

@end

@implementation FieldCategoryDetailVC
@synthesize array_categoryDetailList,str_Title;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	arrayCopyCategory=[[NSMutableArray alloc] init];
	arrayCopyCategory=array_categoryDetailList;
	[self showBackButton:YES];
	lbl_TopTitle.text=str_Title;
	[self customSetup];
    UIColor *color = [UIColor darkGrayColor];
    txtField_Search.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"חפש" attributes:@{NSForegroundColorAttributeName: color}];
	}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark-UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return  [array_categoryDetailList count] ;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
	// To "clear" the footer view
	return [UIView new] ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	FieldCategoryDetailCell *cell = (FieldCategoryDetailCell*) [self tableView:tableView cellForRowAtIndexPath:indexPath];

	WSCategoryFilterDetailResponse *categoryDetail = [array_categoryDetailList objectAtIndex:indexPath.row];
	if(categoryDetail.idTypeBusiness.integerValue >= 6)
	{
		CGSize size = [self getStringSize:cell.lbl_Discription andString:categoryDetail.des];
		if (size.height > 20)
			return 197 + (size.height-20);
		else
			return 197;
	}
	else if(categoryDetail.idTypeBusiness.integerValue >= 2 && categoryDetail.idTypeBusiness.integerValue <= 5)
	{
		CGSize size = [self getStringSize:cell.lbl_Discription andString:categoryDetail.des];
		if (size.height > 40)
			return 96 + (size.height-20);
		else
			return 96;
	}
	else if(categoryDetail.idTypeBusiness.integerValue == 1)
	{
		CGSize size = [self getStringSize:cell.lbl_Name andString:categoryDetail.name];
		if (size.height > 90)
			return 96 + (size.height-20);
		else
			return 96;
		return 96;
	}

	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

	WSCategoryFilterDetailResponse *bussinessResponse = [array_categoryDetailList objectAtIndex:indexPath.row];

	if(bussinessResponse.idTypeBusiness.integerValue >= 6)
	{
		FieldCategoryDetailCell *cell = (FieldCategoryDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"FieldCategoryDetailBigCell"];
		cell.btn_Call.tag=indexPath.row;

		[self populatedFindBussinessDataList:cell toCategory:bussinessResponse type:@"Big"];
		return cell;
	}
	else if(bussinessResponse.idTypeBusiness.integerValue >= 2 & bussinessResponse.idTypeBusiness.integerValue<= 5)
	{
		FieldCategoryDetailCell *cell = (FieldCategoryDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"FieldCategoryDetailmidleCell" ];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.btn_Call.tag=indexPath.row;

		[self populatedFindBussinessDataList:cell toCategory:bussinessResponse type:@"middle"];
		return cell;
	}
	else if(bussinessResponse.idTypeBusiness.integerValue == 1)
	{
		FieldCategoryDetailCell *cell = (FieldCategoryDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"FieldCategoryDetailSmallCell" ];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.btn_ImgPopUp.tag=indexPath.row;
		cell.btn_Call.tag=indexPath.row;
		[self populatedFieldCategorySmallCell:cell toCategory:bussinessResponse];

		return cell;
	}

	return  0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	WSCategoryFilterDetailResponse *bussinessResponse = [array_categoryDetailList objectAtIndex:indexPath.row];
	FindBussinessForMealHolderVC *categoryMealHolderVC = [self getVCWithSB_ID:kFindBussinessHolderVC];
	categoryMealHolderVC.findcategoryResponse = bussinessResponse;
	if(bussinessResponse.idTypeBusiness.integerValue != 1)
	[self.navigationController pushViewController:categoryMealHolderVC animated:YES];
}

#pragma mark-UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	if (![textField.text isEqualToString:@""])
	 [self searchCouponsby:txtField_Search.text];
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	NSString *searchString = txtField_Search.text;
	if([string isEqualToString:@""])
		searchString = [searchString substringToIndex:[searchString length] - 1];
	else
		searchString = [searchString stringByAppendingString:string];

	[self searchCouponsby:searchString];
	return YES;
}


#pragma mark-userDefineMethods
- (IBAction)onClick_BtnCall:(id)sender
{
	UIButton *btnCall = (UIButton*)sender;
	WSCategoryFilterDetailResponse *bussinessResponse = [array_categoryDetailList objectAtIndex:btnCall.tag];
	NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",bussinessResponse.phone]];

	if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
		[[UIApplication sharedApplication] openURL:phoneUrl];
	} else
	{
		UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
		[calert show];
	}
}

-(void)searchCouponsby:(NSString *)aStr
{
	NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[cd] %@) OR (SELF.des contains[cd] %@)",aStr, aStr];
	NSArray *filterArray = [arrayCopyCategory filteredArrayUsingPredicate:cityPredicate];
	array_categoryDetailList=[filterArray mutableCopy];
	if([aStr isEqualToString:@""])
		array_categoryDetailList = arrayCopyCategory;
	[tbl_View reloadData];
}

- (void)customSetup
{
	SWRevealViewController *revealViewController = self.revealViewController;
	if ( revealViewController )
	{
		[self.btn_SideMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
		[self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
	}
}

-(void)populatedFindBussinessDataList:(FieldCategoryDetailCell*)cell toCategory:(WSCategoryFilterDetailResponse*)bussinessResponse type:(NSString*)str_Type
{
	NSString *combined;
	if ([str_Type isEqualToString:@"Big"])
 {
	combined = [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.image];
	}
	else
	{
	combined = [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.logo];

	}
	NSURL *finalURL=[NSURL URLWithString:combined];
	[cell.img_View sd_setImageWithURL:finalURL placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];

	cell.lbl_Name.text = [NSString stringWithFormat:@"%@",bussinessResponse.name];
	cell.lbl_Discription.text = [NSString stringWithFormat:@"%@",bussinessResponse.des];
	CGSize size = [self getStringSize:cell.lbl_Discription andString:bussinessResponse.des];

	if(size.height > 20)
		cell.lbl_DiscriptionConstraintHeight.constant = size.height;
		else
		cell.lbl_DiscriptionConstraintHeight.constant = 20;
	if (bussinessResponse.address.count>0)
    {
	 WSAddressResponse *addressResponse = [bussinessResponse.address objectAtIndex:0];
	 NSString *address = [NSString stringWithFormat:@"%@ %@ , %@",addressResponse.street,addressResponse.NumberStreet,addressResponse.town];
	 cell.lbl_Address.text = address;
	}
	cell.frame=CGRectMake(cell.frame.origin.x, cell.frame.origin.y,cell.frame.size.width,cell.view_BckGround.frame.size.height+cell.frame.origin.y+2);
	[cell.btn_Call addTarget:self action:@selector(onClick_BtnCall:) forControlEvents:UIControlEventTouchUpInside];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
}


-(void)populatedFieldCategorySmallCell:(FieldCategoryDetailCell*)cell toCategory:(WSCategoryFilterDetailResponse*)bussinessResponse
{
	NSString *combined = [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.logo];
	NSURL *finalURL=[NSURL URLWithString:combined];
	[cell.img_View sd_setImageWithURL:finalURL placeholderImage:[UIImage imageNamed:Place_HOLDER_IMAGEBLUE]];
	//cell.img_View.layer.cornerRadius = cell.img_View.frame.size.height /2;
	//cell.img_View.layer.masksToBounds = YES;
	cell.img_View.layer.borderWidth = 0;
    cell.img_View.layer.cornerRadius =  cell.img_View.bounds.size.width/ 2;
    cell.img_View.clipsToBounds = YES;



	cell.lbl_Name.text = [NSString stringWithFormat:@"%@",bussinessResponse.name];
	CGSize size = [self getStringSize:cell.lbl_Name andString:bussinessResponse.name];
	if(size.height > 20)
	cell.lbl_NameconstraintHeight.constant = size.height;
	else
	cell.lbl_NameconstraintHeight.constant = 20;
	if (![bussinessResponse.phone isEqualToString:@"NULL"])
		cell.lbl_Phone.text=[NSString stringWithFormat:@"%@",bussinessResponse.phone];
	[cell.btn_Call addTarget:self action:@selector(onClick_BtnCall:) forControlEvents:UIControlEventTouchUpInside];
	[cell.btn_ImgPopUp addTarget:self action:@selector(onClick_BtnPopUp:) forControlEvents:UIControlEventTouchUpInside];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
}


- (IBAction)onClick_SideMenu:(id)sender
{
	
}

-(IBAction)onClick_BtnPopUp:(id)sender
{
	UIButton *btnCall = (UIButton*)sender;
	WSCategoryFilterDetailResponse *bussinessResponse = [array_categoryDetailList objectAtIndex:btnCall.tag];

	NSString *combined = [NSString stringWithFormat:@"%@%@", mFetchImages, bussinessResponse.logo];
	NSURL *finalURL=[NSURL URLWithString:combined];

	CustomImgView *imgView=[[CustomImgView alloc] initWithImage:Place_HOLDER_IMAGEBLUE imgUrl:finalURL];
	[imgView showInView:nil];


}
@end
