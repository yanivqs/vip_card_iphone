//
//  CustomError.h
//  ReceiptLess
//
//  Created by SanC on 10/03/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomError : NSError
{
	NSString *_errorMessage;
}

@property (strong,nonatomic) NSString *errorMessage;

+(CustomError *) customErrorFromError:(NSError *) error;

+(CustomError *) customErrorWithMessage:(NSString *) message;

@property (strong,nonatomic) id errorData;
@property (strong,nonatomic) NSString *operName;

-(id) initWithCode:(NSInteger) code withMessage:(NSString *) errorMsg;
+ (NSString *)userDefineMsgFromError:(NSError *)error;
@end
