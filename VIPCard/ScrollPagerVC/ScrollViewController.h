//
//  ScrollViewController.h
//  VIPCard
//
//  Created by SanC on 23/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "WSCouponsResponse.h"


@interface ScrollViewController : BaseViewController<ServerResponseDelegate>

@property (nonatomic, strong) WSCouponsResponse *couponObj;
@property(nonatomic,strong)WSCouponsResponse *dealObj;
@property (nonatomic, assign) BOOL isCoupneShow;

@property (weak, nonatomic) IBOutlet UIImageView *logoImg;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *bussinessImage;

@property (weak, nonatomic) IBOutlet UILabel *lblOfferTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOffer;
@property (weak, nonatomic) IBOutlet UILabel *lblDescTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblMarksTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMarks;
@property (weak, nonatomic) IBOutlet SHCStrikethroughLabel *lblPrice1;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice2;
@property (weak, nonatomic) IBOutlet UILabel *lblExpire;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lbl_OfferHeight;

@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIButton *btnMaze;
@property(nonatomic,strong)NSString *str_module;//it may be deal or coupon
@property (strong, nonatomic) IBOutlet UIView *view_DetailBck;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_viewdetailHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cons_lblMarkHeight;

//Actions
- (IBAction)onClickAddButton:(id)sender;
- (IBAction)onClickMazeButton:(id)sender;
- (IBAction)onClickCallButton:(id)sender;
// Barcode

@property (weak, nonatomic) IBOutlet UIImageView *imgBarcode;

@property (weak, nonatomic) IBOutlet UILabel *lblBarcodeText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_BarcodeHeight;
@property (weak, nonatomic) IBOutlet UIView *barcodeView;

/****** Login screen Popup *******/

@property (strong, nonatomic) IBOutlet UIView *popupBaseView;
@property (strong, nonatomic) IBOutlet UIButton *btnSignIn;
//- (IBAction)actionSignIN:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
//- (IBAction)actionSignup:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblSalesAddtionalText;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintAdditionalTextHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constatintDiscPriceHeight;

/*********************************/

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLabelOffer_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLabelDesc_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLabelMark_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *consLabelName_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *consDesc_Title_Height;
@property (weak, nonatomic) IBOutlet UIScrollView *detailScrollView;
@property (weak, nonatomic) IBOutlet UIView *BottomView;

@end