//
//  WSResult.m
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSResult.h"

@implementation WSResult
@synthesize message,status=_status;


-(void) parseWithDictionary:(NSDictionary *)dict
{
    //[super parseWithDictionary:dict];
    id param = [dict objectForKey:@"status"];
    if(param && [param isKindOfClass:[NSString class]])
    {
        self.status = param;
    }
    param = [dict objectForKey:@"message"];
    if(param && [param isKindOfClass:[NSArray class]])
    {
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *msg in param)
        {
            WSErrorMessage *errMsg = [[WSErrorMessage alloc] initWithDictionary:msg];
            [arr addObject:errMsg];
        }
        self.message = [NSArray arrayWithArray:arr];
        [self populateLocalizedErrorMessage];
    }
 }

-(BOOL)isSuccess
{
    return (_status && [_status isEqualToString:@"success"]);
}

-(void) populateLocalizedErrorMessage
{
    if(self.message && [self.message count] > 0)
    {
        WSErrorMessage *errMsg =(WSErrorMessage *) [self.message objectAtIndex:0];
        _localizedErrorMessage =  errMsg.error_msg;
        _error_code = errMsg.error_code;
    }
    else
    {
        _localizedErrorMessage = nil;
        _error_code = @"";
    }
}
@end
