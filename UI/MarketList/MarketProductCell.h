//
//  MarketProductCell.h
//  VIPCard
//
//  Created by Vishal Kolhe on 20/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "CatTopProductDetail.h"

@interface MarketProductCell : BaseTableViewCell
-(void)populateData:(CatTopProductDetail *)aData;
@property (weak, nonatomic) IBOutlet UIImageView *marketImg;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Price1;

@property (weak, nonatomic) IBOutlet UILabel *lbl_limitation;
@property (weak, nonatomic) IBOutlet UILabel *lbl_MarketName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_heightBname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subdes;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_Price;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_lbleheight;


//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lblNameConstraints;
//@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lblDecConstraints;
@property (strong, nonatomic) IBOutlet UILabel *Lbl_SHEKEL;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lblName;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lbldec;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *con_lbllimitation;
@end
