//
//  WSCategoryFilterDetailResponse.h
//  VIPCard
//
//  Created by SanC on 03/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSCategoryFilterDetailResponse : WSBaseData
//dict={
//	about = "<null>";
//	address =     (
//	);
//	aditionalExtras =     (
//	);
//	categorTop =     (
//	);
//	categoryMeal =     (
//	);
//	codeBusiness = 0;
//	coupones =     (
//	);
//	deals =     (
//	);
//	delivery = 0;
//	des = "<null>";
//	email = "<null>";
//	event = 0;
//	facebook = "<null>";
//	fax = "<null>";
//	gridGalleries =     (
//	);
//	homeGalleries =     (
//	);
//	id = 547;
//	idAddress = 0;
//	idTypeBusiness = 1;
//	image = "<null>";
//	logo = "<null>";
//	maxSeats = 0;
//	minDelivery = 0;
//	mobile = "<null>";
//	name = "\U05e1\U05e0\U05d8\U05e8 \U05e1\U05d0\U05d5\U05e0\U05d3";
//	openingHours =     (
//	);
//	phone = "08-9322423";
//	phone2 = "050-6944124";
//	sendEmail = 0;
//	sendFax = 0;
//	sendSms = 0;
//	site = "<null>";
//	takeAway = 0;
//	vipDiscount = 0;
//}
@property (strong ,nonatomic) NSString* about,*codeBusiness,*delivery,*des,*email,*event,*facebook,*fax,*id,*idAddress,*idTypeBusiness,*image,*logo,*maxSeats,*minDelivery,*mobile,*name,*phone,*phone2,*sendEmail,*sendFax,*sendSms,*site,*takeAway,*vipDiscount;

@property (nonatomic, strong) NSArray *openingHours,*address, *aditionalExtras, *categorTop, *categoryMeal, *coupones, *deals,*gridGalleries,*homeGalleries;
@end
