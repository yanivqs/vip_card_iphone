//
//  MarketDealDetailCell.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 27/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketDealDetailCell : UICollectionViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnterms;
@property (weak, nonatomic) IBOutlet UIImageView *imglogo;
@property (weak, nonatomic) IBOutlet UILabel *lblprodname;
@property (weak, nonatomic) IBOutlet UIImageView *imgprod;
@property (weak, nonatomic) IBOutlet UILabel *lblprice;
@property (weak, nonatomic) IBOutlet UILabel *lbldesc;
@property (weak, nonatomic) IBOutlet UILabel *lblsubdesc;
@property (weak, nonatomic) IBOutlet UILabel *lbllimit;
@property (weak, nonatomic) IBOutlet UILabel *lbldate;
@property (weak, nonatomic) IBOutlet UIButton *btnadd;
- (IBAction)showinfo:(id)sender;

@end
