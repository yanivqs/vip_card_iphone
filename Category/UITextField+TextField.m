//
//  UITextField+TextField.m
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "UITextField+TextField.h"

@implementation UITextField (TextField)


-(UITextField*)makeBorderTextView:(UITextField*)textField toColor:(UIColor*)color toWidth:(NSInteger)width
{
    UITextField *tempTextField = textField;    
    tempTextField.layer.borderWidth = width;
    tempTextField.layer.borderColor = color.CGColor;

    return tempTextField;
}

-(UITextField*)buttonCornerRadius:(UITextField*)textFields toRadius:(NSInteger)radius
{
    UITextField *temptextFields = textFields;
    
    temptextFields.layer.cornerRadius = radius;
    temptextFields.layer.masksToBounds = YES;
    
    return temptextFields;
}

-(UITextField*)addPaddingRight:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    textField.rightView = paddingView;
    textField.rightViewMode = UITextFieldViewModeAlways;
    return textField;
}

-(UITextField*)addPaddingLeft:(UITextField*)textField
{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
    return textField;
}

@end
