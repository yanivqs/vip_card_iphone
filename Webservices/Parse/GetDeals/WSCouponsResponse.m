//
//  WSCouponsResponse.m
//  VIPCard
//
//  Created by Vishal Kolhe on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSCouponsResponse.h"


@implementation WSCouponsResponse
@synthesize id, idBusiness, idCategor, unlimited, couponOrDeal, idTypeSeals, price1, price2, quantity1, quantity2, code, discount, name, offer, des, marks, image, startDate, expiredDate, idMeal, isDayDeal, barcode, nameBusiness, logobusiness, phone, latitude, longitude,isbarcode,textPrice;





-(NSString *) dealPriceFormatedValue
{
    NSLog(@"dealPriceFormatedValue: %ld",(long)[self.idTypeSeals integerValue]);
    NSString *shakel = @"₪";
    switch ([self.idTypeSeals integerValue])
    {
        case 1:
                return [NSString stringWithFormat:@"%@%@",shakel,self.price2];
            break;
            
        case 2:
            
            return [NSString stringWithFormat:@"%@ %@%@ %@",NSLocalizedString(@"dSeco", @"dSeco"),self.discount,@"%",NSLocalizedString(@"dDisc", @"dDisc")];
            
            break;
        case 3:
             return [NSString stringWithFormat:@"%@ %@%@ %@",NSLocalizedString(@"dSeco", @"dSeco"), shakel, self.discount,NSLocalizedString(@"dDisc", @"dDisc")];
            break;
            
        case 4:
            return [NSString stringWithFormat:@"%@ %@ %@ %@",NSLocalizedString(@"dBuy", @"dBuy"), self.quantity1, NSLocalizedString(@"dGet", @"dGet"),self.quantity2];
            break;
            
        case 5:
        
            return [NSString stringWithFormat:@"%@ %@ %@ %@%@",NSLocalizedString(@"dBuy", @"dBuy"), self.quantity1, NSLocalizedString(@"dB", @"dB"),shakel,self.price1];
             break;
            
        case 6:
            return [NSString stringWithFormat:@"%@ %@%@ %@ %@%@",NSLocalizedString(@"dBuy2", @"dBuy2"),shakel,self.price1 , NSLocalizedString(@"dPayOnly", @"dPayOnly"),shakel,self.price2];
            
            
            break;
            
        case 7:
            return [NSString stringWithFormat:@"%@%@ %@ %@",self.discount,@"%",NSLocalizedString(@"dDisc", @"dDisc"),self.textPrice];
            
        case 8:
             return [NSString stringWithFormat:@"%@+%@ %@ ",self.quantity1,self.quantity2,self.textPrice];
            break;
            
        case 9:
            return [NSString stringWithFormat:@"%@%@ %@ %@",shakel,self.discount,NSLocalizedString(@"dDisc", @"dDisc"),self.textPrice];
            break;
        case 10:
            if ([NSString isEmpty:self.textPrice]) {
                return @"שבועיים להתנסות \n חינם!";
            }else{
                return [NSString stringWithFormat:@"%@",self.textPrice];
            }
            break;

            
            
        default:
            break;
    }
    
    return @"";
}

@end
