//
//  AppBorderTextField.m
//  CheckNet
//
//  Created by Enovate Macbook Pro on 21/05/16.
//  Copyright © 2016 Enovate IT Outsourcing Pvt Ltd. All rights reserved.
//

#import "AppBorderTextField.h"

@implementation AppBorderTextField

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self styleViewComponent];
    }
    
    return self;
}

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self styleViewComponent];
    }
    
    return self;
}

-(void) styleViewComponent{
    
    [self adjustPaddingOfField];
    [self drawBorder:self.bounds];
    
}

-(void) adjustPaddingOfField{
    UIView *v =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, CGRectGetHeight(self.bounds) - 2.f)];
    self.leftView = v;
    self.rightView = v;
    self.leftViewMode = UITextFieldViewModeAlways;
     self.rightViewMode = UITextFieldViewModeAlways;
    
}

-(void) drawBorder:(CGRect) rect
{
    //Bottom Border
    CALayer *bottomBorder = [CALayer layer];
    CGFloat fieldHeight = CGRectGetHeight(rect);
    CGFloat fieldWidth = CGRectGetWidth(rect);
    CGFloat halfHeight = fieldHeight/2;
    
    bottomBorder.frame = CGRectMake(0.0f, fieldHeight - 1, fieldWidth, 1.0f);
    bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
    [self.layer addSublayer:bottomBorder];
    
    //Left Border
    CALayer *leftBorder = [CALayer layer];
    leftBorder.frame = CGRectMake(0.0f, halfHeight, 1.0f, halfHeight);
    leftBorder.backgroundColor = [UIColor blackColor].CGColor;
    [self.layer addSublayer:leftBorder];
    
    //Right Border
    CALayer *rightBorder = [CALayer layer];
    rightBorder.frame = CGRectMake(fieldWidth -1 , halfHeight, 1.0f, halfHeight);
    rightBorder.backgroundColor = [UIColor blackColor].CGColor;
    [self.layer addSublayer:rightBorder];
    
    //Top Border
    CALayer *topBorder = [CALayer layer];
    
    topBorder.frame = CGRectMake(0.0f,  0 , fieldWidth, 1.0f);
    topBorder.backgroundColor = [UIColor blackColor].CGColor;
    [self.layer addSublayer:topBorder];
    
    //Left Border
    CALayer *leftTopBorder = [CALayer layer];
    leftTopBorder.frame = CGRectMake(0.0f, 0, 1.0f, halfHeight);
    leftTopBorder.backgroundColor = [UIColor blackColor].CGColor;
    [self.layer addSublayer:leftTopBorder];
    
    //Right Border
    CALayer *rightTopBorder = [CALayer layer];
    rightTopBorder.frame = CGRectMake(fieldWidth -1 , 0, 1.0f, halfHeight);
    rightTopBorder.backgroundColor = [UIColor blackColor].CGColor;
    [self.layer addSublayer:rightTopBorder];


}

@end
