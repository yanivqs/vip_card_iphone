//
//  WSExtraForMealsResponse.h
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
@class WSExtraMealsResponse;

#import "WSBaseData.h"
#import "WSExtraMealsResponse.h"

@interface WSExtraForMealsResponse : WSBaseData
{
	//WSExtraMealsResponse *selectedExtraMealsResponse;
}

/*
 id	:	8
 
 idMeal	:	5
 
 description	:	\\u05e9\\u05d3\\u05e8\\u05d5\\u05d2\\u05d9\\u05dd
 
 price	:	0
 
 isChoice	:	0
 
 idExtraType	:	7
 
 idChoice	:	1
 
 maelOrExtra	:	1
 
 isNecessary	:	0
 
 nameExtraType	:	\\u05d8\\u05d1\\u05e2\\u05d5\\u05e0\\u05d9\\u05ea
 
 nameChoice	:
 
 
 
 */

//@property (nonatomic, strong) WSExtraMealsResponse* selectedExtraMealsResponse;

@property (nonatomic, strong) WSExtraMealsResponse* selectedExtraMealsResponse;

@property (nonatomic, strong) NSArray *selectedExtraMealsResponseList;

@property (nonatomic, strong) NSString *id, *idMeal, *description, *price, *isChoice, *idExtraType, *idChoice, *maelOrExtra, *isNecessary, *nameExtraType, *nameChoice,*strSelectedChoice,*extraForMealPrice;

@property (nonatomic, strong) NSArray *extraMealsList;
@property(nonatomic,strong)NSArray *array_SelectedMealsList;

//-(id)initWithDictionary:(NSDictionary *)dict;

-(BOOL) hasSelectedData;
-(NSString *) dispSelectedData ;

-(BOOL) isMultiChoice;
-(BOOL) isRequired;
@end
