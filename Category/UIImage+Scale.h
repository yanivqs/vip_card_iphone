//
//  UIImage+Scale.h
//  TinyNews
//
//  Created by Nava Carmon on 18/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Scale)

- (UIImage*)scaleToSize:(CGSize)size;
- (UIImage *)crop:(CGRect)rect;

@end
