//
//  WSFindBussinessListResponse.h
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSFindBussinessListResponse : WSBaseData
//"id":"1",
//"name":"vipcard",
//"fax":"08-9437847",
//"phone":"08-9437847",
//"phone2":null,
//"mobile":"054-2311123",
//"email":"yanivqs@gmail.com",
//"idAddress":"2",
//"idTypeBusiness":"8",
//"codeBusiness":"31787",
//"vipDiscount":"10",
//"facebook":"vipcard יבנה",
//"site":"vipcard.co.il",
//"sendFax":"0",
//"sendEmail":"0",
//"sendSms":"1",
//"takeAway":"0",
//"maxSeats":"0",
//"delivery":"0",
//"minDelivery":"0",
//"event":"0",
//"logo":"vipLogo.jpg",
//"des":"מתנות והדפסות",
//"image":"vipTop.jpg",
//"about":"",
//"openingHours":[],
//"gridGalleries":{},
//"homeGalleries":{},
//"coupones":[],
//"deals":[],
//"categoryMeal":[],
//"categorTop":[],
//"address":[],
//"aditionalExtras:{}":
//"facebookPage":""
@property (strong , nonatomic) NSString *id, *name, *fax, *phone, *phone2, *mobile, *email, *idAddress,*idTypeBusiness,*codeBusiness,*vipDiscount,*facebook,*site,*sendFax,*sendEmail,*sendSms,*takeAway,*maxSeats,*delivery,*minDelivery,*event,*logo,*des,*image,*about,*facebookPage;

@property (nonatomic, strong) NSArray *openingHoursList, *addressList, *categorTopList, *couponesList, *dealsList, *categoryMealList,*gridGalleries,*homeGalleries,*aditionalExtrasList;


@end
