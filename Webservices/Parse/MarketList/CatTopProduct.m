//
//  CatTopProduct.m
//  VIPCard
//
//  Created by Vishal Kolhe on 20/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CatTopProduct.h"
#import "CatTopProductDetail.h"

@implementation CatTopProduct

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
    if([super initWithDictionary:dict])
    {
        [self parseWithDictionary:dict];
    }
    return self;
}
*/

-(void)populateFromDictionary:(NSDictionary *)dict
{
    //TODO
    @autoreleasepool {
        [super populateFromDictionary:dict];
        NSMutableArray *productDict = [dict objectForKey:@"products"];
        if(productDict.count)
        {
            NSMutableArray *array = [NSMutableArray array];
            
            for(NSDictionary *dict in productDict)
            {
                CatTopProductDetail *Cat = [[CatTopProductDetail alloc] initWithDictionary:dict];
                [array addObject:Cat];
            }
            self.products=array;
            
            
        }
    }
}
@end
