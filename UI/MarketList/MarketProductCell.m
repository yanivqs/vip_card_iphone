//
//  MarketProductCell.m
//  VIPCard
//
//  Created by Vishal Kolhe on 20/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MarketProductCell.h"
#import "CatTopProduct.h"
#import "CatTopProductDetail.h"
#import "MarketProductVC.h"
#import "UIImageView+WebCache.h"
#import "AppGlobalConstant.h"
#import "WSConstants.h"



@implementation MarketProductCell

-(void)populateData:(CatTopProductDetail *)aData
{
//@Gauri Created//
    [self.lblDesc setText:aData.Description];
        //self.con_lbldec.constant=40;
    
    [self.lblName setText:aData.Name];
    
    //self.con_lblName.constant=40;
    [self.Lbl_SHEKEL setText:[NSString stringWithFormat:@"%@",SHEKEL]];
     self.Lbl_SHEKEL.textColor=COLOR_PRICE;
    [self.lblPrice setText:[NSString stringWithFormat:@"%@",aData.Price]];
    self.lblPrice.textColor=COLOR_PRICE;
    
    if ([aData.Price2 isEqual:@"0"])
        {
            [self.lbl_Price1 setHidden:YES];

        }else
        {
            [self.lbl_Price1 setText:[NSString stringWithFormat:@"%@",aData.Price2]];
            self.lbl_Price1.textColor=COLOR_PRICE;
          
        }
    [self fitandCenter:self.lblPrice];
    
    NSMutableAttributedString *text = [self.lbl_Price1.attributedText mutableCopy];
    [text addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, text.length)];
    self.lbl_Price1.attributedText = text;
    

    [self.lbl_subdes setText:aData.SubDescription];
    self.lbl_subdes.lineBreakMode = NSLineBreakByWordWrapping;
    self.lbl_subdes.numberOfLines = 0;
    
   // [self.lbl_MarketName setText:aData.bName];

    if (aData.Limitations.length > 0)
     {
        self.lbl_limitation.text = aData.Limitations;
     }
    else
    {
        self.lbl_limitation.text = @"";
    }
    
    
    //self.con_lbllimitation.constant=40;
        
    CGSize sizeprice = [self getStringSize:self.lblPrice andString:self.lblPrice.text]; //for testing
    self.con_Price.constant = sizeprice.width + 2.f;
    
    NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", mFetchMarketImages,aData.Image]];
   
    
    if(aData.Image.length > 0)
        [self.marketImg sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",PLACE_HOLDER_IMAGE]]];
    else
        [self.marketImg setImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
    
    
    //Update limitation size
    
    [self.lbl_limitation sizeToFit];
    CGRect f = self.lbl_limitation.frame;
    self.con_lbllimitation.constant  = f.size.height;
    //To NAme
    [self.lblName sizeToFit];
    self.con_lblName.constant = self.lblName.frame.size.height;
    //To Desc
    [self.lblDesc sizeToFit];
    self.con_lbldec.constant = self.lblDesc.frame.size.height;
     
}

-(void) fitandCenter:(UIView*)v
{
    CGPoint orig = v.center;
    [v sizeToFit];
    //CGRect newRect = v.frame;
    v.center = orig;
    //newRect.origin.x = (CGRectGetWidth(orig) - CGRectGetWidth(newRect))/2;
}
@end
