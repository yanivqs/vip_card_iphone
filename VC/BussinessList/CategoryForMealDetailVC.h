//
//  CategoryForMealDetailVC.h
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "WSMealsResponse.h"
#import "EMAccordionTableViewController.h"
#import "AppDelegate.h"
#import "ViewPopUp.h"
#import "MealListPopUP.h"



@interface CategoryForMealDetailVC : BaseViewController <EMAccordionTableDelegate,ViewPopUpDelegate,MealListPopUPDelegate>
{
	__weak IBOutlet UILabel *lbl_Prize;
	__weak IBOutlet UILabel *lbl_Name;
	__weak IBOutlet UILabel *lbl_Discription;
	__weak IBOutlet UIImageView *img_View;
	EMAccordionTableViewController *emTV;
	EMAccordionTableParallaxHeaderView *emParallaxHeaderView;
	NSMutableArray *sections,*array_RowData;
	NSArray *array_ChoiceList;
	CGFloat origin;
	bool flag;
	NSInteger tag;
	NSMutableDictionary *dict_sortByIsChoice;

	//old code
	__weak IBOutlet UILabel *lbl_Title;
	UIView *view_PopUP;
	UITableView *tbl_ChoiceName;
	NSMutableArray *array_isChoice,*array_ChoiceUniqeName;
	NSMutableDictionary *dict_isChoice;
	NSString *str_Choice;
	NSIndexPath *existingIndexPath;
	NSArray *indexArray;
	AppDelegate *appDelegate;
	ViewPopUp *viewPopUp;
	__weak IBOutlet NSLayoutConstraint *viewdetailBck;


	//gauri Code
	NSMutableArray *array_Object;
	__weak IBOutlet UITableView *tbl_View;
	NSMutableDictionary *dict_GroupByName;
	__weak IBOutlet UIScrollView *scrollView;





}
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, strong) WSMealsResponse *mealResponse;
@property (weak, nonatomic) IBOutlet UITableView *catTableView;
@property (strong,nonatomic) NSString *businessId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTableHeight;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintUpperViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintLblNameHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintLblDescHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constaintLblDescBGColorHeight; //11 / 08 /2016 - RR

@property (weak, nonatomic) IBOutlet UIButton *btnAddtoCart;
- (IBAction)onClickAddtoCartButton:(id)sender;
-(void)showPopUp;
-(void)hidePopUp;
- (IBAction)showInfo:(id)sender;


@end

