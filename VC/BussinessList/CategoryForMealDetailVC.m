//
//  CategoryForMealDetailVC.m
//  VIPCard
//  Screen is used to Display Multiple Options for MEAL
//  Created by Shilpa Gade on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CategoryForMealDetailVC.h"
#import "CatMealDetailCell.h"
#import "CatForMealsCell.h"
#import "MealListPopUP.h"
#import "iToast.h"
#import "ShoppingCartSummaryVC.h"
#import "ChoiceListForMeals.h"
#import "ChoiceTypeGrt0.h"
#import "ChoiceType0.h"
#import "AppAlertView.h"
#import "ReportError.h"

#define kTableHeaderHeight 42.0f
#define kTableRowHeight 42.0f
@interface CategoryForMealDetailVC () <MealListPopUPDelegate>
{
	NSArray *extraForMealList;
	NSMutableArray *listOfMeals;
	NSMutableDictionary *dict_rowData;

	AppGlobalData *appGlobalData;

	WSExtraForMealsResponse *selectedExtraMealsResponse;
    ReportError *reportErr;
    BOOL openPopFlag, openPopflaghead;
    NSMutableArray *arrBtns;
    int opencnt,popcntr,sectioncnt;
}

@property (nonatomic, strong) WSExtraForMealsResponse *extraMealsResponse;
@property(nonatomic,strong) MealListPopUP *mealListPopUP ;

@end

#define TableViewSectionHeight  41
@implementation CategoryForMealDetailVC
@synthesize mealResponse, extraMealsResponse;

-(void)viewDidLoad
{
	[super viewDidLoad];
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self showBackButton:YES];
	[self.btnAddtoCart setTitle:NSLocalizedString(@"meal_add", @"meal_add") forState:UIControlStateNormal];
	//tbl_View.tableFooterView = self.btnAddtoCart;
	appGlobalData = [AppGlobalData sharedManager];
	[self papulateDataDetail];
	[tbl_View setSectionHeaderHeight:kTableHeaderHeight];
	tbl_View.backgroundColor=[UIColor whiteColor];
	tbl_View.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

	tbl_View.tag=200;
	emTV = [[EMAccordionTableViewController alloc] initWithTable:tbl_View withAnimationType:EMAnimationTypeNone];
	[emTV setDelegate:self];
	emTV.parallaxHeaderView = emParallaxHeaderView;
	sections=[[NSMutableArray alloc] init];
	array_RowData=[[NSMutableArray alloc] init];
	dict_rowData=[[NSMutableDictionary alloc] init];
	array_Object=[[NSMutableArray alloc] init];
	dict_GroupByName=[[NSMutableDictionary alloc] init];
	array_ChoiceList=[self sortData];//sort data by ischoice
    
    [array_Object sortUsingComparator:^NSComparisonResult(id obj1,  id obj2)
     {
         BOOL flag1 = FALSE;
         BOOL flag2 = FALSE;
         if ([obj1 isKindOfClass:[ChoiceTypeGrt0 class]])
         {
             flag1 = ((ChoiceTypeGrt0 *)obj1).isMendatory;
         }
         else
         {
             flag1 = [obj1 isRequired];
         }
         
         if ([obj2 isKindOfClass:[ChoiceTypeGrt0 class]])
         {
             flag2 = ((ChoiceTypeGrt0 *)obj2).isMendatory;
         }
         else
         {
             flag2 = [obj2 isRequired];
         }
         
         if (flag1 > flag2) {
             return (NSComparisonResult)NSOrderedAscending;
         }
         else if (flag1 < flag2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         return (NSComparisonResult)NSOrderedSame;
         
         /*if (obj1.isRequired||obj2.isRequired) {
          return NSOrderedSame;
          }
          else{
          return 1;
          }*/
     }];
    
    
	for (int i=0; i<array_Object.count; i++)
	{
		EMAccordionSection *section01 = [[EMAccordionSection alloc] init];

		if ([[array_Object objectAtIndex:i] isKindOfClass:[ChoiceTypeGrt0 class]])
		{
			ChoiceTypeGrt0 *choiceTypeGrt=[array_Object objectAtIndex:i];
            section01.isRequired = choiceTypeGrt.isMendatory; // Initial
			if (!choiceTypeGrt.isMendatory)
				[section01 setTitleColor:[UIColor grayColor]];
			else
				[section01 setTitleColor:[UIColor redColor]];
            
			[section01 setTitle:NSLocalizedString(@"lbl_under_select_group",@"lbl_under_select_group")];
			[section01 setMiddelTitle:NSLocalizedString(@"RedPlaceHolderText", "RedPlaceHolderText")];

		}
		else
		{
			WSExtraForMealsResponse * resp = [array_Object objectAtIndex:i] ;
			[section01 setTitle:resp.description];
			[section01 setMiddelTitle:NSLocalizedString(@"RedPlaceHolderText", "RedPlaceHolderText")];
			section01.isRequired = [resp isRequired]; // Initial

            if ([resp isRequired])
                [section01 setTitleColor:[UIColor redColor]];

            else
                [section01 setTitleColor:[UIColor grayColor]];
		}
        //Set section Data
		section01.sectionData = [array_Object objectAtIndex:i];
		[emTV addAccordionSection:section01 initiallyOpened:NO];
		[sections addObject:section01];

	}
	
    arrBtns = [[NSMutableArray alloc] init];
    opencnt = 1;
    popcntr = 0;
    sectioncnt = 0;
    openPopFlag = NO;
    openPopflaghead = NO;

	[self.mainScrollView addSubview:emTV.tableView];
	flag=true;
    
    //[self autoPopup:NO];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	//[self.view  bringSubviewToFront:self.btnAddtoCart];

}
-(void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:YES];

}

//##### added by Sarika [29-07-16] #####

-(void)autoPopup:(BOOL)isChild
{
    
    /**
     1. curretIndexPath : section and row : -1 or 100000
     2. IF currentIndexPath != nil then
         Check for Sel Items 
             Check If mandatory  and row > currentIndexPath.row
                Set row : mandatory item
    3.  Run LOOP for EMAccordianSection
          if(required && (currentIndexPath == nil || sectionIndex > currentIndexPath.section))
            {
                  currentIndexPath = section:sectionIdex, row : -1/100000
                 [open section]
            }
     Done
     
     */
    /*
    NSIndexPath *curretIndexPath = [NSIndexPath indexPathForRow:-1 inSection:tag];
    if (curretIndexPath != nil) {
        WSExtraForMealsResponse *meal = selectedExtraMealsResponse;
        if([meal isRequired] && ![meal hasSelectedData])
        {
        }
    }*/
    if (tag < sections.count) {
     EMAccordionSection *section = [sections objectAtIndex:tag];
    if (isChild) {
        
    //for (EMAccordionSection *section in sections) {
        if (section.isRequired) {
            if(section.sectionData && [section.sectionData isKindOfClass:[ChoiceTypeGrt0 class]])
            {
                //check here for items and data
                if(section.items && section.items.count > 0 && popcntr < section.items.count)
                {
                    while(popcntr < section.items.count)
                    {
                        WSExtraForMealsResponse *meal = [section.items objectAtIndex:popcntr];
                         if([meal isRequired] && ![meal hasSelectedData])
                         {
                             NSIndexPath *indexpath = [NSIndexPath indexPathForRow:popcntr inSection:tag];
                             [self tableView:tbl_View didSelectRowAtIndexPath:indexpath];
                             popcntr++;
                             return;
                         }
                        popcntr++;
                        
                    }
                }
                popcntr = 0;
                tag = 0;
                    
                 /*
                    
                    if([meal isRequired] && ![meal hasSelectedData])
                        //return NO;
                    {
                        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:popcntr inSection:tag];
                        [self tableView:tbl_View didSelectRowAtIndexPath:indexpath];
                        
                        if (popcntr < section.items.count-1) {
                            popcntr++;
                            
                        }else{
                            //sectioncnt++;
                            popcntr=0;
                            tag=0;
                        }
                    }else{
                        if (popcntr < section.items.count-1) {
                            popcntr++;
                            [self autoPopup:isChild];
                            
                        }else{
                            popcntr=0;
                            tag=0;
                        }
                        
                    }
                    
                    
                }*/
            }
            /* // no need to open other section
            else if(section.sectionData && [section.sectionData isKindOfClass:[WSExtraForMealsResponse class]])
            {
                WSExtraForMealsResponse *meal = (WSExtraForMealsResponse *) section.sectionData;
                if([meal isRequired] && ![meal hasSelectedData])
                {    //return NO;}
                    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:popcntr inSection:tag];
                    [self tableView:tbl_View didSelectRowAtIndexPath:indexpath];
             
                    if (tag < sections.count-1) {
                        popcntr=0;
                        tag++;
                        if (section.isRequired) {
                            //[self openTheSection:nil];
                        }
                        isChild=NO;
                        [self autoPopup:isChild];
                        
                    }else{
                        popcntr=0;
                        tag=0;
                    }
                }
                else{
                   
                    if (tag < sections.count-1) {
                        popcntr=0;
                        tag++;
                        isChild=NO;
                        if (section.isRequired) {
                            //[self openTheSection:nil];
                        }
                        [self autoPopup:isChild];
                        
                    }else{
                        popcntr=0;
                        tag=0;
                        
                    }
                    
                }
            }*/
        
        }
    
    //}
    }else{
        /*
        if (tag < sections.count-1) {
            popcntr=0;
            tag++;
            EMAccordionSection *section = [sections objectAtIndex:tag];
            if (section.isRequired) {
                [self openTheSection:nil];
            }
        }
         */
    }
}
}
//##### END #####


#pragma mark EMAccordionTableDelegate

- (CGFloat)eaAccodianTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
     EMAccordionSection *emAccordionSection = [sections objectAtIndex:section];
    NSString *str = [emAccordionSection dispMiddleTitle];
    CGFloat w = (tableView.frame.size.width - 30) / 2;
    CGSize s = [str sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:CGSizeMake(w, NSIntegerMax) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize leftSize = [emAccordionSection.title sizeWithFont:helveticaBoldFont(17.0) constrainedToSize:CGSizeMake(130, NSIntegerMax) lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat defHeight = TableViewSectionHeight;
    
    if(leftSize.height > 21)
    {
        defHeight = defHeight + leftSize.height - 21;
    }
    
    
    if(s.height > 34.f)
    {
        return s.height + 10.f;
    }
    
    return defHeight;
}

/*
 CREATE SECTION VIEW FOR CHOICE 0 & GREATER
 */

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	//here place the BUTTON with ARROW sign // STEP1
    EMAccordionSection *emAccordionSection = [sections objectAtIndex:section];
    
   // CGSize mainSize = tableView.bounds.size;
    
    UILabel *lbl_MiddelTitle = [[UILabel alloc] init];
    lbl_MiddelTitle.numberOfLines = 0;
    lbl_MiddelTitle.lineBreakMode = NSLineBreakByWordWrapping;

	float tblView_Width=tableView.frame.size.width;
	UIView *view_Section = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,tblView_Width,TableViewSectionHeight)];
    view_Section.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
	UIView *view_ColorBottomBorder=[[UIView alloc] initWithFrame:CGRectMake(0, view_Section.frame.size.height-2, tableView.frame.size.width,2)];
	view_ColorBottomBorder.backgroundColor=[UIColor redColor];
	

	//Img DropDown
	UIImageView *img_DropDown = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f,(view_Section.frame.size.height-17)/2,17.0f,17.0f)];
	[img_DropDown setBackgroundColor:[UIColor clearColor]];
	[img_DropDown setImage:[UIImage imageNamed:@"Down_arrow"]];
    
    img_DropDown.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;


	//Label Middel
    CGFloat xPos = CGRectGetMaxX(img_DropDown.frame) + 2;
	float width_Label=tblView_Width - xPos;
	 lbl_MiddelTitle.frame = CGRectMake(xPos, 0.0f,width_Label/2, view_Section.bounds.size.height);

	[lbl_MiddelTitle setBackgroundColor:[UIColor clearColor]];
	[lbl_MiddelTitle setText:[emAccordionSection dispMiddleTitle]];
	[lbl_MiddelTitle setTextColor:[emAccordionSection dispTitleColor]];
	lbl_MiddelTitle.textAlignment = NSTextAlignmentRight;
    //--------
    [view_Section addSubview:view_ColorBottomBorder];
    [view_Section addSubview:img_DropDown];
    [view_Section addSubview:lbl_MiddelTitle];
    //--------------
    
    
    CGFloat w  = lbl_MiddelTitle.frame.size.width;
    [lbl_MiddelTitle sizeToFit];
    CGRect f = lbl_MiddelTitle.frame;
    f.size.width = w;
    lbl_MiddelTitle.frame = f;
    
    if(f.size.height > 34)
    {
        f = view_Section.frame;
        CGFloat h = lbl_MiddelTitle.frame.size.height + 10.f;
        f.size.height = h;
        view_Section.frame = f;
        f = img_DropDown.frame;
        f.origin.y = (h - f.size.height)/2;
        img_DropDown.frame = f;
        view_ColorBottomBorder.frame =  CGRectMake(0, h -2 , tableView.frame.size.width,2);
        
    }
    else {
        f.origin.y = (view_Section.frame.size.height - f.size.height) / 2;
        lbl_MiddelTitle.frame = f;
    }
    
    
    
    /*

    CGSize titleFrame = [self getStringSize:lbl_MiddelTitle andString:[emAccordionSection dispMiddleTitle]];

    if(titleFrame.height > 41)
    {
        view_Section.frame = CGRectMake(0.0f, 0.0f,tblView_Width,TableViewSectionHeight + titleFrame.height);
        view_ColorBottomBorder.frame =  CGRectMake(0, view_Section.frame.size.height-2, tableView.frame.size.width,2);
        img_DropDown.frame =  CGRectMake(10.0f,(view_Section.frame.size.height-17)/2,17.0f,17.0f);
        width_Label=tblView_Width-(img_DropDown.frame.origin.x+img_DropDown.frame.size.width+2);
        lbl_MiddelTitle.frame = CGRectMake(img_DropDown.frame.origin.x+img_DropDown.frame.size.width+2, 0.0f,width_Label/2, view_Section.bounds.size.height);

    }
	else
	{
		lbl_MiddelTitle.frame = CGRectMake(img_DropDown.frame.origin.x+img_DropDown.frame.size.width+2, (view_Section.frame.size.height -  titleFrame.height) / 2,width_Label/2, lbl_MiddelTitle.frame.size.height);
	}*/
 
	//label right

	UILabel *lbl_LeftTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lbl_MiddelTitle.frame)+2, 0.0f,width_Label/2-5, view_Section.bounds.size.height)];
    
    lbl_LeftTitle.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;

	
	[lbl_LeftTitle setText:emAccordionSection.title];

	[lbl_LeftTitle setBackgroundColor:[UIColor clearColor]];
	lbl_LeftTitle.textAlignment = NSTextAlignmentRight;
	lbl_LeftTitle.font=helveticaBoldFont(17.0);
    lbl_LeftTitle.numberOfLines = 0;
    lbl_LeftTitle.lineBreakMode = NSLineBreakByWordWrapping;
    
	[view_Section addSubview:lbl_LeftTitle];
	UIButton *btn_Section = [[UIButton alloc] initWithFrame:view_Section.bounds];
	if (section==0)
	 btn_Section.tag=0;
	else
		btn_Section.tag=section;
	[btn_Section addTarget:self action:@selector(openTheSection:) forControlEvents:UIControlEventTouchDown];
    
    //added by Sarika [28-07-2016]
    if (!openPopflaghead && emAccordionSection.isRequired) {
        NSLog(@"isRequired: %d",emAccordionSection.isRequired);
       //[self openTheSection:btn_Section];
        if (![arrBtns containsObject:btn_Section]) {
            [arrBtns addObject:btn_Section];
            NSLog(@"arrBtns head: %@",arrBtns);
            NSLog(@"btn color:%@",[btn_Section titleColorForState:UIControlStateNormal]);
        }
        
    }
    
    //#####################
    
	[view_Section addSubview:btn_Section];

	[view_Section setBackgroundColor:[UIColor whiteColor]];
	return view_Section;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    EMAccordionSection *emAccordionSection = [sections objectAtIndex:section];
    
    
    NSString *aText =  [emAccordionSection dispMiddleTitle];
    if(aText && aText.length > 5)
    {
        CGFloat w = (tableView.bounds.size.width - 40) / 2;
        
        CGSize x = [aText sizeWithFont:helveticaBoldFont(17.0) constrainedToSize:CGSizeMake(w, NSIntegerMax) lineBreakMode:NSLineBreakByWordWrapping];
        
        if(x.height > 42)
            return x.height;
    }
    
    return TableViewSectionHeight;
    
}
*/

//Now provide cell as dropdown to the section

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"CatForMealsCell";
	CatForMealsCell *mealCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	NSMutableArray *items = [self dataFromIndexPath:indexPath];
	if(!items)
		return nil;

	WSExtraForMealsResponse *rowData = [items objectAtIndex:indexPath.row];
	[mealCell populateDataByName:rowData.description str:NSLocalizedString(@"RedPlaceHolderText", "RedPlaceHolderText") isNecessary:rowData.isNecessary response:rowData];
	mealCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (rowData.isNecessary) {
        NSLog(@"necessary...");
    }
    //added by Sarika [28-07-2016]
    if (!openPopFlag && rowData.isNecessary) {
       //[self openPop:indexPath];
    if (![arrBtns containsObject:indexPath]) {
        [arrBtns addObject:indexPath];
        NSLog(@"arrBtns: %@",arrBtns);
    }
        
    }
    
    //##### End #####
    
	return mealCell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

   // CatForMealsCell *cell = (CatForMealsCell*) [self tableView:tbl_View cellForRowAtIndexPath:indexPath];
    
    NSMutableArray *items = [self dataFromIndexPath:indexPath];
    WSExtraForMealsResponse *rowData = [items objectAtIndex:indexPath.row];
    
    CGFloat width = tableView.bounds.size.width - 180.f;
    
    
    NSString *aText = [rowData dispSelectedData];
    
   CGSize descFrame = [rowData.description sizeWithFont:helveticaBoldFont(17.0) constrainedToSize:CGSizeMake(130, NSIntegerMax)];
     CGFloat defHeight = kTableRowHeight;
    if(descFrame.height > 21)
        defHeight = defHeight + descFrame.height - 21;

    
    if(aText && aText.length > 10){
        CGSize titleFrame = [aText sizeWithFont:[UIFont systemFontOfSize:17.f] constrainedToSize:CGSizeMake(width, NSIntegerMax)
                                  lineBreakMode:NSLineBreakByWordWrapping];
        if(titleFrame.height >= 41 )
        {
            return kTableRowHeight + titleFrame.height - 21;
        }
        
    }
	return defHeight;
}


-(MealListPopUP *) getExtraMealSelectView{
    
    self.mealListPopUP=[MealListPopUP loadFromNibWithFrame:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
    
    //[[MealListPopUP alloc] initWithFrame:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
    
	[self.mealListPopUP setDelegate:self];
	return self.mealListPopUP;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	NSMutableArray *items = [self dataFromIndexPath:indexPath];
	if(!items)
		return;
	WSExtraForMealsResponse *item=[items objectAtIndex:indexPath.row];
	self.mealListPopUP = [self getExtraMealSelectView];
	selectedExtraMealsResponse = item;
	item.array_SelectedMealsList=[NSArray arrayWithObject: selectedExtraMealsResponse];
	self.mealListPopUP.extraForMealsObj = item;
    self.mealListPopUP.isChild = YES;
	[appDelegate.window addSubview:self.mealListPopUP];
	[self.mealListPopUP refreshMealList];

}
// added by Sarika
//###### function start ######
-(void)openPop:(NSIndexPath*)indexPath
{
    NSMutableArray *items = [self dataFromIndexPath:indexPath];
    if(!items)
        return;
    WSExtraForMealsResponse *item=[items objectAtIndex:indexPath.row];
    self.mealListPopUP = [self getExtraMealSelectView];
    selectedExtraMealsResponse = item;
    item.array_SelectedMealsList=[NSArray arrayWithObject: selectedExtraMealsResponse];
    self.mealListPopUP.extraForMealsObj = item;
    [appDelegate.window addSubview:self.mealListPopUP];
    [self.mealListPopUP refreshMealList];
}
// ###### End ######

- (NSMutableArray *) dataFromIndexPath: (NSIndexPath *)indexPath {
	if (sections.count>indexPath.section)
	{
		EMAccordionSection *sec = [sections objectAtIndex:indexPath.section];
		return sec.items;
	}
	return nil;
}

- (void) latestSectionOpened:(int)index {

	str_Choice=[NSString stringWithFormat:@"%d",index];
	if (flag)
	 [self createPopUp];//methods for create pop up tableview
	flag=true;

}


#pragma mark-ViewPopUpDelegate
-(void)returnData:(NSString*)str
{
	str_Choice=str;
	EMAccordionSection *section01;
	section01=[sections objectAtIndex:tag];
	//now get list of items based on selected str
	ChoiceTypeGrt0 *choiceType =(ChoiceTypeGrt0 *) section01.sectionData;
    
    [choiceType resetSelection]; // reset selection

	NSArray * sectionItems = [choiceType arrayByChoiceName:str];
	[section01 setItems:[NSMutableArray arrayWithArray:sectionItems]];

	//logic to store selected field for extra meals
	if (choiceType.isMendatory==NO)
		[section01 setTitleColor:[UIColor grayColor]];
	else
		[section01 setTitleColor:[UIColor redColor]];

	selectedExtraMealsResponse = extraMealsResponse;
	
/* TODO#SanC
	for (int i=0; i<sectionItems.count; i++)

	{
		extraMealsResponse=[sectionItems objectAtIndex:i];
		extraMealsResponse.array_SelectedMealsList=extraMealsResponse.extraMealsList;

	}
 */
	NSLog(@"array is=%@",sectionItems);
	[emTV openSectionByIndex:tag];
	//[tbl_View reloadData];
    [self performSelector:@selector(resetScrollViewSize) withObject:nil afterDelay:0.8];
    
    if (opencnt < arrBtns.count) {
        if ([[arrBtns objectAtIndex:opencnt] isKindOfClass:NSClassFromString(@"UIButton")]) {
            //[self openTheSection:[arrBtns objectAtIndex:opencnt]];
        }else if ([[arrBtns objectAtIndex:opencnt] isKindOfClass:NSClassFromString(@"NSIndexPath")]) {
            //[self openPop:[arrBtns objectAtIndex:opencnt]];
        }
        opencnt++;
    }
    //openPopFlag = YES;
    openPopflaghead = YES;
    
    [self autoPopup:YES];
}

#pragma mark-UserDefineMethods


- (IBAction)openTheSection:(id)sender
{
	//NSLog(@"array object=%@",array_Object);
	if(!viewPopUp){
		viewPopUp=[[ViewPopUp alloc] initWithFrame:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
	}
    if (sender != nil) {
        UIButton *btn=(UIButton *)sender;
        tag=btn.tag;
    }
	
	if ([[array_Object objectAtIndex:tag] isKindOfClass:[ChoiceTypeGrt0 class]])
	{
        EMAccordionSection *emAccordionSection = [sections objectAtIndex:tag];
		ChoiceTypeGrt0 *choiceTypegrt=[array_Object objectAtIndex:tag];
		NSArray *set_Name= choiceTypegrt.array_DistChoiceNames;
		viewPopUp.array_tblData=set_Name;
        viewPopUp.header=emAccordionSection.title;
		[self showPopUp];
        [viewPopUp reloadScreenData]; // To refresh screen data

	}
    else if ([[array_Object objectAtIndex:tag] isKindOfClass:[WSExtraForMealsResponse class]])
    {
        if(!self.mealListPopUP){
            self.mealListPopUP = [self getExtraMealSelectView];
        }
        WSExtraForMealsResponse *extraForMeal=[array_Object objectAtIndex:tag];
        selectedExtraMealsResponse = extraForMeal;
        self.mealListPopUP.extraForMealsObj = extraForMeal;
        self.mealListPopUP.isChild=NO;
        [appDelegate.window addSubview:self.mealListPopUP];
        [self.mealListPopUP refreshMealList];
    }
}

-(void)showPopUp
{
	viewPopUp.popUpDelegate = self;
	[appDelegate.window addSubview:viewPopUp];
}

-(void)hidePopUp
{
	viewPopUp.array_tblData=nil;
	[viewPopUp removeFromSuperview];
}

- (IBAction)showInfo:(id)sender {
    
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 300, 200)];
    
    //view.center = self.view.center;
    //AppAlertView *alert = [AppAlertView createWithTitle:@"Title" message:@"Message" cancelButton:@"Ok" otherButtons:nil, nil];
    //[alert show:view rtlFlag:NO];
    [appDelegate.window addSubview:[self getReportErrView]];
}
-(ReportError *) getReportErrView{
    
    reportErr=[ReportError loadFromNibWithFrame:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
    [reportErr fillData:mealResponse.id];
    UITapGestureRecognizer *tapgest = [[UITapGestureRecognizer alloc] initWithTarget:reportErr action:@selector(hideKey)];
    tapgest.delegate = reportErr;
    [reportErr addGestureRecognizer:tapgest];
    return reportErr;
}
-(NSArray *)sortData
{
	[self.navigationController.navigationBar setBarTintColor:[UIColor redColor]];
	if(!selectedExtraMealsResponse || selectedExtraMealsResponse == nil)
	{
		extraForMealList = mealResponse.extraForMealsList;

		if(extraForMealList.count)
		{
			listOfMeals = [NSMutableArray array];

			for(WSExtraForMealsResponse *extraForMealsResponse in extraForMealList)
			{
				extraForMealsResponse.selectedExtraMealsResponseList = nil;
				extraForMealsResponse.selectedExtraMealsResponse = nil;


				if(extraForMealsResponse.extraMealsList && extraForMealsResponse.extraMealsList.count > 0)
				{
					[listOfMeals addObject:extraForMealsResponse];

				}

			}
			dict_sortByIsChoice=[[NSMutableDictionary alloc] init];
			NSArray *array_ChoiceName;
			//Here build array with distinct choices
			NSArray *distChoices = [listOfMeals valueForKeyPath:@"@distinctUnionOfObjects.isChoice"];
			NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self"
																		ascending: YES];
			 distChoices=[distChoices sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];


			for (NSString *isChoice in distChoices)
			{
                 //Store all ZERO choices as section item
				if ([isChoice isEqualToString:@"0"])
				{
					array_ChoiceName =[listOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isChoice = %@",isChoice]];
                    //Add to Section Array
					[array_Object addObjectsFromArray:array_ChoiceName];
				}
				else{
                    //Combine choice items and store then as section
                    
					array_ChoiceName =[listOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isChoice = %@",isChoice]];
					ChoiceTypeGrt0 *choiceTypeGrt0=[[ChoiceTypeGrt0 alloc] init];
					choiceTypeGrt0.array_ChoiceTypeGrt0=array_ChoiceName;
					NSArray *array_count=[array_ChoiceName filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isNecessary = %@",@"1"]];
					if (array_count.count>0)
						choiceTypeGrt0.isMendatory = YES;
					else
						choiceTypeGrt0.isMendatory = NO;



					choiceTypeGrt0.isChoiceValue = isChoice;
                    //Add to section ARRAY
					[array_Object addObject:choiceTypeGrt0];

					//[dict_sortByIsChoice setValue:array_ChoiceName forKey:wSextraForMealsResponse.isChoice];
				}

			}

			/*NSSortDescriptor *gradeSorter = [[NSSortDescriptor alloc] initWithKey:@"isMendatory" ascending:NO];
			NSSortDescriptor *nameSorter = [[NSSortDescriptor alloc] initWithKey:@"isNecessary" ascending:NO];

			[array_Object sortUsingDescriptors:[NSArray arrayWithObjects:gradeSorter, nameSorter, nil]];*/
		}
	}
	return [dict_sortByIsChoice allKeys];


}
-(void)papulateDataDetail
{
	self.title=self.mealResponse.name;
	self.navigationController.navigationBar.backgroundColor=[UIColor redColor];

	self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:helveticaMediumFont(22)};

	lbl_Name.text=self.mealResponse.name;
	[lbl_Name setTextAlignment:NSTextAlignmentRight];
    lbl_Prize.text=[NSString displayPrice:self.mealResponse.price];//[NSString stringWithFormat:@"%@%@",SHEKEL,self.mealResponse.price];
	[lbl_Prize setTextAlignment:NSTextAlignmentLeft];

	lbl_Discription.text=self.mealResponse.description;
	if(![lbl_Discription.text isEqualToString:@""] && lbl_Discription.text!=nil)
    {
        //lbl_Discription.backgroundColor=COLOR_LIGHT_GRAY;
    }
	if(self.mealResponse.image.length > 0)
	{
		NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImages,self.mealResponse.image]];
        /*[img_View sd_setImageWithURL:urlString completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            img_View.image=[self imageWithImage:image scaledToWidth:320];
        }];*/
		[img_View sd_setImageWithURL:urlString placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
		img_View.image=[self imageWithImage:img_View.image scaledToWidth:320];
		
	}
	else
		[img_View setImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];

	[self performSelector:@selector(resetUpperViewHeight) withObject:nil afterDelay:0.0];
}

#pragma -Handle Change in dynamic height
-(void) resetUpperViewHeight{
	CGRect frame = lbl_Name.frame;
    CGFloat height = (array_Object && array_Object.count > 0) ? 270 : 242;
	[lbl_Name sizeToFit];
	frame.size.height = lbl_Name.frame.size.height;
	if(frame.size.height > 25)
	{
		height = height + frame.size.height - 25.f;
		
		self.constaintLblNameHeight.constant = frame.size.height;

	}

	frame = lbl_Discription.frame;
	[lbl_Discription sizeToFit];
	frame.size.height = lbl_Discription.frame.size.height;
	if(frame.size.height > 25)
	{
		height = height + frame.size.height - 25.f;

		self.constaintLblDescHeight.constant = frame.size.height;
        self.constaintLblDescBGColorHeight.constant = frame.size.height; //11 /08 /2016 - RR 

	}
    if ([lbl_Discription.text isEmpty]) {
        self.constaintLblDescHeight.constant = 0;
    }
	//now set the height as upperview height
    CGFloat newH = CGRectGetHeight(self.view.bounds) - 80;
    if((!array_Object || array_Object.count == 0) && newH > height)
    height = newH;

	self.constaintUpperViewHeight.constant = height;




}



-(NSInteger)getChoiceCount
{
	NSArray *array_ChoiceName = [listOfMeals filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isChoice = %@", str_Choice]];
	array_ChoiceUniqeName=[[array_ChoiceName valueForKeyPath:@"@distinctUnionOfObjects.nameChoice"] mutableCopy];

	return [array_ChoiceUniqeName count];
}

-(void)checkDescriptionCount:(WSExtraForMealsResponse *)extraForMeals
{
	NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"SELF.nameExtraType contains[cd] %@", extraForMeals.nameExtraType];
	NSArray *list = [extraForMealList filteredArrayUsingPredicate:cityPredicate];
	if(list.count)
	{
		//NSLog(@"Desc: %@", list);
	}
}


-(void)createPopUp
{
}

#pragma mark-popUpMealListDelegate

-(void)mealListPopup:(MealListPopUP *)mealListPopup  selectedItem:(id)item
{
    // added by Sarika [28-07-2016]
    /*
    
    openPopFlag = YES;
    //openPopflaghead = YES;
    
    if (opencnt < arrBtns.count) {
        UIButton *btn = (UIButton*)[arrBtns objectAtIndex:opencnt];
        if ([btn isKindOfClass:NSClassFromString(@"UIButton")]) {
            //if ([btn.titleLabel.textColor isEqual:[UIColor redColor]]) {
                //[self openTheSection:[arrBtns objectAtIndex:opencnt]];
            //}
            
        }else if ([[arrBtns objectAtIndex:opencnt] isKindOfClass:NSClassFromString(@"NSIndexPath")]) {
            //[self openPop:[arrBtns objectAtIndex:opencnt]];
        }
        opencnt++;
    }
     */
    // ###### End ######
    
	selectedExtraMealsResponse.selectedExtraMealsResponse = item;
    [self reloadTableData];
    [self autoPopup:mealListPopup.isChild];
}

-(void)mealListPopup:(MealListPopUP *)mealListPopup selectedListofItems:(NSArray *)aList
{
    // added by Sarika [28-07-2016]
    //openPopflaghead = YES;
    /*
    openPopFlag = YES;
    if (opencnt < arrBtns.count) {
        if ([[arrBtns objectAtIndex:opencnt] isKindOfClass:NSClassFromString(@"UIButton")]) {
            //[self openTheSection:[arrBtns objectAtIndex:opencnt]];
        }else if ([[arrBtns objectAtIndex:opencnt] isKindOfClass:NSClassFromString(@"NSIndexPath")]) {
            //[self openPop:[arrBtns objectAtIndex:opencnt]];
        }
        opencnt++;
    }
    */
    // ###### End ######
    
    
	selectedExtraMealsResponse.selectedExtraMealsResponseList = aList;
    [self reloadTableData];
    [self autoPopup:mealListPopup.isChild];
}

-(void) reloadTableData{
    [tbl_View reloadData];
    [self performSelector:@selector(resetScrollViewSize) withObject:nil afterDelay:0.2];
}

-(void) resetScrollViewSize{
   
    CGSize s = tbl_View.contentSize;
    BOOL isTableExist = (array_Object && array_Object.count > 0) ? TRUE:FALSE;
    if(s.height < 203 && isTableExist)
        s.height = 203;
    
    self.constraintTableHeight.constant = s.height + ((isTableExist) ? 60.f : 0.f);
    [self.mainScrollView setNeedsUpdateConstraints];
    
    
     CGSize mainSize = self.mainScrollView.contentSize;
    mainSize.height = CGRectGetMinY(tbl_View.frame) + s.height;
    
    [self.mainScrollView setContentSize:mainSize];
    
    
}

-(void) viewWillLayoutSubviews
{
    [self resetScrollViewSize];
    [super viewWillLayoutSubviews];
}

//SAVE TO CART
- (IBAction)onClickAddtoCartButton:(id)sender
{

	BOOL isValidData = [self isValidData];

	if(isValidData)
	{
		NSLog(@"is valid");
		[[[[iToast makeText:NSLocalizedString(@"valid Data", @"valid Data")] setGravity:iToastGravityBottom] setDuration:iToastDurationShort] show];
        //TODO Not sure but better to create COPY of mealResponse
        //mealResponse.description = mealResponse.name;
        NSLog(@"selectedExtraMealsResponse:%@",selectedExtraMealsResponse);
		[appGlobalData addItemToCart:self.businessId cartItem:mealResponse];


		[self.navigationController popViewControllerAnimated:YES];
	}
	else
	{
		NSLog(@"is not valid");
		[[[[iToast makeText:NSLocalizedString(@"Not valid data", @"Not valid data")] setGravity:iToastGravityBottom] setDuration:iToastDurationShort] show];
	}
}

-(BOOL)isValidData
{
     if(sections)
     {
         for (EMAccordionSection *sect in sections)
         {
            if(sect.sectionData && [sect.sectionData isKindOfClass:[ChoiceTypeGrt0 class]])
            {
                //check here for items and data
                if(sect.items && sect.items.count > 0)
                {
                    for (WSExtraForMealsResponse *meal in sect.items)
                    {
                        if([meal isRequired] && ![meal hasSelectedData])
                            return NO;
                    }
                }
            }
            else if(sect.sectionData && [sect.sectionData isKindOfClass:[WSExtraForMealsResponse class]])
            {
                WSExtraForMealsResponse *meal = (WSExtraForMealsResponse *) sect.sectionData;
                if([meal isRequired] && ![meal hasSelectedData])
                    return NO;
            }
             
         }
     }
	
	return YES;
}

@end