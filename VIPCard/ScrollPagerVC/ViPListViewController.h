//
//  ViPListViewController.h
//  VIPCard
//
//  Created by SanC on 22/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "WSDealsResponse.h"

@interface ViPListViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) WSDealsResponse *dealsResponseObj;
@property (assign) BOOL isCoupns;

@property (assign) BOOL isSearch;
@property(nonatomic,strong) NSString *str_module;//it may be come from coupon or deal

@property (nonatomic, strong) NSArray *filteredCouponsList;

@end
