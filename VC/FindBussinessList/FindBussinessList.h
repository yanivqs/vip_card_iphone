//
//  FindBussinessList.h
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "HomeVC.h"
#import "AboutBussinessVC.h"
#import "BussinessList.h"
#import "SWRevealViewController.h"




@interface FindBussinessList : BaseViewController
{
	NSArray *array_ViewControllers;
	
	__weak IBOutlet UIButton *btn_SideSearchBar;
}

@property (strong , nonatomic) IBOutlet UITableView *findBussinessList;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSearch;
@property (weak, nonatomic) IBOutlet UIButton *btn_SideMenu;
@property (weak, nonatomic) IBOutlet UIButton *revealBtnItem;
- (IBAction)onClick_SideMenu:(id)sender;


@end
