//
//  AppColorConstants.h
//  VIPCard
//
//  Created by Andrei on 05/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#ifndef AppColorConstants_h
#define AppColorConstants_h


#define COLOR_PAGER_TITLE_BACKGROUND    [UIColor colorWithRed:(169.0/255) green:(169.0/255) blue:(169.0/255) alpha:1]
#define COLOR_MENU_INDICATOR_COLOR    [UIColor colorWithRed:(255.0/255) green:(255.0/255) blue:(255.0/255) alpha:1]

#define COLOR_MAIN_PRICE     [UIColor colorWithRed:(0.0/255) green:(126.0/255) blue:(0.0/255) alpha:1]
#define COLOR_SELECTED_MENU_TEXT     [UIColor colorWithRed:(05.0/255) green:(156.0/255) blue:(163.0/255) alpha:1]
#define COLOR_SELECTED_MENU_INDICATOR     [UIColor colorWithRed:(52.0/255) green:(177.0/255) blue:(183.0/255) alpha:1]

#define COLOR_TITLE_NEVY_BLUE     [UIColor colorWithRed:(72.0/255) green:(61.0/255) blue:(139.0/255) alpha:1]
#define COLOR_TITLE_Light_BLUE     [UIColor colorWithRed:(102.0/255) green:(204.0/255) blue:(255.0/255) alpha:1]
#define COLOR_LIGHT_GRAY [UIColor colorWithRed:(204.0/255) green:(204.0/255) blue:(204.0/255) alpha:1]

#define COLOR_TEXT_FIELD_BORDER [UIColor colorWithRed:245.0 / 255.0 green:136.0 / 255.0 blue:20.0 / 255.0 alpha:1.0]

#define COLOR_TEXT_FIELD_RED_BORDER [UIColor colorWithRed:245.0 / 255.0 green:136.0 / 255.0 blue:20.0 / 255.0 alpha:1.0]

#define COLOR_TEXT_FIELD_GRAY_BORDER [UIColor colorWithRed:250.0 / 255.0 green:250.0 / 255.0 blue:250.0 / 255.0 alpha:1.0]
//0-0-128

#define COLOR_TOPBAR_PAGER    [UIColor colorWithRed:(216.0/255) green:(217.0/255) blue:(216.0/255) alpha:1]
#define COLOR_GREEN_ALL    [UIColor colorWithRed:(05.0/255) green:(156.0/255) blue:(163.0/255) alpha:1]
#define COLOR_PRICE    [UIColor colorWithRed:(0.0/255) green:(107.0/255) blue:(1.0/255) alpha:1]

#define helveticaFont(r) [UIFont fontWithName:@"HelveticaNeue" size:r]
#define helveticaBoldFont(r) [UIFont fontWithName:@"HelveticaNeue-Bold" size:r]
#define helveticaLightFont1(r) [UIFont fontWithName:@"HelveticaNeue-Light" size:r]
#define helveticaLightFont(r) [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:r]
#define helveticaMediumFont(r) [UIFont fontWithName:@"HelveticaNeue-Medium" size:r]



#endif /* AppColorConstants_h */
