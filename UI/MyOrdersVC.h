//
//  MyOrdersVC.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 26/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "MyOrders.h"
#import "MyOrdersCell.h"

@interface MyOrdersVC : BaseViewController<UITableViewDataSource,UITableViewDelegate,MyOrderDelegate>
{
    NSArray *listOfOrders;
}
@property (weak, nonatomic) IBOutlet UITableView *tblOrders;
@property (weak, nonatomic) IBOutlet UILabel *lblerr;

@end
