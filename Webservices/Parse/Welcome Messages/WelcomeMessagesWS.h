//
//  WelcomeMessagesWS.h
//  Chain
//
//  Created by Andrei Boulgakov on 15/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSBaseData.h"

@interface WelcomeMessagesWS : WSBaseData
{
    NSString *comment_count, *create_date, *user_id, *image_url, *schedule_days, *text_message, *title, *openCount, *avg_ranking, *video_url, *share_count, *video_thumb,*account_id, *avg_message_time, *msg_phone_call_count;
    
    int category;
}

@property (nonatomic, strong) NSString *user_viewed_count;
@property (nonatomic, assign) BOOL allow_holiday;
@property (nonatomic, assign) BOOL verify_comment;
@property (nonatomic, assign) int category;
@property (nonatomic, strong) NSString *comment_count, *create_date, *user_id, *image_url, *schedule_days, *text_message, *title, *openCount, *avg_ranking, *video_url, *share_count, *video_thumb, *account_id, *avg_message_time, *msg_phone_call_count;

- (id) initWithDictionary:(NSDictionary *)dict;
@end
