//
//  BussinessList.m
//  VIPCard
//
//  Created by SanC on 12/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BussinessList.h"

#import "AppColorConstants.h"
#import "ScrollViewController.h"
#import "MainTableViewCell.h"

@interface BussinessList ()

@end

@implementation BussinessList
@synthesize array_Coupans,array_deals,isCoupns,isSearch,str_module;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.array_Coupans.count>0)
        self.array_List=self.array_Coupans;
    if (self.array_deals.count>0)
        self.array_List=self.array_deals;
	// NSLog(@"list is=%@",self.array_List);
    
    [self.tableview registerClass:[MainTableViewCell class] forCellReuseIdentifier:@"mainncell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.array_List count] *2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

	if(indexPath.row%2==0)
	{
		MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mainCell"];

		if (cell==nil)
		{
			NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MainTableViewCell" owner:self options:nil];
			cell=[nib objectAtIndex:0];
		}
		[cell setBackgroundColor:[UIColor clearColor]];
		WSCouponsResponse *dealResponse = [self.array_List objectAtIndex:indexPath.row/2];
		[cell populateDataBussinessList:dealResponse andState:NO];
		//  CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
		// CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];

		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		//        if(expectedNameSize.height > 21)
		//        {
		//            cell.constaintLblNameHeight.constant = expectedNameSize.height;
		//        }
		//        if(expectedDescSize.height > 21)
		//        {
		//            cell.constaintLblDescHeight.constant = expectedDescSize.height;
		//        }
		cell.layer.borderColor = [UIColor blackColor].CGColor;

		cell.layer.borderWidth = 0.6;
		//        [self.mainPrice setStrikethrough:YES];
		[cell.mainPrice setStrikethrough:YES];

		return cell;
	}
	else{
		MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mainCell"];
		// cell.layer.borderColor = [UIColor blackColor].CGColor;
		// cell.layer.borderWidth = 0.5;

		if (cell==nil)
		{
			NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MainTableViewCell" owner:self options:nil];
			cell=[nib objectAtIndex:0];
		}
		[cell setBackgroundColor:COLOR_LIGHT_GRAY];

		[cell populateDataBussinessList:nil andState:YES];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		/*CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
		 CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];
		 [cell.mainPrice setStrikethrough:YES];
		 if(expectedNameSize.height > 21)
		 {
		 cell.con_NameHeight.constant = expectedNameSize.height;
		 }
		 if(expectedDescSize.height > 21)
		 {
		 cell.con_DescriptionHeight.constant = expectedDescSize.height;
		 }*/


		return cell;
	}

//	if(indexPath.row%2==0)
//	{
//		MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mainCell"];
//
//		if (cell==nil)
//		{
//			NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MainTableViewCell" owner:self options:nil];
//			cell=[nib objectAtIndex:0];
//		}
//		[cell setBackgroundColor:[UIColor whiteColor]];
//		WSFindDealsResponse  *response=[self.array_List objectAtIndex:indexPath.row/2];
//		[cell populateDataBussinessList:response andState:NO];
//		// CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
//		// CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];
//
//		cell.selectionStyle = UITableViewCellSelectionStyleNone;
//		//        if(expectedNameSize.height > 21)
//		//        {
//		//            cell.con_NameHeight.constant = expectedNameSize.height;
//		//        }
//		//        if(expectedDescSize.height > 21)
//		//        {
//		//            cell.con_DescriptionHeight.constant = expectedDescSize.height;
//		//        }
//		cell.layer.borderColor = [UIColor blackColor].CGColor;
//		cell.layer.borderWidth = 0.5;
//		//        [self.mainPrice setStrikethrough:YES];
//		[cell.mainPrice setStrikethrough:YES];
//
//		return cell;
//	}
//	else{
//		MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mainCell"];
//		if (cell==nil)
//		{
//			NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MainTableViewCell" owner:self options:nil];
//			cell=[nib objectAtIndex:0];
//		}
//		[cell setBackgroundColor:COLOR_LIGHT_GRAY];
//		[cell populateDataBussinessList:nil andState:YES];
//		cell.selectionStyle = UITableViewCellSelectionStyleNone;
//		return cell;
//	}
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.row%2==0)
	{

		WSCouponsResponse  *couponseResponse=[self.array_List objectAtIndex:indexPath.row/2];
		CGFloat expectedNameSize = 21.f;
		CGFloat expectedDescSize = 21.f;
		float totalHeight = 284.0f;
		CGFloat w = CGRectGetWidth(self.view.bounds) - 30;
		if(![NSString isEmpty:couponseResponse.name])
		{
			CGSize xSize = [couponseResponse.name sizeWithFont:helveticaFont(25.f)
											 constrainedToSize:CGSizeMake(w,NSIntegerMax)
												 lineBreakMode:NSLineBreakByWordWrapping];
			expectedNameSize = xSize.height;
		}
		if(![NSString isEmpty:couponseResponse.des])
		{
			CGSize xSize = [couponseResponse.des sizeWithFont:helveticaFont(20.f)
											constrainedToSize:CGSizeMake(w,NSIntegerMax)
												lineBreakMode:NSLineBreakByWordWrapping];
			expectedDescSize = xSize.height;
		}

		if(![NSString isEmpty:couponseResponse.nameBusiness])
		{
			CGSize xSize = [couponseResponse.nameBusiness sizeWithFont:helveticaFont(17.f)
													 constrainedToSize:CGSizeMake(w,NSIntegerMax)
														 lineBreakMode:NSLineBreakByWordWrapping];
			if (xSize.height > 21)
				totalHeight = totalHeight + xSize.height - 21;
		}

		//[myString sizeWithFont:aLabel.font
		//	 constrainedToSize:maximumLabelSize
		//		 lineBreakMode:aLabel.lineBreakMode];

		// CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
		//CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];
		//adjust the label the the new height.



		if(expectedNameSize > 21)
		{
			totalHeight += expectedNameSize;
		}
		else{
			totalHeight += 21;
		}
		if(expectedDescSize > 21)
		{
			totalHeight += expectedDescSize;
		}
		else{
			totalHeight += 21;
		}
		//totalHeight += 284;
		// if(totalHeight > 350)
		return totalHeight;
		// return 350;
		//return (cell.nameBussiness.frame.origin.y+cell.nameBussiness.frame.size.height+20);
	}
	return 8;

}

//    if(indexPath.row%2==0)
//    {
//        MainTableViewCell *cell = (MainTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
//        CGSize expectedNameSize = [self getStringSize:cell.name andString:cell.name.text];
//        CGSize expectedDescSize = [self getStringSize:cell.desc andString:cell.desc.text];
//        //adjust the label the the new height.
//
//        float totalHeight = 0;
//
//        if(expectedNameSize.height > 21)
//        {
//            totalHeight += expectedNameSize.height;
//        }
//        else{
//            totalHeight += 21;
//        }
//        if(expectedDescSize.height > 21)
//        {
//            totalHeight += expectedDescSize.height;
//        }
//        else{
//            totalHeight += 21;
//        }
//        totalHeight += 208;
//        if(totalHeight > 250)
//            return totalHeight;
//        return 250;
//    }
//
//    return 8;

//}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


#pragma mark-UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    ScrollViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kScrollDetail_SB_ID];
//    WSCouponsResponse  *response=[self.array_List objectAtIndex:indexPath.row];
//    vc.couponObj=response;
//    [self.navigationController pushViewController:vc animated:YES];
	ScrollViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kScrollDetail_SB_ID];

	if ([[self.array_List objectAtIndex:indexPath.row/2] isKindOfClass:[WSCouponsResponse class]])
	{
		WSCouponsResponse *couponRes = [self.array_List objectAtIndex:indexPath.row/2];
		vc.couponObj = couponRes;
		vc.isCoupneShow = NO;
		vc.str_module=@"Coupon";

	}
	else
	{
		WSCouponsResponse  *couponRes = [self.array_List objectAtIndex:indexPath.row/2];
		vc.dealObj = couponRes;
		vc.isCoupneShow=YES;
		vc.str_module=@"Deal";
	}

		[self.navigationController pushViewController:vc animated:YES];
}




@end
