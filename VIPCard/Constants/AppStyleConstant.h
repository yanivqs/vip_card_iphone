//
//  AppStyleConstant.h
//  CheckNet
//
//  Created by Enovate Macbook Pro on 21/05/16.
//  Copyright © 2016 Enovate IT Outsourcing Pvt Ltd. All rights reserved.
//

#ifndef AppStyleConstant_h
#define AppStyleConstant_h


#endif /* AppStyleConstant_h */

//FONT



//APP Default Font
#define AppDefaultBoldFont(r) [UIFont fontWithName:@"Arimo-Bold" size:r]
#define AppDefaultItalicFont(r) [UIFont fontWithName:@"Arimo-Italic" size:r]
#define AppDefaultBoldItalicFont(r) [UIFont fontWithName:@"Arimo-BoldItalic" size:r]
#define AppDefaultFont(r) [UIFont fontWithName:@"Arimo" size:r]

//COLOR

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBAlpha(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define APP_NAVBAR_COLOR  [UIColor greenColor]
#define APP_TITLE_COLOR_WHITE [UIColor whiteColor]

#define APP_TOAST_BORDERCOLOR [UIColor grayColor]

#define APP_BLACK_COLOR  [UIColor blackColor]
#define APP_THEME_COLOR  [UIColor colorWithRed:42.0f/255.0f green:131.0f/255.0f blue:253.0f/255.0f alpha:1.0]

;//@"#2A83FD"
//[UIColor colorWithRed:0.012 green:0.224 blue:0.624 alpha:1.0];//@"#3399fe"

#define APP_THEME_DARKCOLOR [UIColor colorWithRed:0.008 green:0.659 blue:0.431 alpha:1.0]//@"#2a86e2"

//#define APP_HELP_TEXTCOLOR [UIColor colorWithRed:0.031 green:0.282 blue:0.282 alpha:1.0];//@"#848484"
#define APP_HELP_TEXTCOLOR [UIColor colorWithRed:132.0/255 green:132.0/255 blue:132.0/255 alpha:1.0]

#define  APP_PLACEHOLDER_COLOR [UIColor grayColor];

#define ligurinoBoldFont(r) [UIFont fontWithName:@"ligurino-bold" size:r]
#define robotoLightItalicFont(r) [UIFont fontWithName:@"Roboto-LightItalic" size:r]
#define robotoLight(r) [UIFont fontWithName:@"Roboto-Light" size:r]



