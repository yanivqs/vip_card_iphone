//
//  ReportError.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 26/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReportErrDelegate <NSObject>

-(void)submitData:(NSArray*)data;

@end

@interface ReportError : UIView<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>
{
    NSArray *typeArr;
    BOOL openKey;
}

+(ReportError *) loadFromNibWithFrame:(CGRect) frame;

@property (weak, nonatomic) IBOutlet UITextView *txtRemark;
@property (weak, nonatomic) IBOutlet UIView *mainview;
@property (weak, nonatomic) IBOutlet UITableView *tblTypes;
@property (weak, nonatomic) IBOutlet UILabel *lblProdCode;
@property (weak, nonatomic) IBOutlet UILabel *lblProbType;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarks;
@property (weak, nonatomic) IBOutlet UIButton *btnSub;
@property (weak, nonatomic) IBOutlet UILabel *lblHead;
@property (weak, nonatomic) IBOutlet UIButton *btntype;
@property (weak, nonatomic) id<ReportErrDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *txtprodcode;
@property (weak, nonatomic) IBOutlet UIView *contview;


- (IBAction)onClick_Close:(id)sender;
- (IBAction)showTypes:(id)sender;
-(void)fillData:(NSString*)data;

@end
