//
//  CatForMealsCell.m
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "CatForMealsCell.h"

@implementation CatForMealsCell
-(void)populateData:(WSExtraForMealsResponse *)aData
{
    if([aData.maelOrExtra boolValue])
    {
        if(aData.selectedExtraMealsResponseList.count)
        {
            NSMutableString *selectedMeals = [NSMutableString string];
            for(int i=0; i< aData.selectedExtraMealsResponseList.count; i++)//WSExtraMealsResponse *extraMealsResponse in aData.selectedExtraMealsResponseList)
            {
                WSExtraMealsResponse *extraMealsResponse = [aData.selectedExtraMealsResponseList objectAtIndex:i];
                if(i == 0)
                {
                    [selectedMeals appendString:extraMealsResponse.Name];
                }
                else
                {
                    [selectedMeals appendString:[NSString stringWithFormat:@", %@" ,extraMealsResponse.Name]];
                }
            }
            [self.lblSelectedOptions setText:selectedMeals];
            [self.lblSelectedOptions setTextColor:[UIColor darkGrayColor]];
        }
    }
    else if(aData.selectedExtraMealsResponse)
    {
        [self.lblSelectedOptions setText:aData.selectedExtraMealsResponse.Name];
        [self.lblSelectedOptions setTextColor:[UIColor darkGrayColor]];
    }
    else
    {
        if([aData.isNecessary boolValue])
        {
            [self.lblSelectedOptions setTextColor:[UIColor redColor]];
        }
        else
        {
            [self.lblSelectedOptions setTextColor:[UIColor darkGrayColor]];
        }
        
        [self.lblSelectedOptions setText:aData.nameExtraType];
    }
    
    [self.lblDesc setText:aData.description];
  //  [self.lblSelectedOptions setText:aData.nameExtraType];
    
}

-(void)populateDataByName:(NSString *)name str:(NSString *)str_middelName isNecessary:(NSString *)strIsNecessary response:(WSExtraForMealsResponse *)aData
{
	[self.lblSelectedOptions setText:[aData dispSelectedData]];
	if ([self.lblSelectedOptions.text isEqualToString:@""])
   {
	 self.lblSelectedOptions.text=str_middelName;
	}



	self.lblDesc.text=name;
	//self.lblSelectedOptions.textColor=[UIColor blackColor];
	self.lblSelectedOptions.textColor=[UIColor grayColor];
	if ([aData isRequired])
    {
		if(![aData hasSelectedData])
		   self.lblSelectedOptions.textColor=[UIColor redColor];

	}

	/*if([aData.maelOrExtra boolValue])
	{
		if(aData.selectedExtraMealsResponseList.count)
		{
			NSMutableString *selectedMeals = [NSMutableString string];
			for(int i=0; i< aData.selectedExtraMealsResponseList.count; i++)//WSExtraMealsResponse *extraMealsResponse in aData.selectedExtraMealsResponseList)
			{
				WSExtraMealsResponse *extraMealsResponse = [aData.selectedExtraMealsResponseList objectAtIndex:i];
				if(i == 0)
				{
					[selectedMeals appendString:extraMealsResponse.Name];
				}
				else
				{
					[selectedMeals appendString:[NSString stringWithFormat:@", %@" ,extraMealsResponse.Name]];
				}
			}
			[self.lblSelectedOptions setText:selectedMeals];
			[self.lblSelectedOptions setTextColor:[UIColor darkGrayColor]];
		}
	}
	else if(aData.selectedExtraMealsResponse)
	{
		[self.lblSelectedOptions setText:aData.selectedExtraMealsResponse.Name];
		[self.lblSelectedOptions setTextColor:[UIColor darkGrayColor]];
	}*/


/*
	else
	{
		if([aData.isNecessary boolValue])
		{
			[self.lblSelectedOptions setTextColor:[UIColor redColor]];
		}
		else
		{
			[self.lblSelectedOptions setTextColor:[UIColor darkGrayColor]];
		}

		[self.lblSelectedOptions setText:aData.nameExtraType];
	}*/
	[self.lblDesc setText:aData.description];

}




@end
