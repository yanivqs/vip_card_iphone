//
//  WSStreetsResponse.h
//  VIPCard
//
//  Created by Andrei on 29/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBaseData.h"

@interface WSStreetsResponse : WSBaseData
@property (strong ,nonatomic) NSString *id, *name;
@end
