//
//  DealOfTheDayCellCollectionViewCell.m
//  VIPCard
//
//  Created by Andrei on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "DealOfTheDayCell.h"

@implementation DealOfTheDayCell
@synthesize img_Expired;


-(void) layoutSubviews
{
    if (self.sContentSize.height > 0)
       [self.subScrollview setContentSize:self.sContentSize];
       
    [super layoutSubviews];
}


-(void) setSContentSize:(CGSize)sContentSize
{
    _sContentSize = sContentSize;
    if(_sContentSize.height > 0 && self.subScrollview)
    {
        [self performSelector:@selector(resetContentSize) withObject:nil afterDelay:0.0];
    }
}

-(void) resetContentSize
{
    if (self.sContentSize.height > 0)
        [self.subScrollview setContentSize:self.sContentSize];
}
@end
