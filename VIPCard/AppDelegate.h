//
//  AppDelegate.h
//  VIPCard
//
//  Created by SanC on 17/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CoreDataBase.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
	NSInteger index;
}
@property(nonatomic, assign) NSInteger index;


@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTaskIdentifier;

@property(nonatomic)BOOL callflag,makecallflag;
@property(nonatomic) int callcnt;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(void)openMainVC;
-(void)navigateLoginView;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@end

