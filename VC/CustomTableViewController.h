//
//  CustomTableViewController.h
//  VIPCard
//
//  Created by fis on 19/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VIPCardTableViewCell.h"
#import "BaseViewController.h"
@interface CustomTableViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *aTable;

@end
