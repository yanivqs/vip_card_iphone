//
//  JSONUtil.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 18/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONUtil : NSObject

+(NSString*)dicToJSON:(NSDictionary*)dic;
+(NSDictionary*)JSONStrToDic:(NSString*)jsonStr;
@end
