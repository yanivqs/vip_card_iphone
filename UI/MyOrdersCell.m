//
//  MyOrdersCell.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 26/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "MyOrdersCell.h"

@implementation MyOrdersCell
@synthesize lbl_ordDate,lbl_ordSum,btn_ordDetail,btnDel,orderObj,delegate;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)populateData:(MyOrders*)myOrder
{
    orderObj = myOrder;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yy"];
    NSString *ordDt = [df stringFromDate:myOrder.orderDate];
    
    self.textLabel.lineBreakMode =NSLineBreakByWordWrapping;
    self.textLabel.numberOfLines = 0;
    
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.lbl_ordDate.text = ordDt;//[listOfOrders objectAtIndex:indexPath.row];
    self.lbl_ordSum.text = [NSString stringWithFormat:@"%@",myOrder.orderSum];
    
    NSURL *imgurl = [NSURL URLWithString:myOrder.logoURL];
    [self.img_bzlogo sd_setImageWithURL:imgurl placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",PLACE_HOLDER_IMAGE]]];

    
}
-(IBAction)orderDetail:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(onOrderDetailClicked:)]) {
        [delegate onOrderDetailClicked:orderObj];
    }
    
}
-(IBAction)deleteOrder:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(onOrderDeleteClicked:)]) {
        [delegate onOrderDeleteClicked:orderObj];
    }
   
}
@end
