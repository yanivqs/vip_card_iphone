//
//  BussinessListCell.m
//  VIPCard
//
//  Created by SanC on 12/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BussinessListCell.h"

@implementation BussinessListCell
@synthesize lbl_Title,lbl_NewAmount,lbl_Discription,lbl_BottomLeftTitle,img_View,lbl_ExistingAmount;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
	[self.lbl_ExistingAmount setStrikethrough:YES];


    // Configure the view for the selected state
}

@end
