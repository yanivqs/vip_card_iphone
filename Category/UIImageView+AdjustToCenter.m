//
//  UIImageView+AdjustToCenter.m
//  Chain
//
//  Created by Nava Carmon on 6/6/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "UIImageView+AdjustToCenter.h"
#import "UIImage+Scale.h"

@implementation UIImageView (AdjustToCenter)

- (CGFloat)adjustImageForBounds:(UIImage *)img {
    
    CGSize imageSize = img.size;
    CGRect iconBounds = self.bounds;
    
    CGFloat ratio = imageSize.width/imageSize.height;
    
    imageSize.width = iconBounds.size.width;
    imageSize.height = round(imageSize.width/ratio);
    img = [img scaleToSize:imageSize];
    
	[self setImage:img];
    return img.size.height;
}

@end
