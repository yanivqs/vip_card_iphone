//
//  WSErrorMessage.m
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSErrorMessage.h"

@implementation WSErrorMessage


-(NSString *)errorMessage
{
    return [NSString stringWithFormat:@"%@-%@",self.error_code,self.error_msg];
    
}
@end
