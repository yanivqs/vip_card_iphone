//
//  BussinessPopup.m
//  VIPCard
//
//  Created by SanC on 24/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BussinessPopup.h"
#import "KeyConstant.h"
#import "BaseViewController.h"

@implementation BussinessPopup

- (void)drawRect:(CGRect)rect {
	// Drawing code
	self.btn_FilterBussiness.layer.cornerRadius = 12.0;
	self.btn_FilterBussiness.layer.masksToBounds = YES;
	self.btn_FindBussiness.layer.cornerRadius = 12.0;
	self.btn_FindBussiness.layer.masksToBounds = YES;
	UITapGestureRecognizer *singleFingerTap =
	[[UITapGestureRecognizer alloc] initWithTarget:self
											action:@selector(handleSingleTap:)];
	[self addGestureRecognizer:singleFingerTap];


}

+(BussinessPopup *) bussinessPopup:(CGRect)frame
{
	UIView *rootView = [[[NSBundle mainBundle] loadNibNamed:@"BussinessPopup" owner:nil options:nil] lastObject];
	rootView.frame = frame;

	return (BussinessPopup *)rootView;

}

- (IBAction)onClick_FindBussiness:(id)sender {

	if(self.bussDelegate && [self.bussDelegate respondsToSelector:@selector(businessPopup:withButtonIndex:) ])
	{
		[self.bussDelegate businessPopup:self withButtonIndex:1];
	}

	[self removeFromSuperview];

}

- (IBAction)onClick_FilterBussiness:(id)sender {

	if(self.bussDelegate && [self.bussDelegate respondsToSelector:@selector(businessPopup:withButtonIndex:) ])
	{
		[self.bussDelegate businessPopup:self withButtonIndex:2];
	}

	[self removeFromSuperview];
	
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
	[self removeFromSuperview];
}
@end
