//
//  WSBaseResponse.m
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSBaseResponse.h"

@implementation WSBaseResponse

- (id) initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self)
        [self parseWithDictionary:dict];
    
    return self;
}

-(void) parseWithDictionary:(NSDictionary *)dict
{
    _serverResponse = dict;
    id param = [dict objectForKey:@"header"];
    if(param && [param isKindOfClass:[NSDictionary class]])
    {
        WSResponseHeader *responseHeader = [[WSResponseHeader alloc] initWithDictionary:param];
        
        NSTimeZone *timeZone = [NSTimeZone localTimeZone];
        responseHeader.time_zone = timeZone.name;
        self.header = responseHeader;
    }
    
    param = [dict objectForKey:@"result"];
    if(param && [param isKindOfClass:[NSDictionary class]])
    {
        self.result = [[WSResult alloc] initWithDictionary:param];
    }
    
    param = [dict objectForKey:@"responseBody"];
    if(param)
    {
        self.respondeBody = param;
    }

    param = [dict objectForKey:@"method"];
    if(param && [param isKindOfClass:[NSString class]])
    {
        self.method = param;
    }
}

@end
