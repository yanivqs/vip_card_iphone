//
//  WSBZDeliveryTime.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 03/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSBZDeliveryTime : WSBaseData

/*
 "Id": "1",
 "IdBusiness": "4",
 "OpenHour": "09:30:00",
 "CloseHour": "12:30:00",
 "IdDay": "4"
*/

@property(nonatomic,strong) NSString *Id,*IdBusiness,*OpenHour,*CloseHour,*IdDay;
@end
