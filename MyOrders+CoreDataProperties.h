//
//  MyOrders+CoreDataProperties.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 17/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MyOrders.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyOrders (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *orderId;
@property (nullable, nonatomic, retain) NSDate *orderDate;
@property (nullable, nonatomic, retain) NSNumber *orderSum;
@property (nullable, nonatomic, retain) NSString *logoURL, *orderObj,*cartData;;

@end

NS_ASSUME_NONNULL_END
