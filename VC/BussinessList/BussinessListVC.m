//
//  BussinessListVC.m
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "AppGlobalConstant.h"
#import "WSConstants.h"
#import "MarketDealsResponce.h"
#import "BussinessListVC.h"
#import "BussinessCustomCell.h"
#import "WSBussiness.h"
#import "WSCategoryMealResponse.h"
#import "YSLContainerViewController.h"
#import "CategoryForMealListVC.h"
#import "CategoryForMealHolderVC.h"
#import "CatTopProduct.h"
#import "MarketProductVC.h"
#import "MarketPlaceHolderVC.h"
#import "ViPListViewController.h"
#import "SWRevealViewController.h"

@interface BussinessListVC () <ServerResponseDelegate,UITextFieldDelegate,SWRevealViewControllerDelegate>
{
    NSArray *bussinessList;
    NSMutableArray *viewControllers;
    WSBussiness *bussinObj;
    
    NSUInteger currentIndex;
    	NSMutableArray *filter;
}

@property (nonatomic) IBOutlet UIButton* revealButtonItem;
@property (nonatomic,strong) NSArray *bussinessList;

@end

@implementation BussinessListVC
@synthesize bussinessObjResponse;
@synthesize bussinessList = bussinessList;


- (void)viewDidLoad
{
    
    [super viewDidLoad];
        [self showBackButton:YES];
    
    [self customSetup];

    if (_isMarketPlace) {
        _con_searchbarheight.constant=40;
        
        [self.searchbtn setTitle:NSLocalizedString(@"title_activity_market", @"title_activity_market") forState:UIControlStateNormal];
        
        [self getMarketist];
    }
    else
    {
        bussinessList = self.bussinessObjResponse.bussinessList;
    }
  
    
    
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 	[self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];
    [self reloadScreenData];

}

-(void) reloadScreenData{
    [self.collectionView layoutIfNeeded];
    [self.collectionView reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (_isSearch)
     {
         return self.filteredList.count;
    }
    return bussinessList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"BussinessCustomCell";
    BussinessCustomCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    //[[bussinessList objectAtIndex:indexPath.row] isKindOfClass:[MarketDealsResponce class]]
    if (_isMarketPlace)
    {
        MarketDealsResponce *marketObj;
        if(_isSearch)
            marketObj = [self.filteredList objectAtIndex:indexPath.row];
        else
            marketObj = [bussinessList objectAtIndex:indexPath.row];
        
        NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImages,marketObj.logoB]];
        [[cell bussinessImg] sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",PLACE_HOLDER_IMAGE]]];
        
        //cell.lblName.text = [NSString stringWithFormat:@"%@",bussinessObj.name];
        cell.lblName.hidden=YES;
        cell.layer.cornerRadius = 10.0;
        cell.layer.masksToBounds = YES;
    }
    else
    {
        WSBussiness *bussinessObj = nil;
        if(_isSearch)
        {
            bussinessObj = [self.filteredList objectAtIndex:indexPath.row];
        }
        else{
            bussinessObj = [bussinessList objectAtIndex:indexPath.row];
        }
        
        
		NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImagesMealsDeals,bussinessObj.logo]];//26/05/
        [[cell bussinessImg] sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",PLACE_HOLDER_IMAGE]]];
        
        cell.lblName.text = [NSString stringWithFormat:@"%@",bussinessObj.name];
        cell.layer.cornerRadius = 10.0;
        cell.layer.masksToBounds = YES;
    
    }
        return cell;
}



#pragma mark-UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_isMarketPlace)
    {
        
           //MarketDealsResponce *marketObj = [bussinessList objectAtIndex:indexPath.row];
        MarketDealsResponce *marketObj;
        if(_isSearch)
            marketObj = [self.filteredList objectAtIndex:indexPath.row];
        else
            marketObj = [bussinessList objectAtIndex:indexPath.row];
        
        if(marketObj && [marketObj isKindOfClass:[MarketDealsResponce class]])
        {
            
            MarketPlaceHolderVC *marketVC = [self getVCWithSB_ID:kMarketPlaceHolderVC_SB_ID];
            marketVC.marketPlaceId = marketObj.id;
            if(![marketObj.id isEqual:@"-1"])
            {
                marketVC.marketBusinessName = marketObj.nameB;
                marketVC.expiredDate=marketObj.dateExpert;
                marketVC.limitation1=marketObj.limitation;
                
            }
            [self.navigationController pushViewController:marketVC animated:YES];
        }

//        MarketDealsResponce *marketObj = [bussinessList objectAtIndex:indexPath.row];
//        [self getCatTopProduct:marketObj.id];
    
    }
    else
    {
        WSBussiness *bussinessObj;
        if(_isSearch)
        {
            bussinessObj = [self.filteredList objectAtIndex:indexPath.row];
        }
        else{
            bussinessObj = [bussinessList objectAtIndex:indexPath.row];
        }
        
       // WSBussiness *bussinessObj = [bussinessList objectAtIndex:indexPath.row];
        bussinObj = bussinessObj;
		// [[AppGlobalData sharedManager] setListOfCartItems:[[NSMutableArray alloc] init]];
        [self getCatforMealByBussinessID:bussinessObj.id];

    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //collectionView:layout:sizeForItemAtIndexPath:indexPath.item;
    if(IS_LARGE_SCREEN)
    {
        return CGSizeMake(150,150);
    }
    else
    {
        return CGSizeMake(130, 130);
    }
}

-(void)getCatforMealByBussinessID:(NSString *)bussinessID
{
    [[ConnectionsManager sharedManager] getCatMealforBussinesswithID:bussinessID delegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:YES];
}
-(void)getMarketist
{
    [[ConnectionsManager sharedManager] getMarketList_withdelegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:YES];
}

//-(void)getCatTopProduct:(NSString *)marketID
//{
//    [[ConnectionsManager sharedManager] getCatTopProduct_withdelegate:marketID delegate:self];
//    [DejalActivityView activityViewForView:WINDOW andAllowTouches:YES];
//}



//ServerResponseDelegate
-(void)success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
    if([response.method isEqualToString:mFetchCatMealsForBussiness])
    {
		//  NSLog(@"%@", response.respondeBody);
            NSArray *responseList = response.respondeBody;
            if(responseList.count)
            {
                NSMutableArray *temp = [NSMutableArray array];
                for(NSDictionary *dic in responseList)
                {
                    WSCategoryMealResponse *bussinessResponse = [[WSCategoryMealResponse alloc] initWithDictionary:dic];
                    [temp addObject:bussinessResponse];
                }
                NSArray *bussineesListTemp = temp;
                
                
                CategoryForMealHolderVC *categoryMealHolderVC = [self getVCWithSB_ID:kCategoryForMealHolder_SB_ID];
                categoryMealHolderVC.baseviewTitle = self.title;
                categoryMealHolderVC.categoryList = bussineesListTemp;
				categoryMealHolderVC.selectedBussiness = bussinObj;
				[self.navigationController.navigationBar setBarTintColor:[UIColor redColor]];
                [self.navigationController pushViewController:categoryMealHolderVC animated:YES];
            }
        }
    else if([response.method isEqualToString:mfetchMarketDeal])
    {
        if([response.respondeBody isKindOfClass:[NSArray class]])
        {
            NSArray *responseList = response.respondeBody;
            if(responseList.count)
            {
                NSMutableArray *temp = [NSMutableArray array];
                for(NSDictionary *dic in responseList)
                {
                    MarketDealsResponce *marketResponse = [[MarketDealsResponce alloc] initWithDictionary:dic];
                    [temp addObject:marketResponse];
                }
                
                //add all market object
                 MarketDealsResponce *marketResponse = [[MarketDealsResponce alloc] init];
                marketResponse.logoB=@"allMarket.png";
                marketResponse.id=@"-1";
                marketResponse.dateExpert = @"";
                marketResponse.limitation = @"";
                [temp addObject:marketResponse];
                
                
                bussinessList = temp;

                [_collectionView  reloadData];
            }
        }
    }
    
    
}

-(void)failure:(WSBaseResponse *)response
{
    [DejalActivityView removeView];

    if([response.method isEqualToString:@"fetchCatMealForBusiness"])
    {
		// NSLog(@"%@", response.respondeBody);
    }
}
-(void)searchCouponsby:(NSString *)aStr
{
    
        if (_isMarketPlace)
        {
            if(aStr)
            {
                NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.nameB contains[cd] %@)",aStr];
                
                NSArray *filterArray = [bussinessList filteredArrayUsingPredicate:cityPredicate];
                
                self.filteredList = filterArray;
                self.isSearch = YES;
                
            }
            else {
                self.isSearch = NO;
                self.filteredList = self.bussinessList;
            }
            
            [self.collectionView reloadData];
            
        }
      /*  else{
            //search in businesslist
            
            ViPListViewController *vc = [viewControllers objectAtIndex:currentIndex];
            if([vc isKindOfClass:[ViPListViewController class]])
            {
                if(aStr){
                    NSPredicate *cityPredicate = [NSPredicate predicateWithFormat:@"(SELF.name contains[cd] %@)",aStr];
                    NSArray *filterArray = [vc.dealsResponseObj.couponsList filteredArrayUsingPredicate:cityPredicate];
                    vc.filteredCouponsList = filterArray;
                    vc.isSearch = YES;
                }
                else {
                    vc.filteredCouponsList = vc.dealsResponseObj.couponsList;
                    vc.isSearch = NO;
                }
                
                
                [vc viewWillAppear:YES];
            }
       
        }*/
    
    
   //  NSArray *filteredArray = [self.dealsResponseObj.couponsList filteredArrayUsingPredicate:cityPredicate];

}

   


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.isSearch = YES;
    [self searchCouponsby:self.txtFldSearch.text];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *searchString = self.txtFldSearch.text;
    
    if([string isEqualToString:@""])
        searchString = [searchString substringToIndex:[searchString length] - 1];
    else
        searchString = [searchString stringByAppendingString:string];
    
    if(searchString.length <= 0)
    {
        self.isSearch = NO;
        //[self.collectionView reloadData];
    }

    [self searchCouponsby:searchString];
    return YES;
}

-(void)onClickCancelButton
{
    self.isSearch = NO;
    [self.collectionView reloadData];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.isSearch = YES;
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (IBAction)onClickCancelButton:(id)sender
{
    self.isSearch = NO;
    [self.collectionView reloadData];
}
@end
