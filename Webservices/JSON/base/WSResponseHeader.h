//
//  WSResponseHeader.h
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBaseData.h"

@interface WSResponseHeader : WSBaseData

@property (nonatomic,strong) NSString *userId;
@property (nonatomic,strong) NSString *token;
@property (nonatomic,strong) NSString *version;
@property (nonatomic,strong) NSNumber *creationTime;
@property (nonatomic,strong) NSNumber *validityTime;
@property (nonatomic,strong) NSString *deviceType;
@property (nonatomic,strong) NSString *registrationId;
@property (nonatomic, strong) NSString *time_zone;
@property (nonatomic, strong) NSString *mobileDeviceId;
@end
