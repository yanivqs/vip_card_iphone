//
//  WSAdditionalExtrasResponse.h
//  VIPCard
//
//  Created by Vishal Kolhe on 27/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSAdditionalExtrasResponse : WSBaseData
/*
 {
 "id":"1",
 "name":"תבלין לפיצה",
 "price":"0",
 "idBusiness":"3"
 },
 */

@property (nonatomic, strong) NSString *id,*name,*price,*idBusiness;
//-(id)initWithDictionary:(NSDictionary *)dict;

@property(nonatomic,assign) BOOL selected;


@end
