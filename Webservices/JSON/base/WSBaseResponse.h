//
//  WSBaseResponse.h
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSResponseHeader.h"
#import "WSResult.h"

@interface WSBaseResponse : NSObject
{
    
}
@property (nonatomic,strong) WSResponseHeader *header;
@property (nonatomic,strong) WSResult *result;
@property (nonatomic,strong) NSString *method;
@property (nonatomic,strong) id respondeBody;
@property (nonatomic,strong) NSString *version;


//@property (nonatomic, strong) id accounts;
@property (nonatomic,readonly) NSDictionary *serverResponse;

- (id) initWithDictionary:(NSDictionary *)dict;
-(void) parseWithDictionary:(NSDictionary *)dict;


@end
