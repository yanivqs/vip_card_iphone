//
//  WSFetchCouponsResponse.h
//  VIPCard
//
//  Created by Vishal Kolhe on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSFetchCouponsResponse : WSBaseData
@property (nonatomic, strong) NSString *id, *name;
@property (nonatomic, strong) NSArray *couponsList;

//-(id)initWithDictionary:(NSDictionary *)dict;
@end
