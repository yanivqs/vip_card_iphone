//
//  FilterBussinessVC.m
//  MyStore
//
//  Created by SanC on 11/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "FilterBussinessVC.h"

@interface FilterBussinessVC ()

@end

@implementation FilterBussinessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	dict_data = @{@"B" : @[@"Bear", @"Black Swan", @"Buffalo"],
				@"C" : @[@"Camel", @"Cockatoo"],
				@"D" : @[@"Dog", @"Donkey"],
				@"E" : @[@"Emu"],
				@"G" : @[@"Giraffe", @"Greater Rhea"],
				@"H" : @[@"Hippopotamus", @"Horse"],
				@"K" : @[@"Koala"],
				@"L" : @[@"Lion", @"Llama"],
				@"M" : @[@"Manatus", @"Meerkat"],
				@"P" : @[@"Panda", @"Peacock", @"Pig", @"Platypus", @"Polar Bear"],
				@"R" : @[@"Rhinoceros"],
				@"S" : @[@"Seagull"],
				@"T" : @[@"Tasmania Devil"],
				@"W" : @[@"Whale", @"Whale Shark", @"Wombat"]};
	  array_SectionTitles = [[dict_data allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	 array_IndexTitles = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
	searchBar.barTintColor = [UIColor whiteColor];
	searchBar.backgroundColor=[UIColor whiteColor];
	[searchBar setImage:[[UIImage imageNamed: @"Search_icone"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
//	[searchBar setPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];
//	[searchBar setSearchTextPositionAdjustment:UIOffsetMake(0, 0)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return array_SectionTitles.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	NSString *sectionTitle = [array_SectionTitles objectAtIndex:section];
	NSArray *sectionData = [dict_data objectForKey:sectionTitle];
	return [sectionData count];

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return [array_SectionTitles objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

	// Configure the cell...
	NSString *sectionTitle = [array_SectionTitles objectAtIndex:indexPath.section];
	NSArray *sectionData = [dict_data objectForKey:sectionTitle];
	NSString *str_data = [sectionData objectAtIndex:indexPath.row];
	cell.textLabel.text = str_data;
	return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
	//  return animalSectionTitles;
	return array_IndexTitles;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
	return [array_SectionTitles indexOfObject:title];
}
@end
