//
//  FactoryInfoContex.h
//  VIPCard
//
//  Created by Manoj Kumar on 11/9/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FactoryInfoContex : NSObject
@property (strong , nonatomic) NSString *factoryid, *paidByFactory, *deliveryOrderTime,*startDate,*endDate;
@property (strong , nonatomic)NSDate *lateOrderStartDate,*lateOrderEndDate;
+ (id)sharedManager;
@end
