//
//  WSExtraForMealsResponse.m
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSExtraForMealsResponse.h"

@implementation WSExtraForMealsResponse
@synthesize id, idMeal, description, price, isChoice, idExtraType, idChoice, maelOrExtra, isNecessary, nameExtraType, nameChoice, selectedExtraMealsResponse = _selectedExtraMealsResponse, selectedExtraMealsResponseList = _selectedExtraMealsResponseList,strSelectedChoice = _strSelectedChoice,extraForMealPrice;

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
    if([super initWithDictionary:dict])
    {
        [self parseWithDictionary:dict];
    }
    return self;
}
 */

-(void)populateFromDictionary:(NSDictionary *)dict
{
    [super populateFromDictionary:dict];
    
    NSArray *tempExtraMealsList = [dict objectForKey:@"extraMeals"];
    if(tempExtraMealsList.count)
    {
        NSMutableArray *array = [NSMutableArray array];
        for(NSDictionary *dic in tempExtraMealsList)
        {
            WSExtraMealsResponse *extraResponseObj = [[WSExtraMealsResponse alloc] initWithDictionary:dic];
            [array addObject:extraResponseObj];
        }
        self.extraMealsList = array;
    }
    //Check if is Necessary is number
    id param = [dict objectForKey:@"isNecessary"];
    if (param)
    {
        if([param isKindOfClass:[NSNumber class]])
            {
                self.isNecessary = [param stringValue];
            }
        
    }
}

-(BOOL) hasSelectedData
{
	if([self isMultiChoice])
	{
		if(_selectedExtraMealsResponseList != nil && _selectedExtraMealsResponseList.count > 0)
			return TRUE;
	}
	else if(_selectedExtraMealsResponse != nil)
	{
		return TRUE;
	}

	return FALSE;
}

-(BOOL) isMultiChoice
{
    return (maelOrExtra && [maelOrExtra boolValue]);
}
-(BOOL) isRequired
{
    return (isNecessary && [isNecessary boolValue]);
}


-(void) setSelectedExtraMealsResponse:(WSExtraMealsResponse *)selectedExtraMealsResponse
{
	_selectedExtraMealsResponse = selectedExtraMealsResponse;
	[self populateDisplayString];
}

-(void) setSelectedExtraMealsResponseList:(NSArray *)selectedExtraMealsResponseList
{
	_selectedExtraMealsResponseList = selectedExtraMealsResponseList;
	[self populateDisplayString];
}

-(void) populateDisplayString {
	_strSelectedChoice = @"";
	if([self isMultiChoice])
	{
		if(_selectedExtraMealsResponseList != nil && _selectedExtraMealsResponseList.count > 0)
		{
			NSMutableArray *arr = [NSMutableArray array];
			for(WSExtraMealsResponse *item in _selectedExtraMealsResponseList)
				[arr addObject:item.Name];

			_strSelectedChoice = [arr componentsJoinedByString:@", "];
		}


	}
	else if(_selectedExtraMealsResponse != nil)
	{
		_strSelectedChoice =  _selectedExtraMealsResponse.Name;
	}

}

-(NSString *) dispSelectedData {

	return _strSelectedChoice;
}

@end
