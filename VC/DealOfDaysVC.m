//
//  DetailedViewController.m
//  VIPCard
//
//  Created by SanC on 18/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//
#import "DealOfDaysVC.h"
#import "DealOfTheDayCell.h"
#import "ShoppingCartSummaryVC.h"
#import "WSCouponsResponse.h"
#import "BaseViewController.h"
#import "code39.h"
#import "WSBusinessCatResponse.h"
#import "CategoryForMealHolderVC.h"
#import "ReportError.h"
#import "CategoryForMealDetailVC.h"
#import "WSCategoryMealResponse.h"

@interface DealOfDaysVC () <ServerResponseDelegate>
{
    NSInteger selectedDeal,longitude,latitude;
    NSMutableArray *array;
    WSCouponsResponse *wsCouponsRes;
    int selectedIndex,currIndex;
    CGSize scrollContentSize;
    ReportError *reportErr;
}
@end

@implementation DealOfDaysVC
@synthesize collectionView = _collectionView;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackButton:YES];
 if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
	 self.edgesForExtendedLayout = UIRectEdgeNone;

    array = [[NSMutableArray alloc] init];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    
    
    if (selectedDeal == 0)
    {
        _btnPrev.hidden = YES;
    }
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view layoutIfNeeded];
    [self getDealOfDay];
	_btnPrev.hidden = YES;
	_btnNext.hidden=YES;

	if(IS_IPHONE_4)
			self.consTop_collectionView.constant=84;


	


}

-(void)getDealOfDay
{
    [[ConnectionsManager sharedManager] fetchDealOfCoupons_withdelegate:self];
    [DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}

-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    [self performSegueWithIdentifier:@"tableSegue" sender:nil];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [array count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DealOfTheDayCell *dealcell = (DealOfTheDayCell *)[_collectionView dequeueReusableCellWithReuseIdentifier:@"dealcell" forIndexPath:indexPath];
    [self populatedDealOfTheDayCell:dealcell indePath:indexPath];
    return dealcell;
}
/*
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_5)
    {
       return CGSizeMake(320.f, 568.f-64);
    }
    else if(IS_IPHONE_6)
    {
        return CGSizeMake(375.f, 667.f-64);
    }
    else
    {
       return CGSizeMake(414.f, 736.f-64);
    }
}
*/



-(void)populatedDealOfTheDayCell:(DealOfTheDayCell*)cell indePath:(NSIndexPath*)index
{
    WSCouponsResponse *dealOfDayResponse = [array objectAtIndex:index.row];
    
    CGFloat subViewHeight = 564.f;
    cell.cons_SubviewWidth.constant = cell.frame.size.width;
    [cell.btnCall addTarget:self action:@selector(touchToCall:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnWaze addTarget:self action:@selector(touchToWaze:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCopouButton addTarget:self action:@selector(touchToCopoun:) forControlEvents:UIControlEventTouchUpInside];
	cell.btnCopouButton.layer.cornerRadius = 10.0;
	cell.btnCopouButton.layer.masksToBounds = YES;
	[cell.btnCopouButton setTitle:NSLocalizedString(@"btn_use_coupon", @"btn_use_coupon") forState:UIControlStateNormal];

    cell.btnCall.tag = index.row;
    cell.btnCopouButton.tag = index.row;
    
    cell.btnWaze.tag = index.row;
    
    cell.btnWaze.hidden = !(![NSString isEmpty:dealOfDayResponse.latitude] && ![NSString isEmpty:dealOfDayResponse.longitude]); //HIDE if not exist
   
    

    [cell.imgProduct sd_setImageWithURL:[self baseImageURL:dealOfDayResponse.image] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
//	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImages,dealOfDayResponse.logobusiness]];
    //TODO SANC verify the right link
    [cell.imgLogo sd_setImageWithURL:[self baseImageURL:dealOfDayResponse.logobusiness] placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];
    
    cell.lblShortTitle.text = NSLocalizedString(@"OfferTitle", @"OfferTitle");
    cell.lblShortTitle.textColor = COLOR_TITLE_NEVY_BLUE;
    cell.lblLongDesTitle.text = NSLocalizedString(@"lbl_long_desc", @"lbl_long_desc");
    cell.lblLongDesTitle.hidden=YES;
    cell.lblLimitationTitle.text = NSLocalizedString(@"lbl_limitation", @"lbl_limitation");
    
    cell.lblTitle.text = dealOfDayResponse.name;
    cell.lblDiscription2.text = dealOfDayResponse.offer;
    cell.lblDiscription1.text = dealOfDayResponse.des;
    cell.lblDiscription1.hidden=YES;
    cell.lblDiscription3.text = dealOfDayResponse.marks;
   
    cell.lblPrice2.text = [NSString stringWithFormat:@"%@%@",SHEKEL, dealOfDayResponse.price1];
    [cell.lblPrice2 setStrikethrough:YES];
    cell.lblPrice.text = [NSString stringWithFormat:@"%@%@",SHEKEL, dealOfDayResponse.price2];
    cell.lblPrice.textColor = COLOR_MAIN_PRICE;
    //@gauri added
    cell.lbl_comment.text=[NSString stringWithFormat:@"%@:",NSLocalizedString(@"ForClarification", @"ForClarification")];
     
    NSDate *date = [DateTimeUtil dateFromStringDate:dealOfDayResponse.expiredDate withFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [NSString stringWithFormat:@"%@/%@/%@",[DateTimeUtil dayFromDate:date],[DateTimeUtil monthsFromDate:date],[DateTimeUtil yearFromDate:date]];
    
    cell.lblDate.text = [NSString stringWithFormat:@"%@ - %@",NSLocalizedString(@"lbl_dateTitle", @"lbl_dateTitle"),dateString];
    NSDate *currentDate = [NSDate date];
   
        
    CGSize rect = [cell.lblTitle.text getStringHeight:cell.lblTitle.text toWidth:cell.lblTitle.frame toFontName:@"Helvetica" toSize:16];
    cell.titleConstaintHeight.constant = rect.height;
    
    if(rect.height > 21)
        subViewHeight = subViewHeight - 21 + rect.height;
    
    //CGSize stringSize = [self getStringSize:cell.lblDiscription1 andString:cell.lblDiscription1.text];
    cell.discription1ConstraintHeight.constant = 0;//stringSize.height;
    /*
    if(stringSize.height > 21)
        subViewHeight = subViewHeight - 21 + stringSize.height;
     */
    
    CGSize stringSize = [self getStringSize:cell.lblDiscription2 andString:cell.lblDiscription2.text];
    cell.discription2ConstraintHeight.constant = stringSize.height;
    
    if(stringSize.height > 21)
        subViewHeight = subViewHeight - 21 + stringSize.height;
    
    stringSize = [self getStringSize:cell.lblDiscription3 andString:cell.lblDiscription3.text];
    cell.discription3ConstraintHeight.constant = stringSize.height;
    
    if(stringSize.height > 21)
        subViewHeight = subViewHeight - 21 + stringSize.height;
    
    [cell.lblDiscription3 sizeToFit];
     //cell.consBottomView_BottomCons.constant =  cell.cons_SubViewHeight.constant - (lblDescFrame.size.height + lblDescFrame.origin.y) + 130;
   // cell.con_bottomViewHeight.constant=20;
    //cell.consBottomView_BottomCons.constant = 20.0f;


    if(dealOfDayResponse.isbarcode)
    {
		 cell.imgBarcode.image = [self generateCode:wsCouponsRes.barcode];
        cell.imgBarcode.hidden = NO;
        cell.lblBarcodeText.text=wsCouponsRes.barcode;
        cell.barcodeViewHeightConstraints.constant =128;
        cell.barcodeView.hidden = NO;
        cell.btnCopouButton.hidden = YES;
        cell.cons_lblBarcodeHeight.constant = 21.0;
    }
    else
    {
		cell.imgBarcode.image = [self generateCode:wsCouponsRes.barcode];
        cell.imgBarcode.hidden = YES;
        cell.cons_lblBarcodeHeight.constant = 0;
        cell.barcodeViewHeightConstraints.constant = 0;
        
        cell.barcodeView.hidden = YES;
        subViewHeight = subViewHeight - 128;
        cell.btnCopouButton.hidden = NO;
    }
	//shilpa perform changes here

    BOOL isExpiredDate = [self checkExpireyDate:currentDate fromDate:date];
    if (isExpiredDate)
    {
        cell.btnCopouButton.hidden = YES;
        cell.btnCopouButton.enabled = NO;
		cell.img_Expired.hidden=NO;

//		UIImageView *img_View=[[UIImageView alloc] initWithFrame:CGRectMake(123, 40, 298, 270)];
//		img_View.image=[UIImage imageNamed:@"img_Expired"];
//		img_View.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//		[cell addSubview:img_View];



    }
    else
    {
        cell.btnCopouButton.hidden = NO;
        cell.btnCopouButton.enabled = YES;
		cell.img_Expired.hidden=YES;

    }
    
   // CGRect lblDescFrame = cell.lblDiscription3.frame;
    //CGRect lblcommentFrame=cell.lbl_comment.frame;
    cell.cons_SubViewHeight.constant = subViewHeight + 20.f;//lblDescFrame.origin.y + lblDescFrame.size.height+10;
    
  //  NSLog(@"%@ --- %f,%f",dealOfDayResponse.name,cell.cons_SubViewHeight.constant,subViewHeight );

    
     [cell.subView updateConstraintsIfNeeded];
     //[cell.subScrollview setContentSize:CGSizeMake(self.view.frame.size.width,cell.subView.frame.origin.y + cell.subView.frame.size.height+200)];
    scrollContentSize = CGSizeMake(self.view.frame.size.width,cell.subView.frame.origin.y + cell.cons_SubViewHeight.constant+150);
	if (IS_IPHONE_4) {
		scrollContentSize = CGSizeMake(self.view.frame.size.width,cell.subView.frame.origin.y + cell.cons_SubViewHeight.constant+1130);

	}
     [cell.subScrollview setContentSize:scrollContentSize];
    cell.sContentSize = scrollContentSize;
    //reset contentSize
    [self resetScrollViewSize:cell.subScrollview];
    [cell.subScrollview setNeedsLayout];
   
    
}

- (void)selectItemAtIndexPath:(nullable NSIndexPath *)indexPath animated:(BOOL)animated scrollPosition:(UICollectionViewScrollPosition)scrollPosition
{
    //NSLog(@"selectItemAtIndexPath: %ld",indexPath.row);
}
-(void) resetScrollViewSize:(UIScrollView *)aScrollView
{
    CGRect contentRect = CGRectZero;
    for (UIView *view in aScrollView.subviews) {
        
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    
    contentRect.size.width = aScrollView.frame.size.width;
    
    if(aScrollView.frame.size.height < contentRect.size.height)
        
    contentRect.size.height +=20;
    [aScrollView setContentSize:contentRect.size];

	scrollContentSize = contentRect.size;


}




#define Button Action

-(IBAction)touchToCall:(id)sender
{
    UIButton *btnCopoun = (UIButton*)sender;
    WSCouponsResponse *wsCoiponsRes = [array objectAtIndex:btnCopoun.tag];
    
    NSString *phNo = wsCoiponsRes.phone;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
    
}
-(IBAction)touchToWaze:(id)sender
{
//@Gauri added
    UIButton *btnWaze = (UIButton*)sender;
    if(!array || btnWaze.tag >= array.count || btnWaze.tag < 0)
        return;
    
    WSCouponsResponse *wsCoiponsRes = [array objectAtIndex:btnWaze.tag];
    
  if(wsCoiponsRes.latitude && wsCoiponsRes.longitude)
  {
    
    NSString *urlStr =
    [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes&z=10",
     [wsCoiponsRes.latitude doubleValue],[wsCoiponsRes.longitude doubleValue]];
    
    if ([[UIApplication sharedApplication]
         canOpenURL:[NSURL URLWithString:@"waze://"]]) {
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        
    } else {
        
        // Waze is not installed. Launch AppStore to install Waze app
        [[UIApplication sharedApplication] openURL:[NSURL
                                                    URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
  }

//
//    UIButton *btnCopoun = (UIButton*)sender;
//    WSCouponsResponse *wsCoiponsRes = [array objectAtIndex:btnCopoun.tag];
//	[self navigateToLatitude:[wsCoiponsRes.latitude doubleValue] longitude:[wsCoiponsRes.longitude doubleValue] address:@""];
}
-(IBAction)touchToCopoun:(id)sender
{
    UIButton *btnCopoun = (UIButton*)sender;
    selectedIndex = (int)btnCopoun.tag;
    WSCouponsResponse *wsCoiponsRes = [array objectAtIndex:btnCopoun.tag];
     wsCouponsRes = wsCoiponsRes;
	// if([self islogin])
	// {

		NSString *idMeal = wsCoiponsRes.idMeal;
		if (idMeal && ![idMeal isEmpty])
		{
			//UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
			NSString *subCat = wsCoiponsRes.idCategor; //
			if([subCat integerValue] == 64)
			{

				//Fetch business meal by category id
				[self fetchMealsOfBusiness:wsCoiponsRes.idBusiness subCategory:wsCoiponsRes.idCategor];//Call API
				return;
			}
			else if([idMeal integerValue] > 0 )
			{
				//if([[[AppGlobalData sharedManager] getBussinessCart:wsCoiponsRes.idBusiness] count] > 0)
				//{
					ShoppingCartSummaryVC *summaryVC = [self.storyboard instantiateViewControllerWithIdentifier:kShoppingCartSummaryVC_SB_ID];
					summaryVC.isFromCart = NO;
					summaryVC.businessId = wsCoiponsRes.idBusiness;
					summaryVC.selectedBussiness = nil;
					summaryVC.wsCouponsResponse = wsCoiponsRes;
					[self.navigationController pushViewController:summaryVC animated:YES];
				//}

				return;
			}
		}
    [self askBarcodePassword];

		// }
    /*else
    {
        [self showPopupwithmessage:2 toShowValue:nil];
    }*/
	 //else
}

-(void) askBarcodePassword{
    UIAlertView *inputAlterView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirmation", @"Confirmation") message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
    inputAlterView.alertViewStyle = UIAlertViewStylePlainTextInput;
    inputAlterView.tag=100;
    [inputAlterView show];
    
}

-(void) reTryAlert{
    UIAlertView *inputAlterView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"msg_wrong_passord", @"You want to try again?") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"lbl_try_again", @"Try Again") otherButtonTitles:NSLocalizedString(@"lbl_btn_close", @"lbl_btn_close"), nil];
    inputAlterView.tag=200;
    [inputAlterView show];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
  
    if(alertView.tag == 100)
    {
        
        if (alertView.cancelButtonIndex == buttonIndex)
        {
            NSString *title = [alertView textFieldAtIndex:0].text;
            
            if([title isEqualToString:wsCouponsRes.code])
            {
                wsCouponsRes.isbarcode = YES;
                [array replaceObjectAtIndex:selectedIndex withObject:wsCouponsRes];
                [_collectionView reloadData];
            }
            else
            {
                // [[[[iToast makeText:NSLocalizedString(@"wrongBarCode", @"wrongBarCode")] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal] show];
                [self performSelector:@selector(reTryAlert) withObject:nil afterDelay:0.0];
                
            }
        }
    }
    if(alertView.tag == 200)
    {
        
        if (alertView.cancelButtonIndex == buttonIndex)
        {
            [self performSelector:@selector(askBarcodePassword) withObject:nil afterDelay:0.0];
                
        }
    }
    
}


-(void)barcodeimagegenerator
{
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"doddetails"]) {
        ShoppingCartSummaryVC *imageDetail = [segue destinationViewController];
        imageDetail.wsCouponsResponse = wsCouponsRes;
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        // present you c here
        
        [self presentViewController:imageDetail animated:YES completion:NULL];
    }];
    
    }
}


- (IBAction)touchPrev:(id)sender
{
    selectedDeal -- ;
    _btnNext.hidden = NO;
    
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:selectedDeal inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    if (selectedDeal == 0)
    {
        _btnPrev.hidden = YES;
        return;
    }
}

- (IBAction)touchNext:(id)sender {

    selectedDeal ++ ;
    _btnPrev.hidden = NO;
    
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:selectedDeal inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    if (selectedDeal == array.count-1)
    {
        _btnNext.hidden = YES;
        return;
    }
    
}


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
 {
    
    *targetContentOffset = scrollView.contentOffset; // set acceleration to 0.0
    float pageWidth = (float)self.collectionView.bounds.size.width;
    
    int cellToSwipe = (scrollView.contentOffset.x)/(pageWidth) + 0.5; // cell width + min spacing for lines
    if (cellToSwipe < 0) {
        cellToSwipe = 0;
    } else if (cellToSwipe >= array.count-1 ) {
        cellToSwipe = (int) array.count - 1;
    }
    
    _btnPrev.hidden = NO;
    _btnNext.hidden = NO;
    if(cellToSwipe == 0)
    {
      //  _btnNext.hidden = NO;
        _btnPrev.hidden = YES;
    }
    
    if(cellToSwipe >= array.count-1)
    {
        _btnPrev.hidden = NO;
        _btnNext.hidden = YES;
    }
    if(cellToSwipe > 0)
        _btnPrev.hidden = NO;
    
    if(cellToSwipe <= array.count-1)
    {
        selectedDeal = cellToSwipe;
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:cellToSwipe inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma Fetch meal using sub category and bussiness id

-(void)fetchMealsOfBusiness:(NSString *)bussinessId subCategory:(NSString *)subCategory
{
	[[ConnectionsManager sharedManager] fetchMealsOfBusiness_withdelegate:bussinessId subCategory:subCategory delegate:self];
	[DejalActivityView activityViewForView:WINDOW andAllowTouches:NO];
}





- (void) success:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    
    if([response.method isEqualToString:mGetDealOfDay])
    {
        //Fetch Coupons response
        if([response.respondeBody isKindOfClass:[NSArray class]])
        {
            NSArray *responseList = response.respondeBody;
            if(responseList.count)
            {
                NSMutableArray *temp = [NSMutableArray array];
                for(NSDictionary *dic in responseList)
                {
                    WSCouponsResponse *dealsResponse = [[WSCouponsResponse alloc] initWithDictionary:dic];
                    [temp addObject:dealsResponse];
                }                
                //TODO Array
                array = [[NSMutableArray alloc]initWithArray:temp];
            
                NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"expiredDate" ascending:YES];
                NSArray *sortedArray = [array sortedArrayUsingDescriptors:@[sortDescriptor]];
                
                array = [sortedArray mutableCopy]; //Assign Sorted Array
                
                NSInteger crnIndx =  [self compareDate:sortedArray];
                
                selectedDeal = crnIndx;
                 if(crnIndx != 0)
                     _btnPrev.hidden = NO;
                else
                    _btnPrev.hidden = YES;
                
                if(crnIndx >= array.count-1)
                    _btnNext.hidden = YES;
                else
                    _btnNext.hidden = NO;
                
                  [_collectionView reloadData];
                
                if(crnIndx < [array count] && crnIndx != 0)
                    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:crnIndx inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
            }
        }
    }
	else if([response.method isEqualToString:mFetchMealForBusinessByCatSub])
	{

		WSBusinessCatResponse *bussResponse = [[WSBusinessCatResponse alloc] initWithDictionary:response.respondeBody];
		if(bussResponse.meals && bussResponse.meals.count)
		{
			CategoryForMealHolderVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:kCategoryForMealHolder_SB_ID];
			vc.selectedBussiness = bussResponse.business;
			vc.categoryList = bussResponse.meals;
			vc.baseviewTitle = bussResponse.business.name;
            
            if(wsCouponsRes && ![wsCouponsRes.idMeal isEmpty] && [wsCouponsRes.idMeal intValue] > 0)
            {
                vc.dealCouponResponse = wsCouponsRes;
                [self pushToCategoryHolderVC:vc flag:TRUE];
            }
            else{
                [self.navigationController pushViewController:vc animated:YES];
            }
		}
		else {
			//put message no coupon founds TOAST
		}

	}
}

//SanC
-(void) pushToCategoryHolderVC:(CategoryForMealHolderVC *) vc flag:(BOOL) pushToInnerPage
{
    if(pushToInnerPage)
    {
        
        CategoryForMealDetailVC *catMealDetailVC = nil;
        for (WSCategoryMealResponse *catMeal in vc.categoryList)
        {
            for(WSMealsResponse *mealObj in catMeal.mealsList)
            {
                if([mealObj.id isEqualToString:vc.dealCouponResponse.idMeal])
                {
                    catMealDetailVC = [self getVCWithSB_ID:kCategoryForMealDetailVC_SB_ID];
                    //Fixed issue : when same item added to cart twice
                    catMealDetailVC.mealResponse = [mealObj copyObject];
                    catMealDetailVC.businessId = catMeal.idBusiness;
                    break;
                }
            }
            
        }
        
        if(catMealDetailVC)
        {
            NSMutableArray *navArr = [self.navigationController.viewControllers mutableCopy];
            [navArr addObject:vc];
            [navArr addObject:catMealDetailVC];
            [self.navigationController setViewControllers:navArr animated:YES];
            return;
        }
        
        
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}


//---------------------------------------------------

-(NSInteger)compareDate:(NSArray *)array1
{
//    "yyyy-MM-dd"
//      @"dd-MM-yyyy"
    
    NSDate *currentDate = [DateTimeUtil getTodaysDate];
    
    NSInteger currntIndx = -1 ;
    NSLog(@"Current Date : %@",[NSDate date]);
    for (WSCouponsResponse *resp in array1)
    {
        NSDate *date = [DateTimeUtil dateFromStringDate:resp.expiredDate withFormat:@"yyyy-MM-dd"];
        currntIndx++;
        
        //TODO
        
      /*  NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
        
        NSDateComponents *date1Components = [calendar components:comps
                                                        fromDate: date];
        NSDateComponents *date2Components = [calendar components:comps
                                                        fromDate: currentDate];
        
        date = [calendar dateFromComponents:date1Components];
        currentDate = [calendar dateFromComponents:date2Components];
        */
        
        NSComparisonResult result = [currentDate compare:date];
        
        if(result != NSOrderedDescending)
        {
            return currntIndx;
        }
        
    }
    return [array1 count] - 1;
}

- (void) failure:(WSBaseResponse *)response
{
    [DejalActivityView removeView];
    if([response.method isEqualToString:mGetDealOfDay])
    {
        
    }
}

-(NSURL*)baseImageURL:(NSString*)imgName
{
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",mFetchImagesMealsDeals,imgName]];
	return url;
}
-(BOOL)checkExpireyDate:(NSDate *)crrntDate fromDate:(NSDate*)fromDate
{
    
    NSDate *currentDate = crrntDate;
    
    NSDate *date = fromDate;
    
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger comps = (NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear);
        
        NSDateComponents *date1Components = [calendar components:comps
                                                        fromDate: date];
       // NSDateComponents *date2Components = [calendar components:comps
        //                                                fromDate: currentDate];
        
        date = [calendar dateFromComponents:date1Components];
	// currentDate = [calendar dateFromComponents:date2Components];
        
        if ([date compare:currentDate] == NSOrderedAscending)
        {
            return YES;
        }
    
        return NO;
}


#pragma Barcode - Generator code
//Add by - RR
- (UIImage*)generateCode:(NSString*)barcode {
    
    NSString *filtername = nil;
    filtername = @"CICode128BarcodeGenerator";
    
    CIFilter *filter = [CIFilter filterWithName:filtername];
    [filter setDefaults];
    
    NSData *data = [kText dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    UIImage *image = [UIImage imageWithCGImage:cgImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    //CGFloat scaleRate = 128 / image.size.width;
	 //   UIImage *resized = [self resizeImage:image
     //                        withQuality:kCGInterpolationNone
      //                              rate:scaleRate];
    
    // self.imgBarcode.image = resized;
    
    CGImageRelease(cgImage);
    
    return image;
}
- (UIImage *)resizeImage:(UIImage *)image
             withQuality:(CGInterpolationQuality)quality
                    rate:(CGFloat)rate
{
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}
- (IBAction)showinfo:(id)sender
{
    [appDelegate.window addSubview:[self getReportErrView]];
}
-(ReportError *) getReportErrView{
    
    WSCouponsResponse *wsCoiponsRes = [array objectAtIndex:selectedDeal];
    
    reportErr=[ReportError loadFromNibWithFrame:CGRectMake(0, 0,appDelegate.window.frame.size.width,appDelegate.window.frame.size.height )];
    UITapGestureRecognizer *tapgest = [[UITapGestureRecognizer alloc] initWithTarget:reportErr action:@selector(hideKey)];
    [reportErr addGestureRecognizer:tapgest];
    tapgest.delegate = reportErr;
    [reportErr fillData:wsCoiponsRes.id];
    return reportErr;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (UICollectionViewCell *cell in [_collectionView visibleCells]) {
        NSIndexPath *indexPath = [_collectionView indexPathForCell:cell];
        NSLog(@"%ld",(long)indexPath.row);
    }
}

@end
