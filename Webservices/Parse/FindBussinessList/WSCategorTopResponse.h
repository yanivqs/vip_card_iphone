//
//  WSCategorTopResponse.h
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface WSCategorTopResponse : WSBaseData
//"id":"17",
//"":"1",
//"idBusiness":"1",
//"name":"הכל"

@property (strong,nonatomic) NSString *id, *idCategoryTop, *idBusiness, *name; 
@end
