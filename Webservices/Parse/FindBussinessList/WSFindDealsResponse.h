//
//  WSCouponsResponse.h
//  VIPCard
//
//  Created by Vishal Kolhe on 26/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"


@interface WSFindDealsResponse : WSBaseData

/*
 id : "5"
 idBusiness : "1"
 idCategor : "19"
 unlimited : "0"
 couponOrDeal : "1"
 idTypeSeals : "1"
 price1 : "159"
 price2 : "119"
 quantity1 : "0"
 quantity2 : "0"
 code : "0"
 discount : "0"
 name : "מעמד לאייפד"
 offer : "מעמד כרית לאייפד עם תאורה ומקום לספל בעיצוב מודרני"
 des : "כרית עם משטח ופנס לד "
 marks : "עשוי פלסטיק קשיח שפולט חום"
 image : "Za3690-6a.jpg"
 startDate : "2016-01-16"
 expiredDate : "2016-03-28"
 idMeal : "0"
 isDayDeal : "0"
 barcode : "0"
 nameBusiness : "vipcard"
 logobusiness : "vipLogo.jpg"
 phone : "08-9437847"
 latitude : "31.873843"
 longitude : "34.7407"
 textPrice : ""

*/


@property (nonatomic, strong) NSString *id, *idBusiness, *idCategor, *unlimited, *couponOrDeal, *idTypeSeals, *price1, *price2, *quantity1, *quantity2, *code, *discount, *name, *offer, *des, *marks, *image, *startDate, *expiredDate, *idMeal, *isDayDeal, *barcode, *nameBusiness, *logobusiness, *phone, *latitude, *longitude, *textPrice;
@property(nonatomic,assign)BOOL isbarcode;

//-(id)initWithDictionary:(NSDictionary *)dict;
-(NSString *) dealPriceFormatedValue;
 


@end

