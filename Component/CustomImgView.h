//
//  CustomImgView.h
//  VIPCard
//
//  Created by SanC on 29/06/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomImgView : UIImageView

-(instancetype) initWithImage:(NSString *) img imgUrl:(NSURL *)url;

-(void) showInView:(UIView *) parentView;

@end
