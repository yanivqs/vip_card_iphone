//
//  WSMealsResponse.m
//  VIPCard
//
//  Created by Andrei on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSMealsResponse.h"


@implementation WSMealsResponse
@synthesize id,name,idCategoryForBusiness,image,description,price, extraForMealsList;

/*
-(id)initWithDictionary:(NSDictionary *)dict
{
    if([super initWithDictionary:dict])
    {
        [self parseWithDictionary:dict];
    }
    
    return self;
}
 */

-(void)populateFromDictionary:(NSDictionary *)dict
{
    self.origDictionary = [dict copy];
    [super populateFromDictionary:dict];
    
    NSMutableArray *extraMealDict = [dict objectForKey:@"extraForMeals"];
    if(extraMealDict.count)
    {
        NSMutableArray *array = [NSMutableArray array];
        
        for(NSDictionary *dict in extraMealDict)
        {
            WSExtraForMealsResponse *additionaRes = [[WSExtraForMealsResponse alloc] initWithDictionary:dict];
            [array addObject:additionaRes];
        }
        self.extraForMealsList =  array;
    }
	id param = [dict objectForKey:@"price"];
	if(param == nil || ([param isKindOfClass:[NSNull class]]))
	{
		self.price = @"0";
	}
}
-(void)populateData:(NSDictionary *)dict
{
    self.origDictionary = [dict copy];
    [super populateFromDictionary:dict];
    
    NSMutableArray *extraMealDict = [dict objectForKey:@"extraForMealsList"];
    if(extraMealDict.count)
    {
        NSMutableArray *array = [NSMutableArray array];
        
        for(NSDictionary *dict1 in extraMealDict)
        {
            WSExtraForMealsResponse *additionaRes = [[WSExtraForMealsResponse alloc] initWithDictionary:dict1];
            additionaRes.price = additionaRes.extraForMealPrice;
            WSExtraMealsResponse *extrameals = [[WSExtraMealsResponse alloc] initWithDictionary:dict1];
            additionaRes.selectedExtraMealsResponse = extrameals;
            [array addObject:additionaRes];
            //[array addObject:extrameals];
        }
        self.extraForMealsList =  array;
    }else{
        
        /*
        NSMutableArray *extraMealDict = [dict objectForKey:@"extraMeals"];
        if(extraMealDict.count)
        {
            NSMutableArray *array = [NSMutableArray array];
            
            for(NSDictionary *dict1 in extraMealDict)
            {
                WSExtraForMealsResponse *additionaRes = [[WSExtraForMealsResponse alloc] initWithDictionary:dict1];
                WSExtraMealsResponse *extrameals = [[WSExtraMealsResponse alloc] initWithDictionary:dict1];
                additionaRes.selectedExtraMealsResponse = extrameals;
                [array addObject:additionaRes];
                //[array addObject:extrameals];
            }
            self.extraForMealsList =  array;
        }*/
        NSMutableArray *extraMealDict = [dict objectForKey:@"extraForMeals"];
        if(extraMealDict.count)
        {
            NSMutableArray *array = [NSMutableArray array];
            
            for(NSDictionary *dict1 in extraMealDict)
            {
                WSExtraForMealsResponse *additionaRes = [[WSExtraForMealsResponse alloc] initWithDictionary:dict1];
                [array addObject:additionaRes];
            }
            self.extraForMealsList =  array;
        }
    }
    id param = [dict objectForKey:@"price"];
    if(param == nil || ([param isKindOfClass:[NSNull class]]))
    {
        self.price = @"0";
    }
}

-(WSMealsResponse *) copyObject
{
    WSMealsResponse *newObj = [[WSMealsResponse alloc] initWithDictionary:self.origDictionary];
    
    return newObj;
    
}
-(NSDictionary *)dictionaryFromMenu:(WSMealsResponse*)menu {
    
   return [NSDictionary dictionaryWithObjectsAndKeys:menu.id,@"id",
     menu.name, @"name",menu.idCategoryForBusiness,@"idCategoryForBusiness",menu.image,@"image",menu.description,@"description",menu.price,@"price",menu.extraForMealsList,@"extraForMealsList",menu.origDictionary,@"origDictionary",nil];
    
}

@end
