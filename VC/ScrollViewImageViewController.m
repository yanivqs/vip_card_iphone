//
//  ScrollViewImageViewController.m
//  VIPCard
//
//  Created by SanC on 23/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "ScrollViewImageViewController.h"

@interface ScrollViewImageViewController ()
{
NSArray *imagesArr;
//NSMutableArray *imageSizes;
UIImageView *imageView;
}
@end

@implementation ScrollViewImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackButton:YES];
    // Do any additional setup after loading the view.
	imagesArr=[[NSArray alloc]initWithObjects:@"deals_b",@"icon_day",@"marketing_chains",@"meal",@"menu_business",@"menu_coupon", nil];
	CGRect f=self.view.bounds;
	CGFloat pad=0;
	f.size.width +=pad;
	UIScrollView *mainScrollView = [[UIScrollView alloc] initWithFrame:f];
	mainScrollView.pagingEnabled = YES;
	mainScrollView.showsHorizontalScrollIndicator = NO;
	mainScrollView.showsVerticalScrollIndicator = NO;
	//mainScrollView.decelerating=FALSE;

	mainScrollView.delegate=self;
	f.size.width -=pad;
	CGRect innerScrollFrame = f;


	for (NSInteger i=0; i<[imagesArr count];i++)
	{
		UIImage *image = [UIImage imageNamed:[imagesArr objectAtIndex:i]];

		imageView = [[UIImageView alloc]initWithImage:image];

		//imageView.tag= VIEW_FOR_ZOOM_TAG;
		UIScrollView *pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
		pageScrollView.backgroundColor=[UIColor blueColor];
//		pageScrollView.minimumZoomScale = 1.0f;
//		pageScrollView.maximumZoomScale = 5.0f;
//		pageScrollView.zoomScale = 1.0f;
		pageScrollView.contentSize = image.size;
		//pageScrollView.backgroundColor = [UIColor redColor];
		pageScrollView.delegate = self;
		
		pageScrollView.showsHorizontalScrollIndicator = NO;
		pageScrollView.showsVerticalScrollIndicator = NO;
		//frame.size.width =  width + padding;
		//xPos += frame.size.width+25;
		//CGFloat width = [UIScreen mainScreen].bounds.size.width;
		//CGFloat height = [UIScreen mainScreen].bounds.size.height;
		imageView.frame = CGRectMake(0,100,pageScrollView.frame.size.height,pageScrollView.frame.size.width);
		[pageScrollView addSubview:imageView];

		[mainScrollView addSubview:pageScrollView];

		if (i < [imagesArr count]-1) {
			innerScrollFrame.origin.x += innerScrollFrame.size.width+pad;
		}
	}
	mainScrollView.contentSize = CGSizeMake(innerScrollFrame.origin.x+
											innerScrollFrame.size.width+pad, mainScrollView.bounds.size.height);

	[self.view addSubview:mainScrollView];
//	self.lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 50, 200, 50)];
//	self.lbl.textColor = [UIColor blackColor];
//	self.lbl.font = [UIFont systemFontOfSize:20];
//	[self.view addSubview:self.lbl];
}
//- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
//	return [scrollView viewWithTag: VIEW_FOR_ZOOM_TAG];
//}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
	return NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView

{
	//CGFloat x = scrollView.contentOffset.x;
	//CGFloat pageSize = scrollView.bounds.size.width;
	//int page = (int) x/ pageSize;
	//self.lbl.text = [@"Page number :" stringByAppendingFormat:@"%d",(page+1)];
	
	
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

