//
//  CommonCollectionViewController.m
//  VIPCard
//
//  Created by SanC on 18/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//


#import "CommonCollectionViewController.h"
#import "CustomCell.h"
#import "SWRevealViewController.h"
#import "MainScreenViewController.h"
#import "GalleryVC.h"
#import "GridGalleryResponse.h"
#import "BussinessListVC.h"
#import "MyOrdersVC.h"
#import "UIImageView+WebCache.h"

@interface CommonCollectionViewController ()<UIGestureRecognizerDelegate>
{
	NSArray *imagesArray;
	NSArray *descriptionArray;
}



@end

@implementation CommonCollectionViewController
@synthesize array_images,flagForColor,indicatorFlag;

- (void)viewDidLoad {
	[super viewDidLoad];


	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[self showBackButton:NO];
   
	if (array_images.count<=0)
	{
		[self showEntryView];
		flag=false;
	 imagesArray = [[NSArray alloc]initWithObjects:@"sale",@"Coupan",@"meal",@"icon_day",@"menu_business",@"marketing_chains",@"icon_order", nil];
	 descriptionArray = [[NSArray alloc]initWithObjects:@"MVC",@"DVC",@"RVC",@"CCVC",@"SMVC",@"CTVC", nil];
	}
	else
	{
		flag=true;
		[self showBackButton:YES];

		imagesArray=[array_images mutableCopy];
        if (flagForColor)
         {
             self.collectionView.backgroundColor=[UIColor whiteColor];
        }
	}


	[[self collectionView]setDataSource:self];
	[[self collectionView]setDelegate:self];
	//if (indicatorFlag)
	 //_viewPopupWindow.hidden = NO;
	 //else
	_viewPopupWindow.hidden = YES;
	_btnCategoryList.layer.cornerRadius = 12.0;
	_btnCategoryList.layer.masksToBounds = YES;
	_btnFindBussinessList.layer.cornerRadius = 12.0;
	_btnFindBussinessList.layer.masksToBounds = YES;

	UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
																				 action:@selector(handleTap:)];
	recognizer.numberOfTapsRequired = 1;
	recognizer.delegate = self;
	[_viewPopupWindow addGestureRecognizer:recognizer];
	[super viewDidLayoutSubviews];


    [self showBackButton:YES];
    [self.navigationController.navigationBar setTintColor:[UIColor redColor]];
}


-(void)showEntryView
{
	ShabatView *imgView=[[ShabatView alloc] initWithFrame:appDelegate.window.frame];
    /*
    SDWebImageDownloader *sdwebimg =[[SDWebImageDownloader alloc] init];
    [sdwebimg downloadImageWithURL:[NSURL URLWithString:entryImgURL] options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            

    } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
        [imgView showInView:nil];
        [imgView setImageOnView:image];
    }];*/
    [imgView showInView:nil];
    [imgView setImageOnView];

}



-(void)handleTap:(UITapGestureRecognizer *)sender
{
	_viewPopupWindow.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	[self.collectionView layoutIfNeeded];
	[self.collectionView reloadData];
	if (array_images.count<=0)
	{
		[self.navigationController.navigationBar setTranslucent:NO];
		[self.navigationController.navigationBar setHidden:YES];
	}
	else
	{
		[self.navigationController.navigationBar setTranslucent:NO];
		[self.navigationController.navigationBar setHidden:NO];
	}



}

-(void)customScreen
{
	if(IS_LARGE_SCREEN)
	{

	}
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (!flag)
    {
        return imagesArray.count + 1;
    }
    return imagesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier=@"cell";
	CustomCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];


	cell.imageView.layer.cornerRadius = 10.0;
	cell.imageView.layer.masksToBounds = YES;
	cell.imageView.backgroundColor = [UIColor whiteColor];
	if (!flag)
	{
        cell.imageView.image = nil;
        if(indexPath.row <= imagesArray.count-1)
            [[cell imageView] setImage:[UIImage imageNamed:[imagesArray objectAtIndex:indexPath.row]]];
	}
	else
	{

		if([[imagesArray objectAtIndex:indexPath.row] isKindOfClass:[NSDictionary class]])
		{
			NSDictionary *dict=[imagesArray objectAtIndex:indexPath.row];
			NSString *combined =[NSString stringWithFormat:@"%@%@%@", mFetchImagesBYWeb,[dict objectForKey:@"PathDirectory"] ,[dict objectForKey:@"name"]];
			NSURL *finalURL=[NSURL URLWithString:combined];
			cell.imageView.backgroundColor = [UIColor clearColor];
			cell.imageView.layer.borderColor=[UIColor lightGrayColor].CGColor;
			cell.imageView.layer.borderWidth=1.0f;
			[[cell imageView] sd_setImageWithURL:finalURL placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];

		}
		else if([[imagesArray objectAtIndex:indexPath.row] isKindOfClass:[GridGalleryResponse class]])
		{
			GridGalleryResponse *gridGalleryResponse=[imagesArray objectAtIndex:indexPath.row];
			NSString *combined =[NSString stringWithFormat:@"%@%@%@", mFetchImagesBYWeb,gridGalleryResponse.PathDirectory ,gridGalleryResponse.name];
			NSURL *finalURL=[NSURL URLWithString:combined];
			cell.imageView.backgroundColor = [UIColor clearColor];


			[[cell imageView] sd_setImageWithURL:finalURL placeholderImage:[UIImage imageNamed:PLACE_HOLDER_IMAGE]];

		}

	}

	return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (!flag)
 {
//     if (indexPath.row == 0) {
//         MyOrdersVC *myordervc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyOrdersVC"];
//         [self.navigationController pushViewController:myordervc animated:YES];
//         
//     }
//     else
	 if(indexPath.row == 3)
	 {
		 UIViewController *dealVC = [self getVCWithSB_ID:kDealOfTheDay];
		 [self.navigationController pushViewController:dealVC animated:YES];
         
         //UIViewController *billformVC = [self getVCWithSB_ID:@"MyOrdersVC"];
         //[self.navigationController pushViewController:billformVC animated:YES];
	 }
	 else if(indexPath.row == 4)
	 {
		 //_viewPopupWindow.hidden = NO;
		 [self showFindBussinessPopUp];
	 }
     else if(indexPath.row == 5)
     {
         BussinessListVC *dealVC = [self getVCWithSB_ID:kBussinessList_VC];
         dealVC.isMarketPlace=YES;
        // [self.navigationController pushViewController:dealVC animated:YES];
         AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
         appDelegate1.index =300;
         SWRevealViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kRevealVC_SB_ID];
         [vc setFrontViewController:dealVC];
          [self.navigationController pushViewController:vc animated:YES];
         

     }
     else if(indexPath.row == 6)
     {
         //show my orders
         UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyOrdersVC"];
         [self.navigationController pushViewController:vc animated:YES];
         
         
     }
	 else
	 {
         if(indexPath.row > [imagesArray count]-1)
             return;
		 AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		 appDelegate1.index =indexPath.row;
		 SWRevealViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:kRevealVC_SB_ID];
		 [self.navigationController pushViewController:vc animated:YES];
	 }
	}
	else{

			GalleryVC *galleryVC = [self getVCWithSB_ID:kGalleryVC];
			//GridGalleryResponse *gridGalleryResponse=[imagesArray objectAtIndex:indexPath.row];

			galleryVC.array_getData=[imagesArray mutableCopy];
			galleryVC.indePath=indexPath.row;
			galleryVC.path=indexPath;
		    [self.navigationController pushViewController:galleryVC animated:YES];
			}
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	//collectionView:layout:sizeForItemAtIndexPath:indexPath.item;
	if(IS_LARGE_SCREEN)
	{

		return CGSizeMake(150,150);
	}
    else if(IS_IPHONE_4)
    {
        UICollectionViewFlowLayout *collectionViewLayout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
        collectionViewLayout.sectionInset = UIEdgeInsetsMake(40, 40, 40, 40);
        return CGSizeMake(110, 90);
    }
	else
	{
		return CGSizeMake(128, 110);
	}
}

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//	if ([[segue identifier] isEqualToString:@"detailedSegue"]) {
//		DetailedViewController *imageDetail = [segue destinationViewController];
//		NSArray *arrayOfIndexPaths = [self.collectionView indexPathsForSelectedItems];
//		NSIndexPath *indexPathImInterestedIn = [arrayOfIndexPaths firstObject];
//		imageDetail.imgName = [imagesArray objectAtIndex:indexPathImInterestedIn.item];
//		imageDetail.labelDesc=[descriptionArray objectAtIndex:indexPathImInterestedIn.item];
//		//imageDetail setImageView:imag
////		[imageDetail setValue:imagesArray forKeyPath:l
////		NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] lastObject];
////		NSString *largeImageURL = [pictures1 objectAtIndex:indexPath.row];
////		pictureDetail.imageURL = [NSURL URLWithString:largeImageURL];
//	}
//}
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//
//	return 10.0;
//}
//- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
//	UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];
//	attributes.size = [self systemLayoutSizeFittingSize:layoutAttributes.size withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityFittingSizeLevel];
//	return attributes;
//}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)touchCategoryList:(id)sender
{
	UIViewController *dealVC = [self getVCWithSB_ID:kFieldCategoryFilterVC];
	[self.navigationController pushViewController:dealVC animated:YES];

	_viewPopupWindow.hidden = YES;
}
- (IBAction)touchFindBussinessList:(id)sender
{
	UIViewController *dealVC = [self getVCWithSB_ID:kFindBussinessList];
	[self.navigationController pushViewController:dealVC animated:YES];

	_viewPopupWindow.hidden = YES;
}




@end
