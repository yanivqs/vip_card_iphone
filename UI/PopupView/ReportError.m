//
//  ReportError.m
//  VIPCard
//
//  Created by Andrei Boulgakov on 26/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "ReportError.h"


@implementation ReportError
@synthesize txtRemark;

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    txtRemark.layer.borderWidth = 1.0;
    txtRemark.layer.borderColor = [UIColor orangeColor].CGColor;
    
    _mainview.layer.borderWidth = 0.5;
    _mainview.layer.borderColor = [UIColor colorWithRed:207.0/255 green:207.0/255 blue:207.0/255 alpha:1].CGColor;
    
    _lblProdCode.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Product Code", nil)];
    _lblProbType.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Problem Type", nil)];
    _lblRemarks.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"Remarks", nil)];
    //_btnSub.titleLabel.text = NSLocalizedString(@"Submit", nil);
    [_btnSub setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
    _lblHead.text = NSLocalizedString(@"You have found a mistake update", nil);
    txtRemark.delegate = self;
    _txtprodcode.textAlignment = UITextAlignmentRight;
    openKey = NO;
    
}
-(void)hideKey
{
   
    [self endEditing:YES];
}
+(ReportError *) loadFromNibWithFrame:(CGRect) frame
{
    ReportError *rperr = [[[UINib nibWithNibName: @"ReportError"
                                           bundle: [NSBundle bundleForClass: [ReportError class]]]
                            
                            instantiateWithOwner: nil
                            options: nil] firstObject];
    rperr.frame = frame;
    rperr.txtRemark.layer.borderColor = [UIColor orangeColor].CGColor;
    
    
    return rperr;
}
-(void)fillData:(NSString*)data
{
    NSString *code = [NSString stringWithFormat:@"%@ %@",data,NSLocalizedString(@"Product Code", nil)];
    _txtprodcode.text = code;
}
/*
-(void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self convertRect:rawFrame fromView:nil];
    
    NSLog(@"keyboardFrame: %@", NSStringFromCGRect(keyboardFrame));
}*/
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateView];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateView];
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    UITableView *tableView = _tblTypes;
    CGPoint touchPoint = [touch locationInView:tableView];
    return ![tableView hitTest:touchPoint withEvent:nil];
}
-(void)animateView
{
    CGFloat ypos = self.mainview.frame.origin.y;
    if (!openKey) {
        ypos = ypos - 120;
        openKey = YES;
    }else{
        ypos = ypos + 120;
        openKey = NO;
    }
    self.mainview.frame = CGRectMake(self.mainview.frame.origin.x, ypos, self.mainview.frame.size.width, self.mainview.frame.size.height);
}

- (IBAction)onClick_Close:(id)sender {
    [self removeFromSuperview];
    
}

- (IBAction)showTypes:(id)sender {

    typeArr = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Specifics", nil),NSLocalizedString(@"Incorrect Information", nil),NSLocalizedString(@"Price is right", nil),NSLocalizedString(@"Application mulfunction", nil),NSLocalizedString(@"Other", nil), nil];
    [_tblTypes setDataSource:self];
    [_tblTypes setDelegate:self];
    [_tblTypes reloadData];
    
    if (_tblTypes.hidden) {
        [_tblTypes setHidden:NO];
    }else{
        [_tblTypes setHidden:YES];
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return typeArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseID = @"typeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];
    }
    
    
    cell.textLabel.text = [typeArr objectAtIndex:indexPath.row];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    cell.textLabel.textColor = [UIColor blackColor];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tblTypes setHidden:YES];
    [_btntype setTitle:[typeArr objectAtIndex:indexPath.row] forState:UIControlStateNormal];
}

@end
