//
//  ViewPopUp.m
//  VIPCard
//
//  Created by SanC on 13/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "ViewPopUp.h"

@implementation ViewPopUp
@synthesize array_tblData,dict_choice,header;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
*/
 - (void)drawRect:(CGRect)rect {
    // Drawing code
	 self.backgroundColor=[UIColor clearColor];
}


- (instancetype) initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];

	[self setupTableView];

	return self;
}

#pragma mark-UserDefineMethods
-(void)setupTableView
{
	self.backgroundColor = [UIColor clearColor];
	transparentView=[[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,  self.frame.size.height)];
	transparentView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
	[self addSubview:transparentView];
	transparentView.alpha=0.6;
	singleFingerTap =
	[[UITapGestureRecognizer alloc] initWithTarget:self
											action:@selector(handleSingleTap:)];
	[transparentView addGestureRecognizer:singleFingerTap];
	[transparentView setUserInteractionEnabled:TRUE];
	self.tbl_View=[[UITableView alloc] initWithFrame:CGRectMake(10,190, 300,90)];
	self.tbl_View.backgroundColor=[UIColor whiteColor];
	self.tbl_View.delegate=self;
	self.tbl_View.dataSource=self;
	self.tbl_View.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
	[self.tbl_View registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellIdentifier"];
    
//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(1, 50, 276, 30)];
//    headerView.backgroundColor = [UIColor redColor];
//    UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(4, 5, 276, 24)];
//    labelView.textColor = [UIColor whiteColor];
//    labelView.text = header;
//    [headerView addSubview:labelView];
//    self.tbl_View.tableHeaderView = headerView;
	
    [self addSubview:self.tbl_View];
	//[self.tbl_View reloadData];
}
#pragma mark-Delegate



#pragma mark-UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.array_tblData ) ? self.array_tblData.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"cellIdentifier";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
	tableView.separatorColor=[UIColor grayColor];
	tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

	if (cell == nil)
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
	[tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	cell.textLabel.textColor=[UIColor darkTextColor];
	cell.textLabel.text=[self.array_tblData objectAtIndex:indexPath.row];
	cell.textLabel.textAlignment = NSTextAlignmentRight;
    cell.textLabel.numberOfLines=0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	return cell;

}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return header;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(1, 50, 276, 30)];
    headerView.backgroundColor = [UIColor redColor];
    UILabel *labelView = [[UILabel alloc] initWithFrame:CGRectMake(4, 5, 276, 24)];
    labelView.text = header;
    labelView.textAlignment = NSTextAlignmentRight;
    
    [headerView addSubview:labelView];
    return headerView;
}
#pragma mark-UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	NSString *str_indexPath=[array_tblData objectAtIndex:indexPath.row];
	//[popUpDelegate returnData:str_indexPath];
	if(self.popUpDelegate && [self.popUpDelegate respondsToSelector:@selector(returnData:)])
	{
		[self.popUpDelegate returnData:str_indexPath];
	}
	[self removeFromSuperview];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 30;
}

#pragma mark-UserDefineMethods
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
	[self removeFromSuperview];
}

//Reload data to refresh items
-(void) reloadScreenData
{
    if(!array_tblData)
        array_tblData = [NSArray array];
    
    [self.tbl_View reloadData];
    [self adjustHeightOfTableview];
}

- (void)adjustHeightOfTableview
{
    CGFloat height = self.tbl_View.contentSize.height;
    CGFloat maxHeight = self.tbl_View.superview.frame.size.height - self.tbl_View.frame.origin.y;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the frame accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.tbl_View.frame;
        frame.size.height = height;
        self.tbl_View.frame = frame;
        
        // if you have other controls that should be resized/moved to accommodate
        // the resized tableview, do that here, too
    }];
}

@end
