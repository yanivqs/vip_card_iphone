//
//  BaseTableViewCell.m
//  VIPCard
//
//  Created by SanC on 17/03/16.
//  Copyright (c) 2016 Enovate. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "NSString+CommonForApp.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(CGSize)getStringSize:(UILabel *)aLabel andString:(NSString *)aStr
{
    CGSize expectedLabelSize;
    if(!aStr ||[aStr isEqualToString:@""])
        return expectedLabelSize;
    NSMutableString *myString = [[NSMutableString alloc] initWithString:aStr];
    
    CGSize maximumLabelSize = CGSizeMake(aLabel.frame.size.width,9999);

    
    expectedLabelSize = [myString sizeWithFont:aLabel.font
                             constrainedToSize:maximumLabelSize
                                 lineBreakMode:aLabel.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame = aLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    aLabel.frame = newFrame;
    return expectedLabelSize;
}

@end
