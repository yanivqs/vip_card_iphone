//
//  UserRegistrationVC.h
//  VIPCard
//
//  Created by Andrei on 29/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"

@interface UserRegistrationVC : BaseViewController<UITextFieldDelegate>
@property (strong , nonatomic) IBOutlet UITextField *textEmail;
@property (strong , nonatomic) IBOutlet UITextField *textPassword;

@property (strong , nonatomic) IBOutlet UITextField *textPassword_Auth;

@property (strong , nonatomic) IBOutlet UITextField *textLastName;

@property (strong , nonatomic) IBOutlet UITextField *textPhone;

@property (strong , nonatomic) IBOutlet UITextField *textT_LIDH;

@property (strong , nonatomic) IBOutlet UITextField *textFirstName;

@property (strong , nonatomic) IBOutlet UILabel *titleEmail;

@property (strong , nonatomic) IBOutlet UILabel *titlePassword;

@property (strong , nonatomic) IBOutlet UILabel *titlePassword_Auth;

@property (strong , nonatomic) IBOutlet UILabel *titleLastName;

@property (strong , nonatomic) IBOutlet UILabel *titlePhone;

@property (strong , nonatomic) IBOutlet UILabel *titleT_LIDH;

@property (strong , nonatomic) IBOutlet UILabel *titleFirstName;


@property (strong , nonatomic) IBOutlet UIImageView *imgEmail;

@property (strong , nonatomic) IBOutlet UIImageView *imgPassword;

@property (strong , nonatomic) IBOutlet UIImageView *imgPassword_Auth;

@property (strong , nonatomic) IBOutlet UIImageView *imgLastName;

@property (strong , nonatomic) IBOutlet UIImageView *imgPhone;

@property (strong , nonatomic) IBOutlet UIImageView *imgT_LIDH;

@property (strong , nonatomic) IBOutlet UIImageView *imgFirstName;

@property (strong , nonatomic) IBOutlet UIButton *btnCheckOut;

@property (strong , nonatomic) IBOutlet UIButton *btnCheck;

@property (strong , nonatomic) IBOutlet UILabel *titlCheck;

@property (strong , nonatomic) IBOutlet UILabel *check;

@property (strong , nonatomic) IBOutlet UIImageView *imgCheck;

-(IBAction)actionCheckOut:(id)sender;

-(IBAction)actionCheck:(id)sender;

-(void)registrationAPI:(NSDictionary*)dict;
@property(nonatomic,retain)NSMutableDictionary *dic_data;
@end
