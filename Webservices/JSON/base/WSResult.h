//
//  WSResult.h
//  Chain
//
//  Created by Andrei Boulgakov on 01/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WSBaseData.h"
#import "WSErrorMessage.h"

@interface WSResult : WSBaseData

@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSArray *message;

@property  (nonatomic,readonly) NSString *localizedErrorMessage;
@property  (nonatomic,readonly) NSString *error_code;

-(BOOL)isSuccess;

@end
