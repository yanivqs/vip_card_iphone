//
//  Obj_FindControllerList.h
//  VIPCard
//
//  Created by Andrei on 20/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

typedef enum
{
    TYPEBUSSINESS_1 = 1,
    TYPEBUSSINESS_2 = 2,
    TYPEBUSSINESS_3 = 3,
    TYPEBUSSINESS_4 = 3,
    TYPEBUSSINESS_5 = 4,
    TYPEBUSSINESS_6 = 5,
    TYPEBUSSINESS_7 = 5
} CATEGORY;

#import <Foundation/Foundation.h>

@interface Obj_FindControllerList : NSObject


-(NSArray*)getViewControllerList:(NSInteger)id_;

@end
