//
//  BillingFormVC.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 02/08/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
#import "MyOrders.h"
#import "FactoryCoreService.h"
#import "FactoryInfoContex.h"

@interface BillingFormVC : BaseViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    int noOfCards;
}
@property (weak, nonatomic) IBOutlet UILabel *lblchk;
@property (weak, nonatomic) IBOutlet UIButton *btnsplit;
@property (weak, nonatomic) IBOutlet UIButton *btnchk;
@property (weak, nonatomic) IBOutlet UIButton *btnsend;
@property (weak, nonatomic) IBOutlet UITableView *tblcards;
@property (strong, nonatomic) NSString *orderTotal,*orderId,*cnfMsg,*logo;
@property (strong, nonatomic)NSString *sendFax,*sendEmail,*sendSms;
@property (strong, nonatomic) NSMutableDictionary *dicOrder,*cartDic;
@property(strong,nonatomic) MyOrders *myOrder;
@property (assign ,nonatomic) BOOL isFromMyOrder;
@property (weak, nonatomic) IBOutlet UIView *botview;
@property (strong,nonatomic) WSBussiness *selectedBussiness;
@property (assign ,nonatomic) BOOL isCheckFE;
@property (assign ,nonatomic) BOOL isCheckPC;
@property (strong,nonatomic)NSDate *chkboxStartTime,*chkboxEndTime;

- (IBAction)splitcards:(id)sender;
- (IBAction)sendData:(id)sender;

- (IBAction)checkRestPay:(id)sender;


//Manoj
@property (nonatomic, assign) BOOL isCheckedFE,isCheckedPC, isCheckedST;


@property (weak, nonatomic) IBOutlet UIView *baseFactoryEmployeeView;
@property (weak, nonatomic) IBOutlet UIView *basePaymentCompanyView;
@property (weak, nonatomic) IBOutlet UIView *baseSettingtimeView;

@property (weak, nonatomic) IBOutlet UIButton *btnFECheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnPCCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnSTCheckBox;
@property (weak, nonatomic) IBOutlet UILabel *lblFE;
@property (weak, nonatomic) IBOutlet UIButton *dropDownFE;
@property (weak, nonatomic) IBOutlet UILabel *lblPC;
@property (weak, nonatomic) IBOutlet UILabel *lblST;
@property (weak, nonatomic) IBOutlet UIButton *dropDownST;

- (IBAction)showFEPicker:(id)sender;
- (IBAction)showSTPicker:(id)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint_FE_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint_PC_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint_ST_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint_STpicker_ht;

- (IBAction)onClickFECheckbox:(id)sender;
- (IBAction)onClickPCCheckbox:(id)sender;
- (IBAction)onClickSTCheckbox:(id)sender;

//
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint_FE_YPOS;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint_PC_Ypos;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint_ST_Ypos;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_baseFactoryViewHeight;

@end
