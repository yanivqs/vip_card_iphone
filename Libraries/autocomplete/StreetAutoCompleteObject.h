//
//  StreetAutoCompleteObject.h
//  rosinter
//
//  Created by Yogesh Bhamre on 09/10/14.
//  Copyright (c) 2014 Enovate. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MLPAutoCompletionObject.h"

@interface StreetAutoCompleteObject : NSObject <MLPAutoCompletionObject>
@property (strong) NSString *streetName;
@property (strong,nonatomic) NSString *streetId;
- (id)initWithStreetName:(NSString *)name andId:(NSString *) streetId;
@end
