//
//  CategoryForMealHolderVC.h
//  VIPCard
//
//  Created by Vishal Kolhe on 28/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseViewController.h"
@class WSBussiness;
@class WSCouponsResponse;

@interface CategoryForMealHolderVC : BaseViewController
@property (nonatomic, strong) NSArray *categoryList;
@property (strong, nonatomic) IBOutlet UIView *cartContainView;
@property (strong, nonatomic) IBOutlet UILabel *cartTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblNumberOfItems;
@property (strong, nonatomic) IBOutlet UIButton *cartButton;
@property (strong, nonatomic) IBOutlet NSString *baseviewTitle;
//- (IBAction)cartButtonAction:(id)sender;
@property (nonatomic, strong) NSArray *listOfmealItems;

@property (weak, nonatomic) IBOutlet UIButton *btnAddCart;
- (IBAction)onClickAddtoCartButton:(id)sender;

@property (strong, nonatomic) WSBussiness *selectedBussiness;

@property (strong, nonatomic) WSCouponsResponse *dealCouponResponse;

@end
