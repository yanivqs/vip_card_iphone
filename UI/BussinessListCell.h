//
//  BussinessListCell.h
//  VIPCard
//
//  Created by SanC on 12/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHCStrikethroughLabel.h"

@interface BussinessListCell : UITableViewCell
{

}
@property (weak, nonatomic) IBOutlet UILabel *lbl_Title;
@property (weak, nonatomic) IBOutlet UIImageView *img_View;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Discription;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NewAmount;
@property (weak, nonatomic) IBOutlet UILabel *lbl_BottomLeftTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_TitleHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_DescriptionHeight;
@property (weak, nonatomic) IBOutlet SHCStrikethroughLabel *lbl_ExistingAmount;

@end
