//
//  MyOrdersCell.h
//  VIPCard
//
//  Created by Andrei Boulgakov on 26/07/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MyOrders.h"
#import "UIImageView+WebCache.h"
#import "AppGlobalConstant.h"

@protocol MyOrderDelegate <NSObject>

-(void)onOrderDetailClicked:(MyOrders*)myOrder;
-(void)onOrderDeleteClicked:(MyOrders*)myOrder;

@end

@interface MyOrdersCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_ordDate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_ordSum;

@property (weak, nonatomic) IBOutlet UIButton *btn_ordDetail;
@property (weak, nonatomic) IBOutlet UIImageView *img_bzlogo;
@property (weak, nonatomic) IBOutlet UIButton *btnDel;
@property (weak, nonatomic) MyOrders *orderObj;
@property (weak, nonatomic) id delegate;

-(void)populateData:(MyOrders*)myOrder;
-(IBAction)orderDetail:(id)sender;
-(IBAction)deleteOrder:(id)sender;

@end
