//
//  CatTopProduct.h
//  VIPCard
//
//  Created by Vishal Kolhe on 20/05/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "WSBaseData.h"

@interface CatTopProduct : WSBaseData

//id : "47"
//name : "כלי בית ופנאי"
//products:[]
@property(strong,nonatomic)NSString *id,*name;

@property(strong,nonatomic)NSArray *products;
//-(id)initWithDictionary:(NSDictionary *)dict;
@end


