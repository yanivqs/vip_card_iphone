//
//  MainScreenViewController.h
//  VIPCard
//
//  Created by fis on 20/03/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface MainScreenViewController : BaseViewController
{
	NSMutableArray *array;
}
@property(nonatomic, assign)NSArray *firstArray;
@property(nonatomic, assign) NSInteger index;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UITextField *txtFldSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel, *sidebarbutton;
@property(nonatomic,strong)NSString *str_ViewControllers;

-(void) refreshViewOnChange;


@end
