//
//  WelcomeMessagesWS.m
//  Chain
//
//  Created by Andrei Boulgakov on 15/09/14.
//  Copyright (c) 2014 MoshiachTimes. All rights reserved.
//

#import "WelcomeMessagesWS.h"

@implementation WelcomeMessagesWS
@synthesize comment_count, create_date, image_url, text_message, title, schedule_days, user_id, openCount, avg_ranking, video_url, video_thumb, share_count, category, account_id, avg_message_time, msg_phone_call_count;

- (id) initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self)
    {
        [self parseWithDictionary:dict];
    }
    return self;
}

- (void) parseWithDictionary:(NSDictionary *)dict
{
    self.category           =            [[dict objectForKey:@"category"] intValue];
    if([[dict objectForKey:@"open_count"] isKindOfClass:[NSString class]])
    self.openCount          =            [dict objectForKey:@"open_count"];
    else
        self.openCount      =            [[dict objectForKey:@"open_count"] stringValue];
    self.avg_ranking        =            [dict objectForKey:@"avg_ranking"];
    self.comment_count      =            [dict objectForKey:@"comment_count"];
    self.create_date        =            [dict objectForKey:@"create_date"];
    self.image_url          =            [dict objectForKey:@"image_url"];
    self.schedule_days      =            [dict objectForKey:@"schedule_days"];
    self.title              =            [dict objectForKey:@"title"];
    self.user_id            =            [dict objectForKey:@"id"];
    self.text_message       =            [dict objectForKey:@"text_message"];
    self.video_url          =            [dict objectForKey:@"video_url"];
    self.share_count        =            [dict objectForKey:@"share_count"];
    self.video_thumb        =            [dict objectForKey:@"video_thumb"];
    self.account_id         =            [dict objectForKey:@"account_id"];
    self.avg_message_time   =            [dict objectForKey:@"avg_message_time"];
    self.allow_holiday      =            [dict[@"allow_holiday"] boolValue];
    self.verify_comment     =            [dict[@"verify_comment"] boolValue];
    
    if([[dict objectForKey:@"user_viewed_count"] isKindOfClass:[NSString class]])
        self.user_viewed_count          =            [dict objectForKey:@"user_viewed_count"];
    else
        self.user_viewed_count          =            [[dict objectForKey:@"user_viewed_count"] stringValue];
    
    id _msg_phone_call_count = [dict objectForKey:@"msg_phone_call_count"];
    if([_msg_phone_call_count isKindOfClass:[NSNumber class]])
    {
        self.msg_phone_call_count = [_msg_phone_call_count stringValue];
    }
    else if([_msg_phone_call_count isKindOfClass:[NSString class]])
    {
        self.msg_phone_call_count = _msg_phone_call_count;
    }
}

#pragma Overide Default Functions

-(id)getCoreDataFromObject:(id) coreData
{
    
	return nil;
}

-(void) populateObjectFromCorData:(id)coreData
{
	
    
}

@end