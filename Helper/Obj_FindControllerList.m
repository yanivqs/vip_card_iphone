//
//  Obj_FindControllerList.m
//  VIPCard
//
//  Created by Andrei on 20/04/16.
//  Copyright © 2016 Enovate. All rights reserved.
//

#import "Obj_FindControllerList.h"

@implementation Obj_FindControllerList

-(NSArray*)getViewControllerList:(NSInteger)id_
{
    NSArray *array;
    
    switch (id_) {
        case TYPEBUSSINESS_1:
           
            break;
            
        case TYPEBUSSINESS_2:
            array = [NSArray arrayWithObjects: @"ABOUT", @"HOME",nil];
            break;

        case TYPEBUSSINESS_3 | TYPEBUSSINESS_4:
            array = [NSArray arrayWithObjects: @"GALLERY" , @"ABOUT", @"HOME",nil];
            break;

        case TYPEBUSSINESS_5:
            array = [NSArray arrayWithObjects: @"GALLERY", @"DEAL", @"ABOUT", @"HOME",nil];
            break;

        case TYPEBUSSINESS_6 | TYPEBUSSINESS_7:
            array = [NSArray arrayWithObjects: @"GALLERY" , @"COUPON", @"DEAL", @"ABOUT", @"HOME",nil];
            break;
            
        default:
            break;
    }
    
    return array;
}
@end
